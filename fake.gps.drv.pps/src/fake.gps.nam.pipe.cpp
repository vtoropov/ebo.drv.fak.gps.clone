/*
	Created by Tech_dog (ebontrop@gmail.com) on 22-Sep-2013 at 2:50:07p, UTC+3, Taganrog, Sunday;
	This is Pulsepay MagTek HID Device Data Reader Wrapper class implementation file.
	-----------------------------------------------------------------------------
	Adopted to FakeGPS project on 16-Dec-2019 at 0:53:33a, UTC+7, Novosibirsk, Tulenina, Monday;
*/
#include "StdAfx.h"
#include "fake.gps.nam.pipe.h"
#include "fake.gps.acc.sec.h"

using namespace shared::drv::um;

/////////////////////////////////////////////////////////////////////////////

namespace shared { namespace drv { namespace um { namespace _impl {

	class CNamedPipe_Event {
	private:
		HANDLE&    m_evt_ref;
	public:
		 CNamedPipe_Event (HANDLE& _evt_ref) : m_evt_ref(_evt_ref) {
			 m_evt_ref = ::CreateEvent(NULL, TRUE, FALSE, NULL);
		}
		~CNamedPipe_Event (void) {
			if (m_evt_ref) {
				::CloseHandle(m_evt_ref); m_evt_ref = NULL;
			}
		}
	};

}}}}
using namespace shared::drv::um::_impl;
/////////////////////////////////////////////////////////////////////////////

CNamedPipe:: CNamedPipe(IPipeTrace& _trace) : m_trace(_trace), m_pipe(NULL), m_connected(false), m_wait(100) {}
CNamedPipe::~CNamedPipe(void) {}

/////////////////////////////////////////////////////////////////////////////
// https://docs.microsoft.com/en-us/windows/win32/api/namedpipeapi/nf-namedpipeapi-connectnamedpipe

HRESULT     CNamedPipe::Connect(const bool& _b_interruped) {
	if (this->Connected())
		return (__DwordToHresult(ERROR_INVALID_STATE));

	OVERLAPPED over_ = {0};
	CNamedPipe_Event evt_wait(over_.hEvent);
	if (NULL == over_.hEvent)
		return (__LastErrToHresult());

	m_trace.IPipeTrace_PutInfo("CNamedPipe::Connect()::begin();");

	const BOOL b_res = ::ConnectNamedPipe(this->Handle(), &over_);
	if (b_res) {
		m_connected  = true; return S_OK;
	}

	HRESULT hr_ = S_OK;
	bool b_states[3] = { false, false, false }; // {0:pending|1:complete|2:waiting};

	m_trace.IPipeTrace_PutInfo("CNamedPipe::Connect()::await connection...");

	while (false == _b_interruped) {
		DWORD nBytesRead = 0;
		if (b_states[0]) { // in pendig state

			if (::GetOverlappedResult(this->Handle(), &over_, &nBytesRead, FALSE) == FALSE) {

				if (ERROR_PIPE_CONNECTED == ::GetLastError()) {
					::SetEvent(over_.hEvent);
					b_states[1] = true; break; // a client connected between creating pipe and connect operations;
				}
				if (ERROR_IO_INCOMPLETE  != ::GetLastError()) {
					hr_ = __LastErrToHresult();
					m_trace.IPipeTrace_PutError("CNamedPipe::Connect()::overlap: failure code=0x%x;", hr_);
					break;
				}
			}
			else {
				b_states[1] = true; break; // completed;
			}
		}
		else if (b_states[2]) { // in waiting state

			const DWORD dwResult = ::WaitForSingleObject(over_.hEvent, 10);
			b_states[2]  = (WAIT_OBJECT_0 != dwResult); // in wait state;
			b_states[0]  = (WAIT_OBJECT_0 == dwResult); // in pending state;
		}
		else {
			b_states[2] = true; // wait state;
		}
		::Sleep(10);
	}
	m_connected = b_states[1];
	if (m_connected)
	m_trace.IPipeTrace_PutInfo ("CNamedPipe::Connect()::success();"); else
	m_trace.IPipeTrace_PutError("CNamedPipe::Connect()::timeout();", hr_);
	m_trace.IPipeTrace_PutInfo ("CNamedPipe::Connect()::end();");

	return hr_;
}

bool        CNamedPipe::Connected (void) const { return m_connected; }

HRESULT     CNamedPipe::Create (LPCTSTR _lp_sz_name) {

	HRESULT hr_ = m_loc.URI(_lp_sz_name);
	if (FAILED(hr_))
		return(hr_);

	if (this->Is() == true)
		return (__DwordToHresult(ERROR_ALREADY_INITIALIZED));

	const CPipeAccess acc_;
	if (acc_.Is() == false)
		return ((hr_ = OLE_E_BLANK));

	m_pipe = ::CreateNamedPipe(
			m_loc, PIPE_ACCESS_DUPLEX|FILE_FLAG_OVERLAPPED, PIPE_TYPE_BYTE, PIPE_UNLIMITED_INSTANCES, 0, 0, m_wait, acc_
		);
	if (NULL == m_pipe)
		hr_  = __LastErrToHresult();

	return hr_;
}

HRESULT     CNamedPipe::Destroy(void) {

	if (this->Is() == false)
		return (__DwordToHresult(ERROR_INVALID_STATE));

	if (this->Connected())
		this->Disconnect();

	if (::CloseHandle(m_pipe) == FALSE)
		return (__LastErrToHresult());
	
	m_pipe = NULL;

	return S_OK;
}

HRESULT     CNamedPipe::Disconnect(void) {
	if (this->Connected() == false) return __DwordToHresult(ERROR_INVALID_STATE);
	const BOOL b_res = ::DisconnectNamedPipe(this->Handle());
	if (FALSE == b_res)
		return (__LastErrToHresult());
	else {
		this->m_connected = false;
		return S_OK;
	}
}

HANDLE      CNamedPipe::Handle (void) const { return m_pipe ; }
const bool  CNamedPipe::Is     (void) const { return m_pipe !=  NULL; }
TLocatorRef CNamedPipe::Locator(void) const { return m_loc  ; }

/////////////////////////////////////////////////////////////////////////////

CNamedPipe::operator    HANDLE (void) const { return this->Handle(); }

/////////////////////////////////////////////////////////////////////////////