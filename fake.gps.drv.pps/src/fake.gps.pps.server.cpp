/*
	Created by Tech_dog (ebontrop@gmail.com) on 16-Dec-2019 at 3:13:50a, UTC+7, Novosibirsk, Tulenina, Monday;
	This is FakeGPS named pipe connection server interface implementation file;
*/
#include "StdAfx.h"
#include "fake.gps.pps.server.h"

using namespace shared::drv::um;
/////////////////////////////////////////////////////////////////////////////

namespace shared { namespace drv { namespace um { namespace _impl {

	DWORD WINAPI _thread_fun(CPipeServer* _p_srv){
		if (NULL == _p_srv)
			return ERROR_INVALID_ADDRESS;
		else
			return _p_srv->Thread_Fun();
	}

}}}}
using namespace shared::drv::um::_impl;
/////////////////////////////////////////////////////////////////////////////

CPipeServer:: CPipeServer (IPipeTrace& _trace) :
	m_trace(_trace), m_pipe(_trace), m_break_it_on(false), m_thread(NULL), m_reader(*this, _trace), m_writer(*this, _trace) {}
CPipeServer::~CPipeServer (void) {}

/////////////////////////////////////////////////////////////////////////////
bool          CPipeServer::IsOn (void) const { return NULL != m_thread; }
HRESULT       CPipeServer::Turn (const bool _b_on ) {

	if (_b_on == true  && this->IsOn() == true ) return (__DwordToHresult(ERROR_INVALID_STATE));
	if (_b_on == false && this->IsOn() == false) return (__DwordToHresult(ERROR_INVALID_STATE)); HRESULT hr_ = S_OK;

	if (_b_on) {

		hr_ = m_pipe.Create(_T("FakeGPS"));
		if (FAILED(hr_)) {
			m_trace.IPipeTrace_PutError("CPipeServer::Turn(): creating named pipe failed: 0x%x;", hr_);
			return hr_;
		}
		else
			m_trace.IPipeTrace_PutInfo ("CPipeServer::Turn(): named pipe is created;");

		m_break_it_on = false;
		DWORD dw_id_  = 0;

		m_thread = ::CreateThread(NULL, 0, (LPTHREAD_START_ROUTINE)_thread_fun, this, 0, &dw_id_);
		if (NULL == m_thread)
			hr_  = __DwordToHresult(::GetLastError());
		else
			m_trace.IPipeTrace_PutInfo("CPipeServer::Turn(): has started for performing client requests;");
	}
	else {
		m_break_it_on = true;

		::WaitForSingleObject(m_thread, INFINITE);
		::CloseHandle(m_thread);

		m_writer.Stop();
		m_reader.Stop();

		m_pipe.Destroy();

		m_trace.IPipeTrace_PutInfo("CPipeServer::Turn(): named pipe is closed;");

		m_thread = NULL;
		m_trace.IPipeTrace_PutInfo("CPipeServer::Turn(): has stopped;");
	}

	return  hr_;
}

/////////////////////////////////////////////////////////////////////////////

HRESULT     CPipeServer::IPipeEvent_OnCreate(LPCTSTR) {
	HRESULT hr_ = S_OK;
	return  hr_;
}
HRESULT     CPipeServer::IPipeEvent_OnRead  (const LPBYTE _p_data, const DWORD _dw_sz) {
	_p_data; _dw_sz;
	m_trace.IPipeTrace_PutInfo("CPipeServer::IPipeEvent_OnRead()::received: %d byte(s)", _dw_sz);
	HRESULT hr_ = S_OK;
	return  hr_;
}
HRESULT     CPipeServer::IPipeEvent_OnWrite (const LPBYTE _p_data, const DWORD _dw_sz) {
	_p_data; _dw_sz;
	HRESULT hr_ = S_OK;
	return  hr_;
}

/////////////////////////////////////////////////////////////////////////////

DWORD CPipeServer::Thread_Fun(void) {

	HRESULT hr_ = S_OK;

	m_trace.IPipeTrace_PutInfo ("CPipeServer::Thread_Fun()::begin();");

	while (false == m_break_it_on)  {

		if (m_pipe.Connected() == false) {

			bool b_interrupted = m_break_it_on;

			hr_ = this->m_pipe.Connect(b_interrupted); // a reference to volatile variable is not allowed;
			if (SUCCEEDED(hr_))
				m_trace.IPipeTrace_PutInfo ("CPipeServer::CNamedPipe::Connect(): succeeded;");
			else
				m_trace.IPipeTrace_PutError("CPipeServer::CNamedPipe::Connect(): error=0x%x)", hr_);
		}
		if (m_pipe.Connected() == true ) {
			m_reader.Get(m_pipe.Handle());
		}
		::Sleep(10);
	}

	m_trace.IPipeTrace_PutInfo ("CPipeServer::Thread_Fun()::end();");

	return NO_ERROR;
}