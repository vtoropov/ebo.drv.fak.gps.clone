/*
	Created by Tech_dog (ebontrop@gmail.com) on 2-Dec-2019 at 3:11:23a, UTC+7, Novosibirsk, Tulenina, Monday;
	This is Ebo Drv UMDF named pipe data exchange channel interface implementation file;
	-----------------------------------------------------------------------------
	Adopted to FakeGPS project on 16-Dec-2019 at 0:53:33a, UTC+7, Novosibirsk, Tulenina, Monday;
*/
#include "StdAfx.h"
#include "fake.gps.pip.cnn.h"

using namespace shared::drv::um;

/////////////////////////////////////////////////////////////////////////////

CPipeChannel:: CPipeChannel(INamedPipeTrace& _trace) : m_pipe(_trace), m_trace(_trace) { }
CPipeChannel::~CPipeChannel(void) {}

/////////////////////////////////////////////////////////////////////////////

/////////////////////////////////////////////////////////////////////////////

CPipeReader:: CPipeReader(INamedPipeTrace& _trace) : TChannel(_trace), m_b_stop(true) {}
CPipeReader::~CPipeReader(void) {}

/////////////////////////////////////////////////////////////////////////////

const bool  CPipeReader::IsRunning (void) const { return !m_b_stop; }

HRESULT     CPipeReader::Start (void) {

	if (TChannel::m_pipe.Is() == false)
		return (OLE_E_BLANK);

	if (this->IsRunning())
		return (__DwordToHresult(ERROR_BUSY));

	m_b_stop = false;
	HRESULT hr_ = S_OK;

	while (m_b_stop == false) {
		const BOOL b_connected = ::ConnectNamedPipe(TChannel::m_pipe, NULL);
		if (FALSE == b_connected) {
			hr_ = __LastErrToHresult();
			break;
		}
		while (m_b_stop == false) {
			DWORD dw_cmd  = 0;
			DWORD dw_read = 0;
			if (::ReadFile(TChannel::m_pipe, &dw_cmd, sizeof(DWORD), &dw_read, NULL)) {
			}
			else
				break;
		}
	}
	return /*m_error*/hr_;
}

HRESULT     CPipeReader::Stop  (void) {

	if (this->IsRunning() == false)
		return (__DwordToHresult(ERROR_INVALID_STATE));

	if (::DisconnectNamedPipe(TChannel::m_pipe) == FALSE)
		return __LastErrToHresult();
	else
		m_b_stop = true;

	return S_OK;
}

/////////////////////////////////////////////////////////////////////////////