/*
	Created by Tech_dog (ebontrop@gmail.com) on 16-Dec-2019 at 12:39:46p, UTC+7, Novosibirsk, Tulenina, Monday;
	This is FakeGPS named pipe generic interface(s) implementation file;
*/
#include "StdAfx.h"
#include "fake.gps.pps.defs.h"

using namespace shared::drv::um;

/////////////////////////////////////////////////////////////////////////////

CPipeLocator:: CPipeLocator(void) {}
CPipeLocator::~CPipeLocator(void) {}

/////////////////////////////////////////////////////////////////////////////

VOID        CPipeLocator::Clear (void) {
	::memset(m_uri, 0, sizeof(TCHAR) * _countof(m_uri));
}

LPCTSTR     CPipeLocator::URI   (void) const { return m_uri;   }
HRESULT     CPipeLocator::URI   (LPCTSTR _lp){

	if (NULL == _lp || 0 == ::_tcslen(_lp))
		return (E_INVALIDARG);

	static LPCTSTR lp_sz_pat = _T("\\\\.\\pipe\\LOCAL\\%s");

	::_stprintf_s(m_uri, _countof(m_uri), lp_sz_pat, _lp);

	return S_OK;

}

/////////////////////////////////////////////////////////////////////////////

CPipeLocator::operator LPCTSTR  (void) const { return this->URI(); }

/////////////////////////////////////////////////////////////////////////////

CPipeLocator& CPipeLocator::operator = (LPCTSTR _lp_sz_name) { this->URI(_lp_sz_name); return *this; }

/////////////////////////////////////////////////////////////////////////////

CPipeMode:: CPipeMode(const e_mode _mode) : m_current(_mode) {}
CPipeMode::~CPipeMode(void) {}

/////////////////////////////////////////////////////////////////////////////

const bool CPipeMode::IsBin(void) const  { return (TPipeMode::e_bin_mode == m_current); }
const bool CPipeMode::IsMsg(void) const  { return (TPipeMode::e_msg_mode == m_current); }
const
TPipeMode  CPipeMode::Mode (void) const  { return (m_current); }
VOID       CPipeMode::Mode (const e_mode _mode) { this->m_current = _mode; }