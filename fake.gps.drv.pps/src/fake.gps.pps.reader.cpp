/*
	Created by Tech_dog (ebontrop@gmail.com) on 17-Dec-2019 at 4:45:28a, UTC+7, Novosibirsk, Tulenina, Tuesday;
	This is FakeGPS named pipe generic data reader interface implementation file;
*/
#include "StdAfx.h"
#include "fake.gps.pps.reader.h"

using namespace shared::drv::um;
/////////////////////////////////////////////////////////////////////////////

namespace shared { namespace drv { namespace um { namespace _impl {

	DWORD WINAPI _thread_fun(CPipeReader* _p_rdr){
		if (NULL == _p_rdr)
			return ERROR_INVALID_ADDRESS;
		else
			return _p_rdr->Thread_Fun();
	}

	class CPipeRead_Event {
	private:
		HANDLE&    m_evt_ref;
	public:
		CPipeRead_Event (HANDLE& _evt_ref) : m_evt_ref(_evt_ref) {
			m_evt_ref = ::CreateEvent(NULL, TRUE, FALSE, NULL);
		}
		~CPipeRead_Event (void) {
			if (m_evt_ref) {
				::CloseHandle(m_evt_ref); m_evt_ref = NULL;
			}
		}
	};
}}}}
using namespace shared::drv::um::_impl;
/////////////////////////////////////////////////////////////////////////////

CPipeReader:: CPipeReader(IPipeEvents& _evts, IPipeTrace& _trace) :
	m_break_it_on(false), m_thread(NULL), m_events(_evts), m_trace(_trace), m_received(0) { this->Clear(); }
CPipeReader::~CPipeReader(void) {}

/////////////////////////////////////////////////////////////////////////////

HRESULT   CPipeReader::Clear(void) {
	::memset(m_buffer, 0, _countof(m_buffer)); m_received = 0;
	return S_OK;
}

HRESULT   CPipeReader::Get  (const HANDLE _pipe) {

	if (NULL == _pipe)
		return E_INVALIDARG;
	if (NULL != m_thread)
		return __DwordToHresult(ERROR_BUSY);

	HRESULT hr_ = S_OK;

	this->Clear();
	m_break_it_on = false; m_pipe = _pipe;
	DWORD dw_id_  = 0;

	m_thread = ::CreateThread(NULL, 0, (LPTHREAD_START_ROUTINE)_thread_fun, this, 0, &dw_id_);
	if (NULL == m_thread)
		hr_  = __DwordToHresult(::GetLastError());
	else
		m_trace.IPipeTrace_PutInfo("CPipeReader::Get(): has started for reading data;");

	return hr_;
}

HRESULT   CPipeReader::Stop (void) {

	if (m_thread == NULL)
		return (OLE_E_BLANK);

	m_break_it_on = true;

	::WaitForSingleObject(m_thread, INFINITE);
	::CloseHandle(m_thread);

	m_thread = NULL;
	m_pipe   = NULL;

	m_trace.IPipeTrace_PutInfo("CPipeReader::Stop(): data read is stopped;");

	return S_OK;
}

/////////////////////////////////////////////////////////////////////////////

/////////////////////////////////////////////////////////////////////////////
DWORD         CPipeReader::Thread_Fun(void) {

	DWORD dError    = ERROR_SUCCESS;
	m_trace.IPipeTrace_PutInfo("CPipeReader::Thread_Fun()::begin();");

	OVERLAPPED over_ = {0};
	CPipeRead_Event evt_wait(over_.hEvent);
	if (NULL == over_.hEvent)
		return (__LastErrToHresult());
#if (0)
	const BOOL b_result = ::ReadFile(
		m_pipe, m_buffer, _countof(m_buffer) * sizeof(CHAR), &dReceived, &over_
	);

	if (b_result) dError = ERROR_SUCCESS;
	else {
		dError = ::GetLastError();
		if (dError  = ERROR_MORE_DATA)
			dError  = ERROR_SUCCESS;
		b_states[0] = true;
	}
#endif
	HRESULT hr_ = S_OK;
	bool b_states[3] = { false }; // {0:pending|1:complete|2:waiting};

	bool b_not_complete = false;
	bool b_not_pending  = false;

	while (m_break_it_on == false && dError == 0) {

		::Sleep(10);

		if (b_states[0]) { // in pendig state
			DWORD nBytesRead = 0;
			if (::GetOverlappedResult(m_pipe, &over_, &nBytesRead, FALSE) == FALSE) {

				dError = ::GetLastError();
				
				if (ERROR_IO_INCOMPLETE  != dError && ERROR_IO_PENDING != dError) {
					hr_ = __LastErrToHresult();
					m_trace.IPipeTrace_PutError("CPipeReader::Thread_Fun()::overlap: failure code=0x%x;", hr_);
					break;
				}
			}
			else {
				b_states[1] = true; break; // completed;
			}
		}
		else if (b_states[2]) { // in waiting state

			const DWORD dwResult = ::WaitForSingleObject(over_.hEvent, 10);
			b_states[2]  = (WAIT_OBJECT_0 != dwResult); // in wait state;
			b_states[0]  = (WAIT_OBJECT_0 == dwResult); // in pending state;
		}
		else {
			DWORD d_read = 0;
			if (::ReadFile(m_pipe, m_buffer, _countof(m_buffer) * sizeof(CHAR), &d_read, &over_) == FALSE) {

				dError = ::GetLastError();
				switch (dError) {
				case ERROR_IO_INCOMPLETE: {
					if (b_not_complete == false) {
						b_not_complete  = true ;
						m_trace.IPipeTrace_PutInfo("CPipeReader::Thread_Fun()::read: incomplete state;"); dError = 0;
					}} break;
				case ERROR_IO_PENDING   : {
					if (b_not_pending  == false) {
						b_not_pending   = true ;
						m_trace.IPipeTrace_PutInfo("CPipeReader::Thread_Fun()::read: pending state;"); dError = 0;
					}} break;
				}

				if (dError) {
					hr_ = __LastErrToHresult();
					m_trace.IPipeTrace_PutError("CPipeReader::Thread_Fun()::read: failure code=0x%x;", hr_);
					break;
				}
				b_states[2] = true; // wait state;
			}
			else {
				b_states[1] = true; // completed ;
				m_received  = over_.Offset;
				break;
			}
		}
	}
	if (dError == ERROR_SUCCESS) {
	m_events.IPipeEvent_OnRead((LPBYTE)m_buffer, m_received);
	m_trace .IPipeTrace_PutInfo ("CPipeReader::Thread_Fun()::received: %d byte(s)", m_received); } else
	m_trace .IPipeTrace_PutError("CPipeReader::Thread_Fun()::error: 0x%x", __DwordToHresult(dError));
	m_trace .IPipeTrace_PutInfo ("CPipeReader::Thread_Fun()::end();");
	return dError;
}