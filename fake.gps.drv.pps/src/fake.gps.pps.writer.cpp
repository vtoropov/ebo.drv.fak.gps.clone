/*
	Created by Tech_dog (ebontrop@gmail.com) on 18-Dec-2019 at 2:18:53a, UTC+7, Novosibirsk, Tulenina, Wednesday;
	This is FakeGPS named pipe generic data writer interface implementation file;
*/
#include "StdAfx.h"
#include "fake.gps.pps.writer.h"

using namespace shared::drv::um;
/////////////////////////////////////////////////////////////////////////////

namespace shared { namespace drv { namespace um { namespace _impl {

	DWORD WINAPI _thread_fun(CPipeWriter* _p_wrt){
		if (NULL == _p_wrt)
			return ERROR_INVALID_ADDRESS;
		else
			return _p_wrt->Thread_Fun();
	}

	class CPipeWrite_Event {
	private:
		HANDLE&    m_evt_ref;
	public:
		CPipeWrite_Event (HANDLE& _evt_ref) : m_evt_ref(_evt_ref) {
			m_evt_ref = ::CreateEvent(NULL, TRUE, FALSE, NULL);
		}
		~CPipeWrite_Event (void) {
			if (m_evt_ref) {
				::CloseHandle(m_evt_ref); m_evt_ref = NULL;
			}
		}
	public:
		bool Occurred(void) const {

		}
	};
}}}}
using namespace shared::drv::um::_impl;
/////////////////////////////////////////////////////////////////////////////

CPipeWriter:: CPipeWriter(IPipeEvents& _evts, IPipeTrace& _trace) :
	m_break_it_on(false), m_thread(NULL), m_events(_evts), m_trace(_trace), m_written(0) { this->Clear(); }
CPipeWriter::~CPipeWriter(void) {}

/////////////////////////////////////////////////////////////////////////////

HRESULT   CPipeWriter::Clear(void) {
	::memset(m_buffer, 0, _countof(m_buffer)); m_written = 0;
	return S_OK;
}

HRESULT   CPipeWriter::Copy (const PBYTE _p_data, const DWORD _dw_size) {
	if (NULL == _p_data || 0 == _dw_size)
		return (E_INVALIDARG);

	this->Clear();

	errno_t err_t = ::memcpy_s(m_buffer, _countof(m_buffer) * sizeof(CHAR), _p_data, _dw_size);
	if (err_t)
		return (E_OUTOFMEMORY);
	else
		return (S_OK);
}

HRESULT   CPipeWriter::Put  (const HANDLE _pipe, const PBYTE _p_data, const DWORD _dw_size) {

	if (NULL == _pipe   ) return __DwordToHresult(ERROR_INVALID_HANDLE);
	if (NULL != m_thread) return __DwordToHresult(ERROR_BUSY);

	HRESULT hr_ = this->Copy(_p_data, _dw_size);
	if (FAILED(hr_))
		return hr_;

	m_break_it_on = false; m_pipe = _pipe;
	DWORD dw_id_  = 0;

	m_thread = ::CreateThread(NULL, 0, (LPTHREAD_START_ROUTINE)_thread_fun, this, 0, &dw_id_);
	if (NULL == m_thread)
		hr_  = __DwordToHresult(::GetLastError());
	else
		m_trace.IPipeTrace_PutInfo("CPipeWriter::Put(): has started for writing data;");

	return hr_;
}

HRESULT   CPipeWriter::Put  (const HANDLE _pipe,  LPCSTR  _lp_sz_data) {
	HRESULT hr_ = this->Put (_pipe, (PBYTE)_lp_sz_data, static_cast<DWORD>(::strlen(_lp_sz_data)));
	return  hr_;
}

HRESULT   CPipeWriter::Stop (void) {

	if (m_thread == NULL)
		return (OLE_E_BLANK);

	m_break_it_on = true;

	::WaitForSingleObject(m_thread, INFINITE);
	::CloseHandle(m_thread);

	m_thread = NULL;
	m_pipe   = NULL;

	m_trace.IPipeTrace_PutInfo("CPipeWriter::Stop(): data write is stopped;");

	return S_OK;
}

/////////////////////////////////////////////////////////////////////////////

/////////////////////////////////////////////////////////////////////////////
DWORD         CPipeWriter::Thread_Fun(void) {

	DWORD dError    = ERROR_SUCCESS;
	m_trace.IPipeTrace_PutInfo("CPipeWriter::Thread_Fun()::begin();");

	OVERLAPPED over_ = {0};
	CPipeWrite_Event evt_wait(over_.hEvent);
	if (NULL == over_.hEvent)
		return (__LastErrToHresult());

	HRESULT hr_ = S_OK;
	bool b_states[3] = { false }; // {0:pending|1:complete|2:waiting};

	bool b_not_complete = false;
	bool b_not_pending  = false;

	while (m_break_it_on == false) {

		::Sleep(10);

		if (b_states[0]) { // in pendig state
			DWORD nBytesRead = 0;
			if (::GetOverlappedResult(m_pipe, &over_, &nBytesRead, FALSE) == FALSE) {

				dError = ::GetLastError();
				switch (dError) {
				case ERROR_IO_INCOMPLETE: { dError = 0; } break;
				case ERROR_IO_PENDING   : { dError = 0; } break;
				}
				
				if (dError) {
					hr_ = __LastErrToHresult();
					m_trace.IPipeTrace_PutError("CPipeWriter::Thread_Fun()::overlap: failure code=0x%x;", hr_);
					break;
				}
			}
			else {
				b_states[0] = false;
				b_states[1] = true ; continue; // wait/pending is completed, it's necessary to write data;
			}
		}
		else if (b_states[2]) { // in waiting state

			const DWORD dwResult = ::WaitForSingleObject(over_.hEvent, 10);
			b_states[2]  = (WAIT_OBJECT_0 != dwResult); // in wait state;
			b_states[0]  = (WAIT_OBJECT_0 == dwResult); // in pending state;
		}
		else {
			if (::WriteFile(m_pipe, m_buffer, _countof(m_buffer) * sizeof(CHAR), NULL, &over_) == FALSE) {

				dError = ::GetLastError();
				switch (dError) {
				case ERROR_IO_INCOMPLETE: {
					if (b_not_complete == false) {
						b_not_complete  = true ;
						m_trace.IPipeTrace_PutInfo("CPipeWriter::Thread_Fun()::write: incomplete state;");
					} dError = 0; } break;
				case ERROR_IO_PENDING   : {
					if (b_not_pending  == false) {
						b_not_pending   = true ;
						m_trace.IPipeTrace_PutInfo("CPipeWriter::Thread_Fun()::write: pending state;");
					} dError = 0; } break;
				}

				if (dError) {
					hr_ = __DwordToHresult(dError);
					m_trace.IPipeTrace_PutError("CPipeWriter::Thread_Fun()::write: failure code=0x%x;", hr_);
					break;
				}
				b_states[0] = true; // wait state;
			}
			else {
				b_states[1] = true; // completed ;
				m_written  = over_.Offset;
				break;
			}
		}
	}
	if (dError == ERROR_SUCCESS) {
	m_events.IPipeEvent_OnWrite((LPBYTE)m_buffer, m_written);
	m_trace .IPipeTrace_PutInfo ("CPipeWriter::Thread_Fun()::written: %d byte(s)", m_written); } else
	m_trace .IPipeTrace_PutError("CPipeWriter::Thread_Fun()::error: 0x%x", __DwordToHresult(dError));
	m_trace .IPipeTrace_PutInfo ("CPipeWriter::Thread_Fun()::end();");
	return dError;
}