/*
	Created by Tech_dog (ebontrop@gmail.com) on 16-Dec-2019 at 11:10:58a, UTC+7, Novosibirsk, Tulenina, Monday;
	This is FakeGPS named pipe connection client interface implementation file;
*/
#include "StdAfx.h"
#include "fake.gps.pps.client.h"

using namespace shared::drv::um;
/////////////////////////////////////////////////////////////////////////////

namespace shared { namespace drv { namespace um { namespace _impl {

	DWORD WINAPI _thread_fun(CPipeClient* _p_cli){
		if (NULL == _p_cli)
			return ERROR_INVALID_ADDRESS;
		else
			return _p_cli->Thread_Fun();
	}

}}}}
using namespace shared::drv::um::_impl;
/////////////////////////////////////////////////////////////////////////////

CPipeClient:: CPipeClient (INamedPipeTrace& _trace) : m_trace(_trace), m_break_it_on(false), m_thread(NULL), m_pipe(_trace) {}
CPipeClient::~CPipeClient (void) {}

/////////////////////////////////////////////////////////////////////////////

bool          CPipeClient::IsOn (void) const { return NULL != m_thread; }
HRESULT       CPipeClient::Turn (const bool _b_on ) {

	if (_b_on == true  && this->IsOn() == true ) return (__DwordToHresult(ERROR_INVALID_STATE));
	if (_b_on == false && this->IsOn() == false) return (__DwordToHresult(ERROR_INVALID_STATE)); HRESULT hr_ = S_OK;

	if (_b_on) {

		m_break_it_on = false;
		DWORD dw_id_  = 0;

		m_thread = ::CreateThread(NULL, 0, (LPTHREAD_START_ROUTINE)_thread_fun, this, 0, &dw_id_);
		if (NULL == m_thread)
			hr_  = __DwordToHresult(::GetLastError());
		else
			m_trace.IPipeTrace_PutInfo("CPipeClient::Turn(): has started for performing queries to server;");
	}
	else {
		m_break_it_on = true;

		::WaitForSingleObject(m_thread, INFINITE);
		::CloseHandle(m_thread);

		m_pipe.Destroy();

		m_trace.IPipeTrace_PutInfo("CPipeClient::Turn(): named pipe is closed;");

		m_thread = NULL;
		m_trace.IPipeTrace_PutInfo("CPipeClient::Turn(): has stopped;");
	}

	return  hr_;
}

/////////////////////////////////////////////////////////////////////////////

/////////////////////////////////////////////////////////////////////////////

DWORD CPipeClient::Thread_Fun(void) {

	m_trace.IPipeTrace_PutInfo ("CPipeClient::Thread_Fun()::begin();");

	while (false == m_break_it_on)  {

		::Sleep(10);
	}

	m_trace.IPipeTrace_PutInfo ("CPipeClient::Thread_Fun()::end();");

	return NO_ERROR;
}