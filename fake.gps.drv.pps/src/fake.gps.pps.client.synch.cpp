/*
	Created by Tech_dog (ebontrop@gmail.com) on 20-Dec-2019 at 05:34:16a, UTC+7, Novosibirsk, Tulenina, Friday;
	This is FakeGPS named pipe synchronous query client interface implementation file;
*/
#include "StdAfx.h"
#include "fake.gps.pps.client.synch.h"
#include "fake.gps.acc.sec.h"
#include "fake.gps.cmd.time.h"

using namespace shared::drv::um;

/////////////////////////////////////////////////////////////////////////////

CPipeClient_Synch:: CPipeClient_Synch(IPipeTrace& _trace, const TPipeMode _mode) : 
	m_trace(_trace),
	m_pipe(NULL)   ,
	m_state(TPipeState_2::e_connect), m_mode(_mode), m_pending(false), m_break_it_on(false) {
	
	this->Clear();

	if (m_mode.IsBin()) m_trace.IPipeTrace_PutInfo ("CPipeClient_Synch::construct(): binary mode is on;");
	if (m_mode.IsMsg()) m_trace.IPipeTrace_PutInfo ("CPipeClient_Synch::construct(): message mode is on;");
}
CPipeClient_Synch::~CPipeClient_Synch(void) {}

/////////////////////////////////////////////////////////////////////////////

bool          CPipeClient_Synch::IsOn (void) const { return (NULL != m_pipe); }
HRESULT       CPipeClient_Synch::Turn (const bool _b_on ) {
	HRESULT hr_ = S_OK;

	if (_b_on) {

		hr_ = this->Connect();
		if (FAILED(hr_))
			return hr_;
		
		DWORD d_mode = PIPE_READMODE_MESSAGE;
		const bool b_mode = !!::SetNamedPipeHandleState(
				m_pipe, &d_mode, NULL, NULL
			);

		if (b_mode == false) {
			hr_ = __LastErrToHresult(); 
			m_trace.IPipeTrace_PutError ("CPipeClient_Synch::Turn(): set mode error=0x%x", hr_);
			return hr_;
		}

		hr_ = this->Request();
		if (FAILED(hr_))
			return hr_;

		m_trace.IPipeTrace_PutInfo ("CPipeClient_Synch::Turn(): receiving data...");

		bool  b_read = false;

		do {
			b_read = !!::ReadFile(
					m_pipe, m_response, _countof(m_response) * sizeof(TCHAR), &m_read, NULL
				);
			const DWORD dError = ::GetLastError();
			if (b_read == false && dError != ERROR_MORE_DATA) {
				hr_ = __DwordToHresult(dError);
				m_trace.IPipeTrace_PutError ("CPipeClient_Synch::Turn(): read error=0x%x", hr_);
				break;
			}
			else if ( b_read == true ) {
				m_trace.IPipeTrace_PutInfo  ("CPipeClient_Synch::Turn(): read success;");
#if (0)
				CCmd_Time_t time_;
				if (time_.Is ((LPBYTE)m_response, m_read)) {
					time_.Set((LPBYTE)m_response, m_read);
					DWORD d_buf_len = CCmd_Time_t::eFormattedBufferSize;

					CHAR s_time[CCmd_Time_t::eFormattedBufferSize] = {0};
					time_.Text((LPBYTE)s_time, d_buf_len);

					m_trace.IPipeTrace_PutInfo("CPipeClient_Synch::Turn()::received: time command; %d byte(s);", m_read);
					m_trace.IPipeTrace_PutInfo("CPipeClient_Synch::Turn()::time command content: %s;", s_time);
				}
#else
				CCmd_Geo_loc geo_loc;
				if (geo_loc.Is ((LPBYTE)m_response, m_read)) {
					geo_loc.Set((LPBYTE)m_response, m_read);
					DWORD d_buf_len = CCmd_Geo_loc_fmt::e__double_buf_sz;

					CHAR s_geo[CCmd_Geo_loc_fmt::e__double_buf_sz] = {0};
					geo_loc.Text((LPBYTE)s_geo, d_buf_len);

					m_trace.IPipeTrace_PutInfo("CPipeClient_Synch::Turn()::received: geo command; %d byte(s);", m_read);
					m_trace.IPipeTrace_PutInfo("CPipeClient_Synch::Turn()::geo command content: %s;", s_geo);
				}
#endif
				else {
					CAtlStringA cs_msg(m_response);
					CAtlStringA cs_pat; cs_pat.Format (
							"CPipeClient_Synch::received: %d byte(s); %s", m_read, cs_msg.GetString()
						);
					m_trace.IPipeTrace_PutInfo  (cs_pat.GetString());
				}
			}
		}
		while (b_read == false && m_break_it_on == false);

	}
	else {
		hr_ = this->Disconnect();
	}

	return  hr_;
}

/////////////////////////////////////////////////////////////////////////////

const
CCmd_Geo_loc& CPipeClient_Synch::Cmd_geo(void) const { return m_geo_cmd; }
CCmd_Geo_loc& CPipeClient_Synch::Cmd_geo(void)       { return m_geo_cmd; }


/////////////////////////////////////////////////////////////////////////////

HRESULT       CPipeClient_Synch::Clear(void) {
	HRESULT hr_ = S_OK;
	::memset( m_request , 0, sizeof(TCHAR) * _countof(m_request ));
	::memset( m_response, 0, sizeof(TCHAR) * _countof(m_response));
	::memset(&m_over    , 0, sizeof(OVERLAPPED));
	return  hr_;
}

HRESULT       CPipeClient_Synch::Connect(void) {
	HRESULT hr_ = S_OK;

	CPipeLocator locator_;
	hr_ = locator_.URI(_T("FakeGPS_overlap"));

	const CPipeAccess acc_;
	if (acc_.Is() == false)
		return ((hr_ = __DwordToHresult(ERROR_INVALID_SECURITY_DESCR)));

	while (m_break_it_on == false) {

		m_pipe = ::CreateFile(
			locator_.URI(), GENERIC_READ|GENERIC_WRITE, 0, acc_, OPEN_EXISTING, 0, NULL
		);
		if (m_pipe != INVALID_HANDLE_VALUE) {
			m_trace.IPipeTrace_PutInfo ("CPipeClient_Synch::Connect(): success;"); m_state = TPipeState_2::e_writing;
			break;
		}

		const DWORD dError = ::GetLastError();

		switch (dError) {
		case ERROR_PIPE_BUSY: {
				if (::WaitNamedPipe(locator_.URI(), CPipeClient_Cfg::con_wait) == FALSE) {
					hr_ = __DwordToHresult(dError); 
					m_trace.IPipeTrace_PutWarn("CPipeClient_Synch::Connect(): pipe is buzy; error=0x%x", hr_);
					return hr_;
				} 
			} break;
		default:
			hr_ = __DwordToHresult(dError); 
			m_trace.IPipeTrace_PutError ("CPipeClient_Synch::Connect(): system error=0x%x", hr_);
			return (hr_);
		}
	}

	return  hr_;
}

HRESULT       CPipeClient_Synch::Disconnect(void) {
	HRESULT hr_ = S_OK;
	if (this->IsOn()) {

		const bool b_closed = !!::CloseHandle(m_pipe);
		if (b_closed == FALSE) {
			hr_ = __LastErrToHresult(); 
			m_trace.IPipeTrace_PutError("CPipeClient_Synch::Disconnect(): error=0x%x", hr_);
			return (hr_);
		}
	}
	return  hr_;
}

#include <strsafe.h>

HRESULT       CPipeClient_Synch::Request  (void) {

	HRESULT hr_ = S_OK; DWORD d_write = 0;

	if (m_mode.IsMsg()) {

		static LPCTSTR lp_def_req = _T("Default request;");

		::StringCchCopy(
			m_request, _countof(m_request), lp_def_req
		);
		d_write  = (::lstrlen(lp_def_req) + 1) * sizeof(TCHAR);
	}
	else if (m_mode.IsBin()) {
#if (0)
		CCmd_Time_t time_; time_.Local();
		hr_ = time_.Copy((LPBYTE)m_request , (d_write = _countof(m_request) * sizeof(TCHAR)));
		if (FAILED(hr_)) {
		m_trace.IPipeTrace_PutError("CPipeClient_Synch::Request(): time command copy error=0x%x;", hr_);
		}
#else
		hr_ = m_geo_cmd.Copy((LPBYTE)m_request, (d_write = _countof(m_request) * sizeof(TCHAR)));
		if (FAILED(hr_)) {
		m_trace.IPipeTrace_PutError("CPipeClient_Synch::Request(): geo command copy error=0x%x;", hr_);
		}
#endif
	}

	const bool b_written = !!::WriteFile(
			m_pipe, m_request, d_write, &m_write, NULL
		);
	if (false == b_written) {
		hr_ = __LastErrToHresult(); 
		m_trace.IPipeTrace_PutError("CPipeClient_Synch::Request(): write error=0x%x", hr_);
	}
	else
		m_state = TPipeState_2::e_reading;

	return  hr_;
}