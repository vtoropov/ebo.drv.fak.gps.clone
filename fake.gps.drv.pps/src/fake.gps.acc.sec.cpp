/*
	Created by Tech_dog (ebontrop@gmail.com) on 2-Dec-2019 at 1:12:56a, UTC+7, Novosibirsk, Tulenina, Monday;
	This is Ebo Drv UMDF named pipe access security interface implementation file;
	-----------------------------------------------------------------------------
	Adopted to FakeGPS project on 16-Dec-2019 at 0:53:32a, UTC+7, Novosibirsk, Tulenina, Monday;
*/
#include "StdAfx.h"
#include "fake.gps.acc.sec.h"

using namespace shared::drv::um;

/////////////////////////////////////////////////////////////////////////////

CPipeAccess:: CPipeAccess(void) {
#if (0)
	m_error << __MODULE__ << S_OK >> __MODULE__;
#endif
	const DWORD dw_sz_ = sizeof(SECURITY_ATTRIBUTES);
	::memset(&m_sec_atts, 0, dw_sz_);

	m_sec_atts.nLength = dw_sz_;
	m_sec_atts.bInheritHandle = FALSE;

	static LPCTSTR lp_sz_acc = _T(
			"D:"                     // Discretionary ACL;
			"(D;OICI;GA;;;BG)"       // Deny access to built-in guests;
			"(D;OICI;GA;;;AN)"       // Deny access to anonymous logon;
			"(A;OICI;GRGWGX;;;AU)"   // Allow read/write/execute to authenticated users;
			"(A;OICI;GA;;;BA)"       // Allow full control to administrators;
		);

	const BOOL b_result = ::ConvertStringSecurityDescriptorToSecurityDescriptor(
			lp_sz_acc, SDDL_REVISION_1, &m_sec_atts.lpSecurityDescriptor, NULL
		);
#if (0)
	if (FALSE == b_result)
		m_error = __LastErrToHresult();
#endif
}
CPipeAccess::~CPipeAccess(void) {}

/////////////////////////////////////////////////////////////////////////////
#if (0)
TErrorRef     CPipeAccess::Error      (void) const { return  m_error; }
#endif
const bool    CPipeAccess::Is         (void) const { return  m_sec_atts.lpSecurityDescriptor != NULL; }
const
SECURITY_ATTRIBUTES* CPipeAccess::Ptr (void) const { return &m_sec_atts; }
const
SECURITY_ATTRIBUTES& CPipeAccess::Ref (void) const { return  m_sec_atts; }
SECURITY_ATTRIBUTES& CPipeAccess::Ref (void)       { return  m_sec_atts; }

/////////////////////////////////////////////////////////////////////////////
CPipeAccess::operator const
SECURITY_ATTRIBUTES&     (void) const { return  this->Ref(); }
CPipeAccess::operator
LPSECURITY_ATTRIBUTES    (void) const { return (LPSECURITY_ATTRIBUTES)&this->m_sec_atts; }

/////////////////////////////////////////////////////////////////////////////