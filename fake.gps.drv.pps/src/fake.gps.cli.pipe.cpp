/*
	Created by Tech_dog (ebontrop@gmail.com) on 16-Dec-2019 at 12:44:41p, UTC+7, Novosibirsk, Tulenina, Monday;
	This is FakeGPS client pipe interface implementation file;
*/
#include "StdAfx.h"
#include "fake.gps.cli.pipe.h"

using namespace shared::drv::um;
/////////////////////////////////////////////////////////////////////////////

namespace shared { namespace drv { namespace um { namespace _impl {

	DWORD WINAPI _thread_fun(CClientPipe* _p_pip){
		if (NULL == _p_pip)
			return ERROR_INVALID_ADDRESS;
		else
			return _p_pip->Thread_Fun();
	}

}}}}
using namespace shared::drv::um::_impl;
/////////////////////////////////////////////////////////////////////////////

CClientPipe:: CClientPipe (IPipeEvents& _evts, IPipeTrace& _trace) : 
	m_trace(_trace), m_pipe(NULL), m_thread(NULL), m_timeout(0), m_evnts(_evts) {}
CClientPipe::~CClientPipe (void) {}

/////////////////////////////////////////////////////////////////////////////

const bool    CClientPipe::Connected (void) const { return this->Is(); }
HRESULT       CClientPipe::Create    (PCTSTR _lp_sz_name, const DWORD _wait_msec) {

	HRESULT hr_ = m_loc.URI(_lp_sz_name);
	if (FAILED(hr_))
		return(hr_);

	if (this->Is() == true)
		return (__DwordToHresult(ERROR_ALREADY_INITIALIZED));

	m_break_it_on = false; m_timeout = _wait_msec;
	DWORD dw_id_  = 0;

	m_thread = ::CreateThread(NULL, 0, (LPTHREAD_START_ROUTINE)_thread_fun, this, 0, &dw_id_);
	if (NULL == m_thread)
		hr_  = __DwordToHresult(::GetLastError());
	else
		m_trace.IPipeTrace_PutInfo("CPipeClient::Turn(): has started for performing queries to server;");
	
	return hr_;
}

HRESULT       CClientPipe::Destroy   (void) {
	m_break_it_on = true;

	::WaitForSingleObject(m_thread, INFINITE);
	::CloseHandle(m_thread);

	m_thread = NULL;

	if (this->Is() == false)
		return (__DwordToHresult(ERROR_INVALID_STATE));
	
	if (::CloseHandle(m_pipe) == FALSE)
		return (__LastErrToHresult());
	else
		m_pipe = NULL;

	m_trace.IPipeTrace_PutInfo("CClientPipe::Destroy(): client pipe is destroyed;");

	return S_OK;
}

HANDLE        CClientPipe::Handle    (void) const { return m_pipe; }
const bool    CClientPipe::Is        (void) const { return m_pipe !=  NULL && m_pipe != INVALID_HANDLE_VALUE; }
TLocatorRef   CClientPipe::Locator   (void) const { return m_loc ; }

/////////////////////////////////////////////////////////////////////////////

CClientPipe::operator HANDLE (void) const { return this->Handle(); }

/////////////////////////////////////////////////////////////////////////////

DWORD         CClientPipe::Thread_Fun(void) {

	DWORD dError = ERROR_SUCCESS;
	m_trace.IPipeTrace_PutInfo("CClientPipe::Thread_Fun()::begin();");

	bool b_not_found = false;
	bool b_not_free  = false;

	while (m_break_it_on == false) {
		::Sleep(m_timeout);
		m_pipe = ::CreateFile(
			m_loc.URI(), GENERIC_READ|GENERIC_WRITE, 0, NULL, OPEN_EXISTING, FILE_FLAG_OVERLAPPED, NULL
		);
		if (m_pipe != INVALID_HANDLE_VALUE)
			break;
		dError = ::GetLastError();

		switch (dError) {
		case ERROR_PIPE_BUSY     : {
			if (b_not_free  == false) {
				b_not_free   = true ;
				m_trace.IPipeTrace_PutInfo ("CClientPipe::Thread_Fun()::driver is not free;");
			} continue;
		} break;
		case ERROR_FILE_NOT_FOUND: {
				if (b_not_found == false) {
					b_not_found  = true ;
					m_trace.IPipeTrace_PutInfo ("CClientPipe::Thread_Fun()::driver is not loaded;");
				} continue;
			} break;
		}
		if (dError != NO_ERROR)
			break;

		if (::WaitNamedPipe(m_loc.URI(), m_timeout))
			break;
		dError = ::GetLastError();
		if (dError == ERROR_SEM_TIMEOUT)
			continue;
		else {
			break;
		}
	}
	if (m_pipe != INVALID_HANDLE_VALUE) {
	m_evnts.IPipeEvent_OnCreate(m_loc.URI());
	m_trace.IPipeTrace_PutInfo ("CClientPipe::Thread_Fun()::connected;"); } else
	m_trace.IPipeTrace_PutWarn("CClientPipe::Thread_Fun()::connect error: 0x%x", __DwordToHresult(dError));
	m_trace.IPipeTrace_PutInfo ("CClientPipe::Thread_Fun()::end();");
	return dError;
}