#ifndef _STDAFX_4D66287F_C16B_43e5_919D_DADFB8E00745_INCLUDED
#define _STDAFX_4D66287F_C16B_43e5_919D_DADFB8E00745_INCLUDED
/*
	Created by Tech_dog (ebontrop@gmail.com) on 16-Aug-2016 at 5:56:22a, GMT+7, Phuket, Rawai, Tuesday;
	This is Evalu8 Outlook add-in precompiled header include file.
	-----------------------------------------------------------------------------
	Adopted to ebo.out.not.goo.lib project on 7-Sep-2019 at 11:26:53a, UTC+7, Novosibirsk, Tulenina, Saturday;
	Adopted to Ebo Drv UMDF communicate project on 01-Dec-2019 at 10:58:37p, UTC+7, Novosibirsk, Tulenina, Sunday;
	Adopted to FakeGPS project on 16-Dec-2019 at 0:53:33a, UTC+7, Novosibirsk, Tulenina, Monday;
*/
#include "fake.gps.pipes.ver.h"

#ifndef STRICT
#define STRICT
#endif

#ifndef WIN32_LEAN_AND_MEAN
#define WIN32_LEAN_AND_MEAN
#endif

#pragma warning(disable: 4481)  // nonstandard extension used: override specifier 'override'
#pragma warning(disable: 4996)  // security warning: function or variable may be unsafe

#define _ATL_APARTMENT_THREADED
#define _ATL_NO_AUTOMATIC_NAMESPACE

#include <atlbase.h>
#include <atlcom.h>
#include <comdef.h>
#include <atlstr.h>
#include <atlsafe.h>

#ifdef _DEBUG
	#define _ATL_DEBUG_INTERFACES
	#define _CRTDBG_MAP_ALLOC
	#include <stdlib.h>
	#include <crtdbg.h>
#endif

#include <vector>
#include <map>

#include <Sddl.h>

using namespace ::ATL;

#define __DwordToHresult(err_code)  HRESULT_FROM_WIN32(err_code)
#define __LastErrToHresult()        HRESULT_FROM_WIN32(::GetLastError())

#endif/*_STDAFX_4D66287F_C16B_43e5_919D_DADFB8E00745_INCLUDED*/