/*
	Created by Tech_dog (ebontrop@gmail.com) on 16-Aug-2016 at 6:20:45a, GMT+7, Phuket, Rawai, Tuesday;
	This is Evalu8 Outlook add-in precompiled headers implementation file.
	-----------------------------------------------------------------------------
	Adopted to ebo.out.not.goo.lib project on 6-Sep-2019 at 11:31:15a, UTC+7, Novosibirsk, Tulenina, Saturday;
	Adopted to Ebo Drv UMDF communicate project on 01-Dec-2019 at 10:59:26p, UTC+7, Novosibirsk, Tulenina, Sunday;
	Adopted to FakeGPS project on 16-Dec-2019 at 0:53:32a, UTC+7, Novosibirsk, Tulenina, Monday;
*/

#include "StdAfx.h"

#if (_ATL_VER < 0x0700)
#include <atlimpl.cpp>
#endif //(_ATL_VER < 0x0700)
