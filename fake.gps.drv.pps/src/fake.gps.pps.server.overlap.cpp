/*
	Created by Tech_dog (ebontrop@gmail.com) on 19-Dec-2019 at 2:03:51a, UTC+7, Novosibirsk, Tulenina, Thursday;
	This is FakeGPS named pipe overlap server interface implementation file;
*/
#include "StdAfx.h"
#include "fake.gps.pps.server.overlap.h"
#include "fake.gps.acc.sec.h"
#include "fake.gps.cmd.geo.loc.h"
#include "fake.gps.cmd.time.h"

using namespace shared::drv::um;


/////////////////////////////////////////////////////////////////////////////

namespace shared { namespace drv { namespace um { namespace _impl {

	DWORD WINAPI _thread_fun(CPipeServer_Overlap* _p_srv){
		if (NULL == _p_srv)
			return ERROR_INVALID_ADDRESS;
		else
			return _p_srv->Do_work();
	}

}}}}
using namespace shared::drv::um::_impl;
/////////////////////////////////////////////////////////////////////////////

CPipeServer_Overlap:: CPipeServer_Overlap(ICommandProcessor& _proc, IPipeTrace& _trace, const TPipeMode _mode) : 
	m_trace(_trace),
	m_pipe (NULL ) ,
	m_proc (_proc) ,
	m_state(TPipeState::e_connect), m_mode(_mode), m_pending(false), m_break_it_on(false), m_thread(NULL) {

	this->Clear();

	if (m_mode.IsBin()) m_trace.IPipeTrace_PutInfo ("CPipeServer_Overlap::construct(): binary mode is on;");
	if (m_mode.IsMsg()) m_trace.IPipeTrace_PutInfo ("CPipeServer_Overlap::construct(): message mode is on;");
}
CPipeServer_Overlap::~CPipeServer_Overlap(void) {}

/////////////////////////////////////////////////////////////////////////////

bool          CPipeServer_Overlap::IsOn (void) const { return (NULL != m_pipe); }
HRESULT       CPipeServer_Overlap::Turn (const bool _b_on ) {
	HRESULT hr_ = S_OK;

	if (_b_on) {

		CPipeLocator locator_;
		hr_ = locator_.URI(_T("FakeGPS_overlap"));

		m_over.hEvent = ::CreateEvent(NULL, TRUE, TRUE, NULL);
		if (NULL == m_over.hEvent) {
			return (hr_ = __LastErrToHresult());
		}

		const CPipeAccess acc_;
		if (acc_.Is() == false)
			return ((hr_ = __DwordToHresult(ERROR_INVALID_SECURITY_DESCR)));

		m_pipe = ::CreateNamedPipe(
			locator_.URI(), PIPE_ACCESS_DUPLEX|FILE_FLAG_OVERLAPPED, PIPE_TYPE_MESSAGE|PIPE_READMODE_MESSAGE|PIPE_WAIT, 1,
			sizeof(TCHAR) * _countof(m_request ),
			sizeof(TCHAR) * _countof(m_response), CPipeServer_Cfg::con_wait, acc_
			);
		if (m_pipe == INVALID_HANDLE_VALUE) {
			const DWORD d_error = ::GetLastError(); hr_ = __DwordToHresult(d_error); 
			if (d_error == ERROR_PIPE_BUSY) {
				m_trace.IPipeTrace_PutError("CPipeServer_Overlap::Turn(on)::create: pipe already exists; error=0x%x;", hr_);
			}
			else {
				m_trace.IPipeTrace_PutError("CPipeServer_Overlap::Turn(on)::create: error=0x%x;", hr_);
			}
			return (hr_);
		}
		else
			m_trace.IPipeTrace_PutInfo ("CPipeServer_Overlap::Turn(on)::create: success;");

		hr_ = this->Connect();
		if (FAILED(hr_))
			return hr_;

		DWORD dw_id_  = 0;
		m_thread = ::CreateThread(NULL, 0, (LPTHREAD_START_ROUTINE)_thread_fun, this, 0, &dw_id_);

	}
	else {
		m_break_it_on = true;

		::WaitForSingleObject(m_thread, INFINITE);
		::CloseHandle(m_thread);

		hr_ = this->Disconnect();
		::CloseHandle(m_pipe);    m_pipe = NULL;
#if (0)
		if (SUCCEEDED(hr_))
			m_trace.IPipeTrace_PutInfo ("CPipeServer_Overlap::Turn(off): success;");
		else
			m_trace.IPipeTrace_PutError("CPipeServer_Overlap::Turn(off): error=0x%x;", hr_);
#endif
	}

	return  hr_;
}

/////////////////////////////////////////////////////////////////////////////

HRESULT       CPipeServer_Overlap::Clear(void) {
	HRESULT hr_ = S_OK;
	::memset( m_request , 0, sizeof(TCHAR) * _countof(m_request ));
	::memset( m_response, 0, sizeof(TCHAR) * _countof(m_response));
	::memset(&m_over    , 0, sizeof(OVERLAPPED));
	return  hr_;
}

HRESULT       CPipeServer_Overlap::Connect(void) {
	HRESULT hr_ = S_OK;
	
	if (::ConnectNamedPipe(m_pipe, &m_over)) {
		hr_ = __LastErrToHresult(); 
		m_trace.IPipeTrace_PutError("CPipeServer_Overlap::Connect(): error=0x%x;", hr_);
		return (hr_);
	}

	const DWORD dError = ::GetLastError();

	switch (dError) {
	case ERROR_IO_PENDING     : { m_pending = true;          } break;
	case ERROR_PIPE_CONNECTED : { ::SetEvent(m_over.hEvent); } break;
	default:
		hr_ = __DwordToHresult(dError); 
		m_trace.IPipeTrace_PutError("CPipeServer_Overlap::Connect(): error=0x%x;", hr_);
	}

	if (SUCCEEDED(hr_)) {
		m_state = m_pending ? TPipeState::e_connect : TPipeState::e_reading;
	}

	return  hr_;
}

HRESULT       CPipeServer_Overlap::Disconnect(void) {
	HRESULT hr_ = S_OK;
	if (this->IsOn()) {
		if (::DisconnectNamedPipe(m_pipe) == FALSE) {
			hr_ = __LastErrToHresult(); 
			m_trace.IPipeTrace_PutError("CPipeServer_Overlap::Disconnect(): error=0x%x;", hr_);
			return (hr_);
		}
	}
	return  hr_;
}

#include <strsafe.h>

HRESULT       CPipeServer_Overlap::Response  (void) {

	HRESULT hr_ = S_OK;
	if (m_mode.IsMsg()) {

		static LPCTSTR lp_def_resp = _T("Default response;");

		::StringCchCopy(
				m_response, _countof(m_response), lp_def_resp
			);
		m_write = (::lstrlen(lp_def_resp) + 1) * sizeof(TCHAR);
	}
	else if (m_mode.IsBin()) {
#if (0)
		CCmd_Time_t time_; time_.Local();
		hr_ = time_.Copy((LPBYTE)m_response, (m_write = _countof(m_response) * sizeof(TCHAR)));
		if (FAILED(hr_)) {
			m_trace.IPipeTrace_PutError("CPipeServer_Overlap::Response(): time command copy error=0x%x;", hr_);
		}
#else
		CCmd_Geo_loc geo_loc;
		m_proc.ICmdProc_Get(geo_loc);
		hr_ = geo_loc.Copy((LPBYTE)m_response, (m_write = _countof(m_response) * sizeof(TCHAR)));
		if (FAILED(hr_)) {
			m_trace.IPipeTrace_PutError("CPipeServer_Overlap::Response(): geo command copy error=0x%x;", hr_);
		}
#endif
	}
	else {
		hr_ = __DwordToHresult(ERROR_INVALID_STATE);
		m_trace.IPipeTrace_PutError("CPipeServer_Overlap::Response(): unknown data exchange mode, 0x%x;", hr_);
	}

	return  hr_;
}

/////////////////////////////////////////////////////////////////////////////

DWORD         CPipeServer_Overlap::Do_work   (void) {

	HRESULT hr_ = S_OK;

	while (m_break_it_on == false) {
		const DWORD d_wait = ::WaitForSingleObject(m_over.hEvent, CPipeServer_Cfg::con_wait); d_wait;
		if (d_wait == WAIT_TIMEOUT) {
			continue;
		}

		if (m_pending) {
			DWORD d_trans = 0;
			const bool b_over = !!::GetOverlappedResult(m_pipe, &m_over, &d_trans, FALSE ); b_over;

			switch (m_state) {
			case TPipeState::e_connect:  {
				if (b_over == false) {
					return (hr_ = __LastErrToHresult());
				}
				else
					m_state =  TPipeState::e_reading;
			} break;
			case TPipeState::e_reading:  {
				if (b_over == false || d_trans == 0) {
					hr_ = this->Disconnect();
					hr_ = this->Connect();    continue;
				}
				m_read  = d_trans;
				m_state = TPipeState::e_writing;
#if (0)
				CCmd_Time_t time_;
				if (time_.Is ((LPBYTE)m_request, m_read)) {
					time_.Set((LPBYTE)m_request, m_read);
					DWORD d_buf_len = CCmd_Time_t::eFormattedBufferSize;

					CHAR s_time[CCmd_Time_t::eFormattedBufferSize] = {0};
					time_.Text((LPBYTE)s_time, d_buf_len);

					m_trace.IPipeTrace_PutInfo ("CPipeServer_Overlap::Do_work()::received time command: %d byte(s);", m_read);
					m_trace.IPipeTrace_PutInfo ("CPipeServer_Overlap::Do_work()::time command content: %s;", s_time);
				}
#else
				CCmd_Geo_loc geo_;
				if (geo_.Is ((LPBYTE)m_request, m_read)) {
					geo_.Set((LPBYTE)m_request, m_read);
					DWORD d_buf_len = CCmd_Geo_loc::eFormattedBufferSize;

					CHAR s_geo[CCmd_Geo_loc::eFormattedBufferSize] = {0};
					geo_.Text((LPBYTE)s_geo, d_buf_len);

					m_trace.IPipeTrace_PutInfo ("CPipeServer_Overlap::Do_work()::received geo command: %d byte(s);", m_read);
					m_trace.IPipeTrace_PutInfo ("CPipeServer_Overlap::Do_work()::geo command content: %s;", s_geo);

					m_proc.ICmdProc_Set(geo_);
				}
#endif
				else
					m_trace.IPipeTrace_PutInfo ("CPipeServer_Overlap::Do_work()::received: %d byte(s);", m_read);
			} break;
			case TPipeState::e_writing:  {
				if (b_over == false || d_trans != m_write) {
					hr_ = this->Disconnect();
					hr_ = this->Connect();    continue;
				}
				else
					m_state =  TPipeState::e_reading;
			} break;
			}
		}

		switch (m_state) {
		case TPipeState::e_reading: {
			m_read = 0;
			const bool b_read = !!::ReadFile(
				m_pipe, m_request, sizeof(TCHAR) * _countof(m_request), &m_read, &m_over
			);
			if (b_read == true && m_read != 0) {
				m_trace.IPipeTrace_PutInfo ("CPipeServer_Overlap::Do_work()::accept: %d byte(s);", m_read);
				m_pending = false;
				m_state   = TPipeState::e_writing;  continue;
			}

			const DWORD d_error = ::GetLastError();
			if (b_read != true && d_error == ERROR_IO_PENDING) {
				m_pending = true; continue;
			}

			hr_ = this->Disconnect();
			hr_ = this->Connect();

		} break;
		case TPipeState::e_writing: {

			hr_ = this->Response();

			DWORD n_written = 0;
			const bool b_write = !!::WriteFile(
				m_pipe, m_response, m_write, &n_written, &m_over
			);
			if (b_write  == true && n_written == m_write) {
				m_trace.IPipeTrace_PutInfo ("CPipeServer_Overlap::Do_work()::response: %d byte(s);", m_write);
				m_pending = false;
				m_state   = TPipeState::e_reading;  continue;
			}

			const DWORD d_error = ::GetLastError();
			if (b_write  != true && d_error == ERROR_IO_PENDING) {
				m_pending = true; continue;
			}

			hr_ = this->Disconnect();
			hr_ = this->Connect();

		} break;

		default:
			hr_ =  __DwordToHresult(ERROR_INVALID_STATE);
			m_trace.IPipeTrace_PutError("CPipeServer_Overlap::Do_work()::state: error=0x%x;", hr_);
			return hr_;
		}
	}
	return NO_ERROR;
}

/////////////////////////////////////////////////////////////////////////////

/////////////////////////////////////////////////////////////////////////////