#ifndef _EBOCOMPPSCLIENT_H_B76116BD_4050_4C7B_A6EE_49A40C3C5C6F_INCLUDED
#define _EBOCOMPPSCLIENT_H_B76116BD_4050_4C7B_A6EE_49A40C3C5C6F_INCLUDED
/*
	Created by Tech_dog (ebontrop@gmail.com) on 16-Dec-2019 at 11:04:49a, UTC+7, Novosibirsk, Tulenina, Monday;
	This is FakeGPS named pipe connection client interface declaration file;
*/
#include "fake.gps.pps.defs.h"
#include "fake.gps.cli.pipe.h"

namespace shared { namespace drv { namespace um {

	class CPipeClient {
	protected:
		INamedPipeTrace&
		              m_trace ;
		volatile bool m_break_it_on;
		HANDLE        m_thread;
		CClientPipe   m_pipe  ;     // creating client pipe is very different in comparison with server side named pipe object;

	public:
		 CPipeClient (INamedPipeTrace& _trace);
		~CPipeClient (void);

	public:
		virtual DWORD Thread_Fun(void);

	public:
		bool          IsOn (void) const;
		HRESULT       Turn (const bool _b_on );
	};

}}}

#endif/*_EBOCOMPPSCLIENT_H_B76116BD_4050_4C7B_A6EE_49A40C3C5C6F_INCLUDED*/