#ifndef _FAKEGPSPPSSERVERSIMPLIFIED_H_B76116BD_4050_4C7B_A6EE_49A40C3C5C6F_INCLUDED
#define _FAKEGPSPPSSERVERSIMPLIFIED_H_B76116BD_4050_4C7B_A6EE_49A40C3C5C6F_INCLUDED
/*
	Created by Tech_dog (ebontrop@gmail.com) on 19-Dec-2019 at 1:40:00a, UTC+7, Novosibirsk, Tulenina, Thursday;
	This is FakeGPS named pipe overlapped server interface declaration file;
*/
#include "fake.gps.pps.defs.h"
#include "fake.gps.cmd.defs.h"

namespace shared { namespace drv { namespace um {

	class CPipeServer_State {
	public:
		enum state : DWORD {
			e_connect    = 0x0,
			e_reading    = 0x1,
			e_writing    = 0x2,
		};
	};
	typedef
	      CPipeServer_State::state TPipeState;

	class CPipeServer_Cfg {
	public:
		static const DWORD buf_size = 4096;
		static const DWORD con_wait = 1000;
	};

	class CPipeServer_Overlap {
	protected:
		IPipeTrace&   m_trace; ICommandProcessor& m_proc;
		OVERLAPPED    m_over ;
		HANDLE        m_pipe ;
		TCHAR         m_request [CPipeServer_Cfg::buf_size]; DWORD   m_read ;
		TCHAR         m_response[CPipeServer_Cfg::buf_size]; DWORD   m_write;
		TPipeState    m_state;
		bool          m_pending;
		volatile bool m_break_it_on;
		HANDLE        m_thread;
		CPipeMode     m_mode  ;

	public:
		 CPipeServer_Overlap (ICommandProcessor& _proc, IPipeTrace& _trace, const TPipeMode = CPipeMode::e_msg_mode);
		~CPipeServer_Overlap (void);

	public:
		DWORD         Do_work   (void);
		bool          IsOn      (void) const;
		HRESULT       Turn      (const bool _b_on );

	private:
		HRESULT       Clear     (void);
		HRESULT       Connect   (void);
		HRESULT       Disconnect(void);
		HRESULT       Response  (void);
	};

}}}

#endif/*_FAKEGPSPPSSERVERSIMPLIFIED_H_B76116BD_4050_4C7B_A6EE_49A40C3C5C6F_INCLUDED*/