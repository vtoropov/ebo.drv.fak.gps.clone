#ifndef _FAKEGPSPPSREADER_H_A00D3976_BD76_41BC_9566_21CD5B21BF54_INCLUDED
#define _FAKEGPSPPSREADER_H_A00D3976_BD76_41BC_9566_21CD5B21BF54_INCLUDED
/*
	Created by Tech_dog (ebontrop@gmail.com) on 17-Dec-2019 at 4:29:34a, UTC+7, Novosibirsk, Tulenina, Tuesday;
	This is FakeGPS named pipe generic data reader interface declaration file;
*/
#include "fake.gps.pps.defs.h"

namespace shared { namespace drv { namespace um {

	class CPipeReader {
	protected:
		IPipeTrace &  m_trace ;
		IPipeEvents&  m_events;
		volatile bool m_break_it_on;
		HANDLE        m_pipe  ;
		PCHAR         m_buffer[256];
		HANDLE        m_thread;
		volatile DWORD
		              m_received;

	public:
		 CPipeReader (IPipeEvents&, IPipeTrace&);
		~CPipeReader (void);

	public:
		virtual DWORD Thread_Fun(void);

	public:
		HRESULT       Clear(void);
		HRESULT       Get  (const HANDLE _pipe); // asynch read operation;
		HRESULT       Stop (void);               // interrupts the read operation and closes work thread;
	};

}}}

#endif/*_FAKEGPSPPSREADER_H_A00D3976_BD76_41BC_9566_21CD5B21BF54_INCLUDED*/