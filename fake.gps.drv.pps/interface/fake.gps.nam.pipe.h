#ifndef _FAKEGPSNAMPIPE_H_A00D3976_BD76_41BC_9566_21CD5B21BF54_INCLUDED
#define _FAKEGPSNAMPIPE_H_A00D3976_BD76_41BC_9566_21CD5B21BF54_INCLUDED
/*
	Created by Tech_dog (ebontrop@gmail.com) on 1-Dec-2019 at 11:19:13p, UTC+7, Novosibirsk, Tulenina, Sunday;
	This is Ebo Drv UMDF generic named pipe interface declaration file;
	-----------------------------------------------------------------------------
	Adopted to FakeGPS project on 16-Dec-2019 at 0:53:33a, UTC+7, Novosibirsk, Tulenina, Monday;
*/
#include "fake.gps.pps.defs.h"

namespace shared { namespace drv { namespace um {

	class CNamedPipe {
	protected:
		IPipeTrace&
		            m_trace;
		HANDLE      m_pipe ;
		TLocator    m_loc  ;
		bool        m_connected;
		DWORD       m_wait ; // a timeout is specified for CreateNamedPipe(); this is used for waiting client connection(s);

	public:
		 CNamedPipe (IPipeTrace&);
		~CNamedPipe (void);

	public:
		HRESULT     Connect   (const bool& _b_interruped); // time consuming operation; is intended for a call from work thread;
		bool        Connected (void) const;
		HRESULT     Create    (LPCTSTR _lp_sz_name);
		HRESULT     Destroy   (void)      ;
		HRESULT     Disconnect(void)      ;
		HANDLE      Handle    (void) const;
		const bool  Is        (void) const;
		TLocatorRef Locator   (void) const;

	public:
		operator    HANDLE (void) const;
	};

}}}

#endif/*_FAKEGPSNAMPIPE_H_A00D3976_BD76_41BC_9566_21CD5B21BF54_INCLUDED*/