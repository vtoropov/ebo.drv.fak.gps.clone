#ifndef _EBOCOMPPSSERVER_H_B76116BD_4050_4C7B_A6EE_49A40C3C5C6F_INCLUDED
#define _EBOCOMPPSSERVER_H_B76116BD_4050_4C7B_A6EE_49A40C3C5C6F_INCLUDED
/*
	Created by Tech_dog (ebontrop@gmail.com) on 16-Dec-2019 at 2:48:56a, UTC+7, Novosibirsk, Tulenina, Monday;
	This is FakeGPS named pipe connection server interface declaration file;
*/
#include "fake.gps.pps.defs.h"
#include "fake.gps.nam.pipe.h"
#include "fake.gps.pps.reader.h"
#include "fake.gps.pps.writer.h"

namespace shared { namespace drv { namespace um {

	class CPipeServer : public IPipeEvents {
	protected:
		IPipeTrace&
		              m_trace;
		volatile bool m_break_it_on;
		HANDLE        m_thread;
		CNamedPipe    m_pipe  ;   // only one client can connect to server for this version of implementation;
		CPipeReader   m_reader;
		CPipeWriter   m_writer;

	public:
		 CPipeServer (IPipeTrace& _trace);
		~CPipeServer (void);

	private: // IPipeEvents
#pragma warning(disable: 4481)
		virtual HRESULT     IPipeEvent_OnCreate(LPCTSTR) override sealed;
		virtual HRESULT     IPipeEvent_OnRead  (const LPBYTE _p_data, const DWORD _dw_sz) override sealed;
		virtual HRESULT     IPipeEvent_OnWrite (const LPBYTE _p_data, const DWORD _dw_sz) override sealed;
#pragma warning(default: 4481)
	public:
		virtual DWORD Thread_Fun(void);

	public:
		bool          IsOn (void) const;
		HRESULT       Turn (const bool _b_on );
	};

}}}

#endif/*_EBOCOMPPSSERVER_H_B76116BD_4050_4C7B_A6EE_49A40C3C5C6F_INCLUDED*/