#ifndef _FAKEGPSPPSWRITER_H_A00D3976_BD76_41BC_9566_21CD5B21BF54_INCLUDED
#define _FAKEGPSPPSWRITER_H_A00D3976_BD76_41BC_9566_21CD5B21BF54_INCLUDED
/*
	Created by Tech_dog (ebontrop@gmail.com) on 18-Dec-2019 at 2:11:05a, UTC+7, Novosibirsk, Tulenina, Wednesday;
	This is FakeGPS named pipe generic data writer interface declaration file;
*/
#include "fake.gps.pps.defs.h"

namespace shared { namespace drv { namespace um {

	class CPipeWriter {
	protected:
		IPipeTrace &  m_trace ;
		IPipeEvents&  m_events;
		volatile bool m_break_it_on;
		HANDLE        m_pipe  ;
		PCHAR         m_buffer[256];
		HANDLE        m_thread;
		volatile DWORD
		              m_written;

	public:
		 CPipeWriter (IPipeEvents&, IPipeTrace&);
		~CPipeWriter (void);

	public:
		virtual DWORD Thread_Fun(void);

	public:
		HRESULT       Clear(void);
		HRESULT       Copy (const PBYTE _p_data, const DWORD _d_size); // copies data to internal buffer being written to driver;
		HRESULT       Put  (const HANDLE _pipe , const PBYTE _p_data , const DWORD _dw_size); // asynch write operation;
		HRESULT       Put  (const HANDLE _pipe , LPCSTR  _lp_sz_data);
		HRESULT       Stop (void);               // interrupts the write operation and closes work thread;
	};

}}}

#endif/*_FAKEGPSPPSWRITER_H_A00D3976_BD76_41BC_9566_21CD5B21BF54_INCLUDED*/