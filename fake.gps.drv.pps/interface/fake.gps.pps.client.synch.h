#ifndef _FAKEGPSPPSCLIENTSIMPLIFIED_H_B76116BD_4050_4C7B_A6EE_49A40C3C5C6F_INCLUDED
#define _FAKEGPSPPSCLIENTSIMPLIFIED_H_B76116BD_4050_4C7B_A6EE_49A40C3C5C6F_INCLUDED
/*
	Created by Tech_dog (ebontrop@gmail.com) on 20-Dec-2019 at 05:34:54a, UTC+7, Novosibirsk, Tulenina, Friday;
	This is FakeGPS named pipe synchronous query client interface declaration file;
*/
#include "fake.gps.cmd.defs.h"
#include "fake.gps.pps.defs.h"
#include "fake.gps.cmd.geo.loc.h"

namespace shared { namespace drv { namespace um {

	class CPipeClient_State {
	public:
		enum state : DWORD {
			e_connect    = 0x0,
			e_reading    = 0x1,
			e_writing    = 0x2,
		};
	};
	typedef
	      CPipeClient_State::state TPipeState_2;

	class CPipeClient_Cfg {
	public:
		static const DWORD buf_size = 4096;
		static const DWORD con_wait = 5000;
	};

	class CPipeClient_Synch{
	protected:
		IPipeTrace&   m_trace;
		OVERLAPPED    m_over ;
		HANDLE        m_pipe ;
		TCHAR         m_request [CPipeClient_Cfg::buf_size]; DWORD   m_read ;
		TCHAR         m_response[CPipeClient_Cfg::buf_size]; DWORD   m_write;
		TPipeState_2  m_state;
		bool          m_pending;
		volatile bool m_break_it_on;
		CPipeMode     m_mode   ;
		CCmd_Geo_loc  m_geo_cmd;

	public:
		 CPipeClient_Synch (IPipeTrace& _trace, const TPipeMode = CPipeMode::e_msg_mode);
		~CPipeClient_Synch (void);

	public:
		bool          IsOn   (void) const;
		HRESULT       Turn   (const bool _b_on );

	public:
		const
		CCmd_Geo_loc& Cmd_geo(void) const;
		CCmd_Geo_loc& Cmd_geo(void)      ;

	private:
		HRESULT       Clear  (void);
		HRESULT       Connect(void);
		HRESULT       Disconnect(void);
		HRESULT       Request   (void);
	};

}}}

#endif/*_FAKEGPSPPSCLIENTSIMPLIFIED_H_B76116BD_4050_4C7B_A6EE_49A40C3C5C6F_INCLUDED*/