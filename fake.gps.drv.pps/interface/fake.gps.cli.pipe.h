#ifndef _FAKEGPSCLIPIPE_H_A00D3976_BD76_41BC_9566_21CD5B21BF54_INCLUDED
#define _FAKEGPSCLIPIPE_H_A00D3976_BD76_41BC_9566_21CD5B21BF54_INCLUDED
/*
	Created by Tech_dog (ebontrop@gmail.com) on 16-Dec-2019 at 12:29:55p, UTC+7, Novosibirsk, Tulenina, Monday;
	This is FakeGPS client pipe interface declaration file;
*/
#include "fake.gps.pps.defs.h"

namespace shared { namespace drv { namespace um {

	class CClientPipe {
	protected:
		IPipeTrace &  m_trace ;
		IPipeEvents&  m_evnts ;
		volatile bool m_break_it_on;
		HANDLE        m_pipe  ;
		TLocator      m_loc   ;
		HANDLE        m_thread;    // creating a client pipe is based on creating file that connects to server pipe asynch;
		DWORD         m_timeout    ;

	public:
		 CClientPipe (IPipeEvents&, IPipeTrace&);
		~CClientPipe (void);

	public:
		virtual DWORD Thread_Fun(void);

	public:
		HRESULT       Create    (LPCTSTR _lp_sz_name, const DWORD _wait_msec);
		const bool    Connected (void) const; // it is the same as Is(), because client pipe cannot exist without connection;
		HRESULT       Destroy   (void)      ;
		HANDLE        Handle    (void) const;
		const bool    Is        (void) const;
		TLocatorRef   Locator   (void) const;

	public:
		operator      HANDLE    (void) const;

	};

}}}

#endif/*_FAKEGPSCLIPIPE_H_A00D3976_BD76_41BC_9566_21CD5B21BF54_INCLUDED*/