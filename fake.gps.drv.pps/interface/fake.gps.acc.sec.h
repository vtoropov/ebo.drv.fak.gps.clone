#ifndef _EBOCOMACCSEC_H_316873A1_A4CE_447B_8930_965B5ED6B0D4_INCLUDED
#define _EBOCOMACCSEC_H_316873A1_A4CE_447B_8930_965B5ED6B0D4_INCLUDED
/*
	Created by Tech_dog (ebontrop@gmail.com) on 2-Dec-2019 at 0:24:28a, UTC+7, Novosibirsk, Tulenina, Monday;
	This is Ebo Drv UMDF named pipe access security interface declaration file;
	-----------------------------------------------------------------------------
	Adopted to FakeGPS project on 16-Dec-2019 at 0:53:32a, UTC+7, Novosibirsk, Tulenina, Monday;
*/
#if (0)
#include "shared.gen.sys.err.h"
#endif
namespace shared { namespace drv { namespace um {
#if (0)
	using shared::sys_core::CError;
#endif
	class CPipeAccess {
	protected:
		SECURITY_ATTRIBUTES   m_sec_atts;
#if (0)
		CError   m_error;
#endif
	public:
		 CPipeAccess (void) ;
		~CPipeAccess (void) ;

	public:
#if (0)
		TErrorRef    Error (void) const;
#endif
		const bool   Is    (void) const;
		const
		SECURITY_ATTRIBUTES* Ptr (void) const;
		const
		SECURITY_ATTRIBUTES& Ref (void) const;
		SECURITY_ATTRIBUTES& Ref (void)      ;

	public:
		operator const
		SECURITY_ATTRIBUTES&     (void) const;
		operator
		LPSECURITY_ATTRIBUTES    (void) const;
	};

}}}

#endif/*_EBOCOMACCSEC_H_316873A1_A4CE_447B_8930_965B5ED6B0D4_INCLUDED*/