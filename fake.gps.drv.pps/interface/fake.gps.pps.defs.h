#ifndef _EBOCOMPPSSDEFS_H_B76116BD_4050_4C7B_A6EE_49A40C3C5C6F_INCLUDED
#define _EBOCOMPPSSDEFS_H_B76116BD_4050_4C7B_A6EE_49A40C3C5C6F_INCLUDED
/*
	Created by Tech_dog (ebontrop@gmail.com) on 16-Dec-2019 at 2:59:30a, UTC+7, Novosibirsk, Tulenina, Monday;
	This is FakeGPS named pipe generic interface(s) declaration file;
*/

namespace shared { namespace drv { namespace um {

	interface IPipeTrace {
		virtual HRESULT     IPipeTrace_PutError(LPCSTR _lp_sz_desc, const HRESULT _h_result) PURE;
		virtual HRESULT     IPipeTrace_PutInfo (LPCSTR _lp_sz_desc) PURE;
		virtual HRESULT     IPipeTrace_PutInfo (LPCSTR _lp_sz_desc, const DWORD   _d_data  ) PURE;
		virtual HRESULT     IPipeTrace_PutInfo (LPCSTR _lp_sz_desc, LPCSTR    _lp_sz_data  ) PURE;
		virtual HRESULT     IPipeTrace_PutWarn (LPCSTR _lp_sz_desc, const HRESULT _h_result) PURE;
	};

	interface IPipeEvents {
		virtual HRESULT     IPipeEvent_OnCreate(LPCTSTR) PURE;
		virtual HRESULT     IPipeEvent_OnRead  (const LPBYTE _p_data, const DWORD _dw_sz) PURE;
		virtual HRESULT     IPipeEvent_OnWrite (const LPBYTE _p_data, const DWORD _dw_sz) PURE;
	};

	class CPipeLocator {
	protected:
		TCHAR       m_uri[256];

	public:
		 CPipeLocator (void);
		~CPipeLocator (void);

	public:
		VOID        Clear (void)      ;
		LPCTSTR     URI   (void) const;
		HRESULT     URI   (LPCTSTR)   ;

	public:
		operator LPCTSTR  (void) const;
	public:
		CPipeLocator& operator = (LPCTSTR _lp_sz_name);
	};

	typedef CPipeLocator    TLocator;
	typedef const TLocator& TLocatorRef;

	class CPipeMode {
	public:
		enum e_mode {
		     e_msg_mode = 0x0,
		     e_bin_mode = 0x1,
		};
	private:
		e_mode     m_current; 

	public:
		 CPipeMode (const e_mode = e_mode::e_msg_mode);
		~CPipeMode (void) ;

	public:
		const bool IsBin(void) const;
		const bool IsMsg(void) const;
		const
		e_mode     Mode (void) const;
		VOID       Mode (const e_mode);
	};

	typedef CPipeMode::e_mode TPipeMode;

}}}

#endif/*_EBOCOMPPSSDEFS_H_B76116BD_4050_4C7B_A6EE_49A40C3C5C6F_INCLUDED*/