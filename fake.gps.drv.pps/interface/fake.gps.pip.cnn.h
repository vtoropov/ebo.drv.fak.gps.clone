#ifndef _EBOCOMPIPCNN_H_B76116BD_4050_4C7B_A6EE_49A40C3C5C6F_INCLUDED
#define _EBOCOMPIPCNN_H_B76116BD_4050_4C7B_A6EE_49A40C3C5C6F_INCLUDED
/*
	Created by Tech_dog (ebontrop@gmail.com) on 2-Dec-2019 at 2:51:29a, UTC+7, Novosibirsk, Tulenina, Monday;
	This is Ebo Drv UMDF named pipe data exchange channel interface declaration file;
	-----------------------------------------------------------------------------
	Adopted to FakeGPS project on 16-Dec-2019 at 0:53:33a, UTC+7, Novosibirsk, Tulenina, Monday;
*/
#include "fake.gps.pps.defs.h"
#include "fake.gps.cli.pipe.h"

namespace shared { namespace drv { namespace um {

	class CPipeChannel {
	protected:
		INamedPipeTrace&
		             m_trace;
		CClientPipe  m_pipe ;

	public:
		 CPipeChannel (INamedPipeTrace&) ;
		~CPipeChannel (void) ;

	public:
	};

	class CPipeReader : public CPipeChannel {
	                   typedef CPipeChannel TChannel;
	private:
		mutable bool m_b_stop;
	public:
		 CPipeReader (INamedPipeTrace&);
		~CPipeReader (void);

	public:
		const bool  IsRunning (void) const;
		HRESULT     Start (void) ;
		HRESULT     Stop  (void) ;
	};

}}}

#endif/*_EBOCOMPIPCNN_H_B76116BD_4050_4C7B_A6EE_49A40C3C5C6F_INCLUDED*/