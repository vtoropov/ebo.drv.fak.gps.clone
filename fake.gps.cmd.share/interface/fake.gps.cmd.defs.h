#ifndef _FAKEGPSCMDDEFS_H_045BDCC3_5585_4F68_A3AC_F9927979FFCD_INCLUDEDED
#define _FAKEGPSCMDDEFS_H_045BDCC3_5585_4F68_A3AC_F9927979FFCD_INCLUDEDED
/*
	Created by Tech_dog (ebontrop@gmail.com) on 13-Dec-2019 at 0:53:58a, UTC+7, Novosibirsk, Tulenina, Friday;
	This is Fake GPS driver shared command set interface declaration file.
*/

namespace shared { namespace drv { namespace um {

	class CCmd__Type {
	public:
		enum _type : DWORD {
			e_undef   = 0x0,
			e_time_t  = 0x1,  // time_t as a data type is transferred through server/client connection;
			e_locate  = 0x2,  // ge-olocation data;
		};
	public:
		 CCmd__Type (void) ;
		~CCmd__Type (void) ;
	};

	typedef CCmd__Type::_type TCmdType;

	interface ICommand {
		virtual const bool     Is    (const PBYTE _p_data, const INT _n_size) const PURE;
		virtual const PBYTE    Raw   (void) const PURE;                             // a pointer for reading data;
		virtual       PBYTE    Raw   (void)       PURE;                             // a pointer to  assign  data;
		virtual       HRESULT  Set   (const PBYTE _p_data, const INT _n_size) PURE; // copies data if appropriate type;
		virtual       DWORD    Size  (void) const PURE;                             // a size of the data buffer ;
		virtual       TCmdType Type  (void) const PURE;
	};

	interface ICommandProcessor {
		virtual HRESULT   ICmdProc_Get(ICommand&) PURE;
		virtual HRESULT   ICmdProc_Set(ICommand&) PURE;
	};

}}}

#endif/*_FAKEGPSCMDDEFS_H_045BDCC3_5585_4F68_A3AC_F9927979FFCD_INCLUDEDED*/