#ifndef _FAKEGPSCMDTIME_H_045BDCC3_5585_4F68_A3AC_F9927979FFCD_INCLUDEDED
#define _FAKEGPSCMDTIME_H_045BDCC3_5585_4F68_A3AC_F9927979FFCD_INCLUDEDED
/*
	Created by Tech_dog (ebontrop@gmail.com) on 25-Dec-2019 at 1:56:31p, UTC+7, Novosibirsk, Tulenina, Wednesday;
	This is Fake GPS driver shared system time command interface declaration file.
*/
#include "fake.gps.cmd.defs.h"

namespace shared { namespace drv { namespace um {

	class CCmd_Time_t : public ICommand {
	public:
		enum _fmt {
			eFormattedBufferSize = 30, // in bytes;
		};
	private:
		typedef struct {
			TCmdType  _type;
			time_t    _time;
			char      _reserved[4];
		} TTimeData;
	private:
		TTimeData    m_time;

	public:
		 CCmd_Time_t (void);
		~CCmd_Time_t (void);

	public: //  ICommand;
#pragma warning(disable: 4481)
		virtual const bool     Is    (const PBYTE _p_data, const INT _n_size) const override sealed;
		virtual const PBYTE    Raw   (void) const override sealed;
		virtual       PBYTE    Raw   (void)       override sealed;
		virtual       HRESULT  Set   (const PBYTE _p_data, const INT _n_size) override sealed;
		virtual       DWORD    Size  (void) const override sealed;
		virtual       TCmdType Type  (void) const override sealed;
#pragma warning(default: 4481)
	public:
		HRESULT   Copy(LPBYTE _dest, DWORD& _sz) const;// copies data to destination buffer; _sz - buffer size/copied bytes;
		time_t    Get (void) const;
		HRESULT   Set (const time_t);
		HRESULT   Text(LPBYTE _dest, DWORD& _sz) const;// copies object as text to buffer; _sz - buffer size/copied bytes;

	public:
		HRESULT   Local(void);
		
	public:
		CCmd_Time_t& operator= (const time_t);
	};

}}}

#endif/*_FAKEGPSCMDTIME_H_045BDCC3_5585_4F68_A3AC_F9927979FFCD_INCLUDEDED*/