#ifndef _FAKEGPSCMDGEOLOC_H_045BDCC3_5585_4F68_A3AC_F9927979FFCD_INCLUDEDED
#define _FAKEGPSCMDGEOLOC_H_045BDCC3_5585_4F68_A3AC_F9927979FFCD_INCLUDEDED
/*
	Created by Tech_dog (ebontrop@gmail.com) on 25-Dec-2019 at 2:03:34p, UTC+7, Novosibirsk, Tulenina, Wednesday;
	This is Fake GPS driver shared geolocation coordinate setting/retrieving command interface declaration file.
*/
#include "fake.gps.cmd.defs.h"

namespace shared { namespace drv { namespace um {

	class CCmd_Geo_loc : public ICommand {
	private:
		typedef struct {
			TCmdType  _type;
			DOUBLE    _latitude ;
			DOUBLE    _longitude;
		} TGeoLocateData;
	private:
		TGeoLocateData m_locate;

	public:
		 CCmd_Geo_loc (void);
		~CCmd_Geo_loc (void);

	public: //  ICommand;
#pragma warning(disable: 4481)
		virtual const bool     Is    (const PBYTE _p_data, const INT _n_size) const override sealed;
		virtual const PBYTE    Raw   (void) const override sealed;
		virtual       PBYTE    Raw   (void)       override sealed;
		virtual       HRESULT  Set   (const PBYTE _p_data, const INT _n_size) override sealed;
		virtual       DWORD    Size  (void) const override sealed;
		virtual       TCmdType Type  (void) const override sealed;
#pragma warning(default: 4481)
	public:
		HRESULT   Copy(LPBYTE _dest, DWORD& _sz) const;// copies data to destination buffer; _sz - buffer size/copied bytes;
		DOUBLE    Latitude (void) const  ;
		HRESULT   Latitude (const DOUBLE);
		DOUBLE    Longitude(void) const  ;
		HRESULT   Longitude(const DOUBLE);
		HRESULT   Text(LPBYTE _dest, DWORD& _sz) const;// copies object as text to buffer; _sz - buffer size/copied bytes;

	};

	class CCmd_Geo_loc_fmt {
	public:
		enum __aux {
			e__double_buf_sz = 0x30, // buffer size in count of characters, for formatting double value;
		};
	public:
		 CCmd_Geo_loc_fmt(void);
		~CCmd_Geo_loc_fmt(void);
	public:
		static LPCSTR    Pattern_a(void)/* const*/;
		static LPCTSTR   Pattern_w(void)/* const*/;
	};

}}}

#endif/*_FAKEGPSCMDGEOLOC_H_045BDCC3_5585_4F68_A3AC_F9927979FFCD_INCLUDEDED*/