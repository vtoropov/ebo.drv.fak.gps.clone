/*
	Created by Tech_dog (ebontrop@gmail.com) on 25-Dec-2019 at 2:00:09p, UTC+7, Novosibirsk, Tulenina, Wednesday;
	This is Fake GPS driver shared system time command interface implementation file.
*/
#include "StdAfx.h"
#include "fake.gps.cmd.time.h"

using namespace shared::drv::um;

/////////////////////////////////////////////////////////////////////////////

CCmd_Time_t:: CCmd_Time_t(void) { m_time._type = TCmdType::e_time_t; m_time._time = 0; ::memset(&m_time._reserved, 0, sizeof(char) * _countof(m_time._reserved)); }
CCmd_Time_t::~CCmd_Time_t(void) {}

/////////////////////////////////////////////////////////////////////////////
const
bool     CCmd_Time_t::Is    (const PBYTE _p_data, const INT _n_size) const {
	if (NULL == _p_data) return false;
	if (this->Size() < static_cast<ULONG>(_n_size)) return false;

	const PDWORD p_type = reinterpret_cast<PDWORD>(_p_data);
	return (NULL != p_type && *p_type == m_time._type);
}

const
PBYTE    CCmd_Time_t::Raw   (void) const { return (PBYTE)&m_time; }
PBYTE    CCmd_Time_t::Raw   (void)       { return (PBYTE)&m_time; }

HRESULT  CCmd_Time_t::Set   (const PBYTE _p_data, const INT _n_size) {
	if (this->Is(_p_data, _n_size) == false)
		return DISP_E_TYPEMISMATCH;

	HRESULT hr_ = S_OK;

	const errno_t err_ = ::memcpy_s(&m_time, this->Size(), (void*)_p_data, this->Size());
	if (err_)
		hr_ = __DwordToHresult(ERROR_INVALID_DATA);

	return hr_;
}

DWORD    CCmd_Time_t::Size  (void) const   { return sizeof(TTimeData); }
TCmdType CCmd_Time_t::Type  (void) const   { return (CCmd__Type::_type)m_time._type; }

/////////////////////////////////////////////////////////////////////////////

HRESULT  CCmd_Time_t::Copy  (LPBYTE _dest, DWORD& _sz) const {

	if (NULL == _dest) return (E_POINTER);
	if ( 0 == _sz)     return (E_INVALIDARG);
	if (this->Size() > _sz)
		return (DISP_E_OVERFLOW);

	HRESULT hr_ = S_OK;
	const errno_t err_ = ::memcpy_s(_dest, _sz, (void*)this->Raw(), this->Size());
	if (err_)
		hr_ = __DwordToHresult(ERROR_INVALID_DATA);
	else
		_sz = this->Size();

	return  hr_;
}
time_t   CCmd_Time_t::Get   (void) const { return m_time._time; }
HRESULT  CCmd_Time_t::Set   (const time_t _t) { m_time._time = _t; return S_OK; }
HRESULT  CCmd_Time_t::Text  (LPBYTE _dest, DWORD& _sz) const {

	if (NULL == _dest) return (E_POINTER);
	if ( 0 == _sz)     return (E_INVALIDARG);
	if (this->Size() > _sz || _sz < _fmt::eFormattedBufferSize)
		return (DISP_E_OVERFLOW);

	struct tm p_tm = {0};
	::std::localtime_s(&p_tm, &this->m_time._time);  // assumes local time for this version;

	if (::std::strftime(
		(PCHAR)_dest, _sz, "%Y-%m-%d %H:%M:%S", &p_tm
	)) {
	}

	HRESULT hr_ = S_OK;
	return  hr_;
}

/////////////////////////////////////////////////////////////////////////////

HRESULT  CCmd_Time_t::Local(void) {
	m_time._time = ::std::time(NULL);
	return S_OK;
}

/////////////////////////////////////////////////////////////////////////////

CCmd_Time_t& CCmd_Time_t::operator= (const time_t _tm) {
	this->Set(_tm); return *this;
}