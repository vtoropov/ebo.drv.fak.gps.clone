/*
	Created by Tech_dog (ebontrop@gmail.com) on 25-Dec-2019 at 2:03:34p, UTC+7, Novosibirsk, Tulenina, Wednesday;
	This is Fake GPS driver shared geolocation coordinate setting/retrieving command interface implementation file.
*/
#include "StdAfx.h"
#include "fake.gps.cmd.geo.loc.h"

using namespace shared::drv::um;
/////////////////////////////////////////////////////////////////////////////

CCmd_Geo_loc:: CCmd_Geo_loc(void) { m_locate._type = TCmdType::e_locate; m_locate._latitude = 0.0; m_locate._longitude = 0.0; }
CCmd_Geo_loc::~CCmd_Geo_loc(void) {}

/////////////////////////////////////////////////////////////////////////////
const
bool     CCmd_Geo_loc::Is    (const PBYTE _p_data, const INT _n_size) const {
	if (NULL == _p_data) return false;
	if (this->Size() < static_cast<ULONG>(_n_size)) return false;

	const PDWORD p_type = reinterpret_cast<PDWORD>(_p_data);
	return (NULL != p_type && *p_type == m_locate._type);
}

const
PBYTE    CCmd_Geo_loc::Raw   (void) const { return (PBYTE)&m_locate; }
PBYTE    CCmd_Geo_loc::Raw   (void)       { return (PBYTE)&m_locate; }

HRESULT  CCmd_Geo_loc::Set   (const PBYTE _p_data, const INT _n_size) {
	if (this->Is(_p_data, _n_size) == false)
		return DISP_E_TYPEMISMATCH;

	HRESULT hr_ = S_OK;

	const errno_t err_ = ::memcpy_s(&m_locate, this->Size(), (void*)_p_data, _n_size);
	if (err_)
		hr_ = __DwordToHresult(ERROR_INVALID_DATA);

	return hr_;
}

DWORD    CCmd_Geo_loc::Size  (void) const   { return sizeof(TGeoLocateData); }
TCmdType CCmd_Geo_loc::Type  (void) const   { return (CCmd__Type::_type)m_locate._type; }

/////////////////////////////////////////////////////////////////////////////

HRESULT  CCmd_Geo_loc::Copy  (LPBYTE _dest, DWORD& _sz) const {

	if (NULL == _dest) return (E_POINTER);
	if ( 0 == _sz)     return (E_INVALIDARG);
	if (this->Size() > _sz)
		return (DISP_E_OVERFLOW);

	HRESULT hr_ = S_OK;
	const errno_t err_ = ::memcpy_s(_dest, _sz, (void*)this->Raw(), this->Size());
	if (err_)
		hr_ = __DwordToHresult(ERROR_INVALID_DATA);
	else
		_sz = this->Size();

	return  hr_;
}

DOUBLE   CCmd_Geo_loc::Latitude (void) const     { return m_locate._latitude ; }
HRESULT  CCmd_Geo_loc::Latitude (const DOUBLE _v){ m_locate._latitude  = _v; return S_OK; }
DOUBLE   CCmd_Geo_loc::Longitude(void) const     { return m_locate._longitude; }
HRESULT  CCmd_Geo_loc::Longitude(const DOUBLE _v){ m_locate._longitude = _v; return S_OK; }

HRESULT  CCmd_Geo_loc::Text  (LPBYTE _dest, DWORD& _sz) const {

	if (NULL == _dest) return (E_POINTER);
	if ( 0 == _sz)     return (E_INVALIDARG);
	if (this->Size() > _sz || _sz < CCmd_Geo_loc_fmt::e__double_buf_sz)
		return (DISP_E_OVERFLOW);

	CHAR sz_pattern[CCmd_Geo_loc_fmt::e__double_buf_sz] = {0};
	::sprintf_s(
		(PCHAR)sz_pattern, _countof(sz_pattern), "Lat:%s;Lon:%s;", CCmd_Geo_loc_fmt().Pattern_a(), CCmd_Geo_loc_fmt().Pattern_a()
	);

	::sprintf_s((PCHAR)_dest, _sz, sz_pattern, m_locate._latitude, m_locate._longitude);

	HRESULT hr_ = S_OK;
	return  hr_;
}

/////////////////////////////////////////////////////////////////////////////

CCmd_Geo_loc_fmt:: CCmd_Geo_loc_fmt(void) {}
CCmd_Geo_loc_fmt::~CCmd_Geo_loc_fmt(void) {}

/////////////////////////////////////////////////////////////////////////////

LPCSTR   CCmd_Geo_loc_fmt::Pattern_a(void)/* const */{

	static LPCSTR  lp_sz_pat =   ("%16.12lf");
	return lp_sz_pat;
}

LPCTSTR  CCmd_Geo_loc_fmt::Pattern_w(void)/* const */{

	static LPCTSTR  lp_sz_pat = _T("%16.12lf");
	return lp_sz_pat;
}