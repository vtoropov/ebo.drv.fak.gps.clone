/*
	Created by Tech_dog (ebontrop@gmail.com) on 15-May-2012 at 9:44:59pm, GMT+3, Rostov-on-Don, Tuesday;
	This is Pulsepay Server Application Common Error Trace class implementation file;
	-----------------------------------------------------------------------------
	Adopted to sound-bin-trans project on 5-Apr-2019 at 9:55:50a, UTC+7, Phuket, Rawai, Thursday;
	Adopted to FakeGPS driver project on 13-Dec-2019 at 10:20:24a, UTC+7, Novosibirsk, Tulenina, Friday;
*/
#include "StdAfx.h"
#include "shared.gen.sys.err.h"

using namespace shared::sys_core;

/////////////////////////////////////////////////////////////////////////////

namespace shared { namespace sys_core { namespace details {

	LPCTSTR    Error_NotFoundMessage(void) {
		static LPCTSTR pszNotFound = _T("Error details not found");
		return pszNotFound;
	}

	VOID       Error_FormatMessage(const DWORD dwError, ::ATL::CAtlString& buffer_ref) {
		TCHAR szBuffer[_MAX_PATH] = {0};

		::FormatMessage(
			FORMAT_MESSAGE_FROM_SYSTEM,
			NULL,
			dwError,
			MAKELANGID(LANG_INVARIANT, SUBLANG_CUSTOM_DEFAULT),
			szBuffer,
			_MAX_PATH - 1,
			NULL
		);
		buffer_ref = szBuffer;
	}

	VOID       Error_NormalizeMessage(::ATL::CAtlString& buffer_ref) {
		if (!buffer_ref.IsEmpty())
		{
			buffer_ref.Replace(_T("\r")  , _T(" "));
			buffer_ref.Replace(_T("\n")  , _T(" "));
			buffer_ref.TrimRight();
		}
	}
}}}
using namespace shared::sys_core::details;
/////////////////////////////////////////////////////////////////////////////

CError:: CError(void) : m_result(OLE_E_BLANK), m_desc(_T("#n/a")) {}
CError::~CError(void) {}

/////////////////////////////////////////////////////////////////////////////

DWORD    CError::Code  (void) const     { SAFE_LOCK(m_lock);  return (0xffff & m_result); }
VOID     CError::Code  (const DWORD _v) {
	SAFE_LOCK(m_lock);  m_result = __DwordToHresult(_v);
	Error_FormatMessage(_v, m_desc);
	Error_NormalizeMessage( m_desc);
}
LPCTSTR  CError::Desc  (void) const     { SAFE_LOCK(m_lock);  return m_desc.GetString(); }
CAtlString
         CError::Format(LPCTSTR lp_sz_sep) const {

	static LPCTSTR lp_sz_pat = _T("; Code=0x%x; desc=%s; module=%s;");

	CAtlString cs_pat(lp_sz_pat);

	if (lp_sz_sep && ::_tcslen(lp_sz_sep))
		cs_pat.Replace(_T("; "), lp_sz_sep);

	CAtlString cs_desc;
	cs_desc.Format(
		cs_pat, this->Result(),
		::_tcslen(this->Desc())   ? this->Desc()   : _T("#n/a"),
		::_tcslen(this->Module()) ? this->Module() : _T("#n/a")
		);
	return cs_desc;
}
LPCTSTR  CError::Module(void) const { SAFE_LOCK(m_lock); return m_module.GetString(); }
VOID     CError::Module(LPCTSTR _v) { SAFE_LOCK(m_lock); m_module = _v;   }
HRESULT  CError::Result(void) const { SAFE_LOCK(m_lock); return m_result; }
VOID     CError::Result(const HRESULT _hr) {
	SAFE_LOCK(m_lock);
	m_result = _hr;
	if (S_OK == m_result) {
		m_desc = _T("No error");
		return;
	}
	::ATL::CAtlString   cs_module = m_module; // saves an original;
	::ATL::CAtlString   cs_source;
	::ATL::CComPtr<IErrorInfo> sp;
	if (S_OK == ::GetErrorInfo(0, &sp) && sp) {
		_com_error err_(m_result, sp , true );
		m_desc    =(LPCTSTR)err_.Description();
		cs_source =(LPCTSTR)err_.Source();

		if (cs_source.IsEmpty() == false)
			m_module = cs_source;
	}
	else {
		_com_error err_(m_result);
		m_desc    =(LPCTSTR)err_.Description();
		cs_source =(LPCTSTR)err_.Source();
		if (cs_source.IsEmpty() == false)
			m_module = cs_source;
	}
	if (m_module.IsEmpty())
		m_module = cs_module;
	if (m_desc.IsEmpty())
		details::Error_FormatMessage(m_result, m_desc);
	details::Error_NormalizeMessage(m_desc);
}

VOID     CError::State (const DWORD _code, LPCTSTR _lp_sz_desc) {
	if (_code == ERROR_SUCCESS) *this << S_OK;
	if (_code != ERROR_SUCCESS) *this << HRESULT_FROM_WIN32(_code);
	*this = _lp_sz_desc;
}

VOID     CError::State (HRESULT _err_code, LPCTSTR _lp_err_desc) {
	SAFE_LOCK(m_lock);
	m_result  = _err_code;
	*this = _lp_err_desc;
}

bool     CError::State (void) const { SAFE_LOCK(m_lock); return(TRUE == (FAILED(m_result))); }

/////////////////////////////////////////////////////////////////////////////

CError& CError::operator << (const HRESULT _v) { *this = _v; return *this; }
CError& CError::operator << (LPCTSTR _lp_sz_v) {  this->Module(_lp_sz_v); return *this; }
CError& CError::operator =  (const DWORD   _v) { *this = HRESULT_FROM_WIN32(_v); return *this; }
CError& CError::operator =  (const HRESULT _v) {  this->Result(_v)  ; return *this; }
CError& CError::operator =  (LPCTSTR _lp_sz_v) { m_desc   = _lp_sz_v; return *this; }
CError& CError::operator =  (const CError& _e) {
	SAFE_LOCK(m_lock);
	m_desc   = _e.m_desc;
	m_module = _e.m_module;
	m_result = _e.m_result;
	return *this;
}

/////////////////////////////////////////////////////////////////////////////

CError::operator const bool (void) const { return this->State(); }
CError::operator HRESULT(void) const { return this->Result(); }
CError::operator LPCTSTR(void) const { return this->Desc(); }

/////////////////////////////////////////////////////////////////////////////

namespace shared { namespace sys_core {
	//
	// important: operator must return true in cases when error object does not provide success;
	//
	bool operator==(const bool _lhs, const CError& _rhs) { return (_lhs != SUCCEEDED(_rhs.Result())); }
	bool operator!=(const bool _lhs, const CError& _rhs) { return (_lhs == SUCCEEDED(_rhs.Result())); }
	bool operator==(const CError& _lhs, const bool _rhs) { return (SUCCEEDED(_lhs.Result()) != _rhs); }
	bool operator!=(const CError& _lhs, const bool _rhs) { return (SUCCEEDED(_lhs.Result()) == _rhs); }
}}