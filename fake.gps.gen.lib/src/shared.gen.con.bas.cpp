/*
	Created by Tech_dog (ebontrop@gmail.com) on 11-Feb-2019 at 7:39:00a, UTC+7, Novosibirsk, Tulenina, Monday;
	This is FIX Engine shared library test desktop console app interface implementation file.
	-----------------------------------------------------------------------------
	Adopted to sound-bin-trans project on 5-Apr-2019 at 11:42:04a, UTC+7, Phuket, Rawai, Thursday;
	Adopted to FakeGPS driver project on 13-Dec-2019 at 9:49:24a, UTC+7, Novosibirsk, Tulenina, Friday;
*/
#include "StdAfx.h"
#include "shared.gen.con.bas.h"

using namespace shared::common::ui;
/////////////////////////////////////////////////////////////////////////////

namespace shared { namespace common { namespace ui { namespace _impl {

	HICON Console_LoadIcon(const UINT nIconResId, const bool bTreatAsLargeIcon)
	{
		const SIZE szIcon = {
					::GetSystemMetrics(bTreatAsLargeIcon ? SM_CXICON : SM_CXSMICON), 
					::GetSystemMetrics(bTreatAsLargeIcon ? SM_CYICON : SM_CYSMICON)
				};
		const HINSTANCE hInstance = ::ATL::_AtlBaseModule.GetModuleInstance();
		const HICON hIcon = (HICON)::LoadImage(
			hInstance, MAKEINTRESOURCE(nIconResId), IMAGE_ICON, szIcon.cx, szIcon.cy, LR_DEFAULTCOLOR
		);
		return hIcon;
	}

}}}}
using namespace shared::common::ui::_impl;
/////////////////////////////////////////////////////////////////////////////

CConsole:: CConsole(void) {}
CConsole::~CConsole(void) {}

/////////////////////////////////////////////////////////////////////////////

HRESULT   CConsole::OnClose  (void) {
	HRESULT hr_ = S_OK;
	const bool b_verb = true;
	if (b_verb) {
		::_tprintf(_T("\n\n\tPress any key or click [x] button to exit"));
		::_gettch();
	}
	return  hr_;
}

HRESULT   CConsole::OnCreate (LPCTSTR _lp_sz_cap) {
	HRESULT hr_ = S_OK;
	::SetConsoleOutputCP(CP_UTF8);
	std::wstring w_sep_(77, _T('-'));

	CAtlString cs_cap(_lp_sz_cap);
	if (cs_cap.IsEmpty())
		cs_cap = _T("Generic Console");

	::_tprintf(_T("%s\n")  , w_sep_.c_str());
	::_tprintf(_T("\t%s\n"), cs_cap.GetString());
	::_tprintf(_T("%s\n")  , w_sep_.c_str());

	return  hr_;
}

HRESULT   CConsole::OnWait   (LPCTSTR _lp_sz_msg) {
	HRESULT hr_ = S_OK;
	const bool b_verb = true;
	if (b_verb) {
		if (NULL != _lp_sz_msg)
			::_tprintf(_T("\n\n\t%s"), _lp_sz_msg);
		else
			::_tprintf(_T("\n\n\tPress any key to continue"));
		::_gettch();
	}
	return  hr_;
}

HRESULT   CConsole::SetIcon  (const WORD _w_res_id ) {
	HRESULT hr_ = S_OK;

	HICON h_sml_ico = Console_LoadIcon(_w_res_id, false);
	HICON h_lag_ico = Console_LoadIcon(_w_res_id, true );

	HWND  h_con_wnd = ::GetConsoleWindow();
	CWindow con_wnd = h_con_wnd;

	con_wnd.SetIcon (h_sml_ico, FALSE); h_sml_ico = NULL;
	con_wnd.SetIcon (h_lag_ico, TRUE ); h_lag_ico = NULL;

	return  hr_;
}

HRESULT   CConsole::Write    (const _tp _t, LPCTSTR _lp_sz_msg, LPCTSTR _lp_sz_sep) {
	HRESULT hr_ = S_OK; _lp_sz_sep;

	static LPCTSTR  lp_sz_tm_pat = _T("%02d/%02d/%02d %02d:%02d:%02d.%03d");
	SYSTEMTIME st = { 0 };
	::GetLocalTime(&st);

	::ATL::CAtlString cs_timestamp;
	cs_timestamp.Format(
		lp_sz_tm_pat,
		st.wMonth   ,
		st.wDay     ,
		st.wYear%100,  // only 2 digits
		st.wHour    ,
		st.wMinute  ,
		st.wSecond  ,
		st.wMilliseconds
	);

	::ATL::CAtlString cs_out;
	switch(_t)
	{
	case _tp::e_err   : cs_out.Format(_T("\n\t[ERROR][%s] %s"), cs_timestamp.GetString(), _lp_sz_msg); break;
	case _tp::e_info  : cs_out.Format(_T("\n\t[INFO] [%s] %s"), cs_timestamp.GetString(), _lp_sz_msg); break;
	case _tp::e_warn  : cs_out.Format(_T("\n\t[WARN] [%s] %s"), cs_timestamp.GetString(), _lp_sz_msg); break;
	default:
	hr_ = DISP_E_TYPEMISMATCH;
	}
	if (SUCCEEDED(hr_))
		::_tprintf(cs_out.GetString());

	return  hr_;
}

HRESULT   CConsole::WriteErr (TErrorRef _err) {
	return this->Write(_tp::e_err, _err.Format(lp_sz_sep).GetString(), lp_sz_sep);
}

HRESULT   CConsole::WriteErr (LPCTSTR _lp_sz_msg, LPCTSTR  _lp_sz_sep) {
	return  this->Write(_tp::e_err, _lp_sz_msg, _lp_sz_sep);
}

HRESULT   CConsole::WriteInfo(LPCTSTR _lp_sz_msg, LPCTSTR  _lp_sz_sep) {
	return  this->Write(_tp::e_info, _lp_sz_msg, _lp_sz_sep);
}

HRESULT   CConsole::WriteWarn(LPCTSTR _lp_sz_msg, LPCTSTR  _lp_sz_sep) {
	return  this->Write(_tp::e_warn, _lp_sz_msg, _lp_sz_sep);
}

/////////////////////////////////////////////////////////////////////////////

CConsole& CConsole::operator << (const WORD _v) { CAtlString cs_v; cs_v.LoadString(_v); this->WriteInfo((LPCTSTR)cs_v); return *this; }
CConsole& CConsole::operator << (LPCTSTR    _v) { this->WriteInfo(_v); return *this; }
CConsole& CConsole::operator << (TErrorRef  _e) { this->WriteErr (_e); return *this; }
CConsole& CConsole::operator << (const CAtlString& _cs) { this->WriteInfo(_cs.GetString()); return *this; }
