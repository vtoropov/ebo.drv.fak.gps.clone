﻿#define __use_dark
using System;
using FirstFloor.ModernUI.Windows.Controls;

namespace Fake.Core.UI {

	public partial class MainWindow : ModernWindow {

		public MainWindow() {
#if __use_dark
			FirstFloor.ModernUI.Presentation.AppearanceManager.Current.ThemeSource =
			new Uri("pack://application:,,,/FirstFloor.ModernUI;component/Assets/ModernUI.Dark.xaml");
#endif
			InitializeComponent();
			WindowStartupLocation = System.Windows.WindowStartupLocation.CenterScreen;
		}
	}
}
