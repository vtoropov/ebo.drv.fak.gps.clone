﻿using System.Windows.Controls;

namespace Fake.Core.UI.Content {
	/// <summary>
	/// </summary>
	public partial class SettingsAppearance : UserControl {
		public SettingsAppearance() {

			InitializeComponent();

			this.DataContext = new SettingsAppearanceViewModel();
		}
	}
}