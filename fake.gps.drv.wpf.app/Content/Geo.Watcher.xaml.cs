﻿using System;                  // event handler declaration;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Threading;

using System.Device.Location;  // System.Device.dll must be referred;

using FirstFloor.ModernUI.Windows.Controls;
using FirstFloor.ModernUI.Presentation;

using Fake.Core.UI.Data;

namespace Fake.Core.UI.Content {

	class CGridRow {
		public string Record    { get; set; }
		public string Timestamp { get; set; }
	};

	// https://docs.microsoft.com/en-us/dotnet/api/system.device.location.geocoordinatewatcher

	public partial class GeoWatcher : UserControl {

		private CTimerDataProvider   t_provider = null;
		private CWatchDataProvider   w_provider = null;

		public string        Error { get; private set; }
		public bool          InLog { get; private set; }
		public int           Wait  { get;         set; }
		public eProviderType Type  { get; private set; }

		public GeoWatcher()  {

			InitializeComponent();

			this.Loaded += OnLoaded;
		}
		/////////////////////////////////////////////////////////////////////////////
		~GeoWatcher() {
#if false
			// exception: Cannot access a disposed object.\r\nObject name: 'GeoCoordinateWatcher'.";
			if (this.m_watcher != null && false)
				this.m_watcher.Stop();
#endif
		}

		/////////////////////////////////////////////////////////////////////////////

		private void OnClear (object sender, RoutedEventArgs e) {
			this.track_table.Items.Clear();
			this.AddRow("Records are cleared;");
		}

		private void OnLoaded(object sender, RoutedEventArgs e) {
			Keyboard.Focus(this.track_table);

			this.table_border.BorderBrush = new SolidColorBrush(AppearanceManager.Current.AccentColor) { Opacity = 0.5 };
			this.Type = eProviderType.e_event;
			this.OnState(this.InLog = false);
			this.Wait = 5000;
		}

		private void OnStart (object sender, RoutedEventArgs e) {
			if (this.InLog) {
				this.Error = "Watching process is already started;";
				return;
			}
			this.OnState(this.InLog = true);

			switch (this.OnType()) {
			case eProviderType.e_event: {
					w_provider = new CWatchDataProvider();
					w_provider.Start(new Data.OnEvent(this.OnEvent));
				} break;
			case eProviderType.e_timer: {
					t_provider = new CTimerDataProvider(this.Wait);
					t_provider.Start(new Data.OnEvent(this.OnEvent));
				} break;
			default:
				this.Error = "Provider type is not supported;";
				ModernDialog.ShowMessage(this.Error, "Error", MessageBoxButton.OK);
				this.OnState(this.InLog = false);
				return;
			}
			this.AddRow("Track is started;");
		}

		private void OnStop  (object sender, RoutedEventArgs e) {
			if (this.InLog == false) {
				this.Error = "Watching process is already stopped;";
				return;
			}

			switch (this.OnType()) {
			case eProviderType.e_event: {
					w_provider.Stop();
				} break;
			case eProviderType.e_timer: {
					t_provider.Stop();
				} break;
			default:
				this.Error = "Provider type is not supported;";
				ModernDialog.ShowMessage(this.Error, "Error", MessageBoxButton.OK);
				this.OnState(this.InLog = false);
				break;
			}
			this.OnState(this.InLog = false);
			this.AddRow("Track is stopped;");
		}
		/////////////////////////////////////////////////////////////////////////////
		
		private void AddRow (ref CGridRow _row) {
			bool b_desc = (this.sort_order.IsChecked == true);
			if ( b_desc ) {
				this.track_table.Items.Insert(0, _row);
			}
			else {
				this.track_table.Items.Add(_row);
			}
			
			this.track_table.SelectedItem = _row ;
			this.track_table.ScrollIntoView(_row);
		}

		private void AddRow (string _record) {
			this.AddRow(this.Timestamp(), _record);
		}

		private void AddRow (string _timestamp, string _record) {
			CGridRow row_ = new CGridRow() {
				Timestamp = _timestamp,
				Record    = _record
			};
			this.AddRow(ref row_);
		}

		private void OnEvent(string _timestamp, string _record) {
			this.AddRow(_timestamp, _record);
		}

		private void OnState( bool _log_val) {
			this.InLog = _log_val;
			this.but_start.IsEnabled = (this.InLog == false);
			this.but_stop .IsEnabled = (this.InLog == true );
			this.event_src.IsEnabled = (this.InLog == false);
		}

		private eProviderType OnType() {
			switch (this.event_src.SelectedIndex) {
			case 1 : return eProviderType.e_timer;
			default: return eProviderType.e_event;
			}
		}

		private string Timestamp() {
			DateTime time_st = DateTime.Now;
			return  (time_st.ToString());
		}
	}
}
