﻿/*
	Created by Tech_dog (ebontrop@gmail.com) on 15-Jan-2020 at 9:41:57a, UTC+7, Novosibirsk, Tulenina, Wednesday;
	This is Fake GPS timer-based data provider class implementation file;
 */
using System;
using System.ComponentModel;            // for BackgroundWorker definition;
using System.Collections.ObjectModel;   // for observable collection;
using System.Threading;

using System.Device.Location;           // System.Device.dll must be referred for geo-watcher class;

namespace Fake.Core.UI.Data {

	class CTimerDataProvider : IDataProvider {

		private readonly BackgroundWorker back_worker = null;
		private readonly ObservableCollection<CDataRecord> data_recs = null;

		public string Error { get; set; }
		public int    Wait4 { get; set; }

		public event OnEvent Event;

		public CTimerDataProvider(int _wait_in_ms) {
			back_worker = new BackgroundWorker() {
				WorkerReportsProgress      = true,
				WorkerSupportsCancellation = true
			};
			back_worker.DoWork += new DoWorkEventHandler(_on_work);
			back_worker.RunWorkerCompleted += new RunWorkerCompletedEventHandler(_on_complete);
			back_worker.ProgressChanged += new ProgressChangedEventHandler(_on_progress);
			this.Wait4 = _wait_in_ms;

			data_recs  = new ObservableCollection<CDataRecord>();
		}
		/////////////////////////////////////////////////////////////////////////////
		public bool Break() {
			bool b_result = false;
			if ( b_result ) {
			}
			else if (back_worker == null) { this.Error = "Worker object is not created;"; }
			else if (back_worker.IsBusy == false) { this.Error = "Worker object is not started;"; }
			else {
				back_worker.CancelAsync(); b_result = true;
			}
			return (b_result);
		}
		public bool Start(OnEvent _handler) { bool b_result = false;
			if ( b_result ) {
			}
			else if (back_worker == null) { this.Error = "Worker object is not created;"; }
			else if (back_worker.IsBusy == true) { this.Error = "Worker object is started;"; }
			else {
				this.Event = _handler;
				back_worker.RunWorkerAsync(argument: _handler); b_result = true;
			}
			return (b_result);
		}
		public bool Stop () { return this.Break(); }

		/////////////////////////////////////////////////////////////////////////////

		private string Timestamp() {
			DateTime time_st = DateTime.Now;
			return  (time_st.ToString());
		}

		/////////////////////////////////////////////////////////////////////////////
		// https://docs.microsoft.com/en-us/dotnet/framework/winforms/controls/how-to-run-an-operation-in-the-background
		private void _on_work(object sender, DoWorkEventArgs e) {
			BackgroundWorker bw = sender as BackgroundWorker;
			while (bw.CancellationPending == false) {
#if false
				OnEvent handler_ = (OnEvent)e.Argument;
				if (handler_!= null)
					handler_(this.Timestamp(), "");
#else
				CDataRecord rec_ = new CDataRecord();

				GeoCoordinateWatcher watcher = new GeoCoordinateWatcher();
				bool b_started = watcher.TryStart(
					true, TimeSpan.FromMilliseconds(this.Wait4)
				);

				if (b_started == false) {
					rec_.Acceptable  = false;
					rec_.Description = "Geo-watcher start is timed out;";
				}
				else {
					rec_.Position    = new CGeoPosition(watcher.Position.Location);
					rec_.Acceptable  =!rec_.Position.IsUnknown;
					
					if (rec_.Acceptable == false) {
						rec_.Description = "Unknown latitude and longitude;";
					}
				}
				data_recs.Add (rec_);
				bw.ReportProgress(0);
				watcher.Dispose();
#endif
				Thread.Sleep(this.Wait4);
			}
			e.Cancel = true;
		}

		private void _on_complete(object sender, RunWorkerCompletedEventArgs e) {
			if (e.Error != null) {
				this.Error = e.Error.Message;
			}
			else if (e.Cancelled) {
				this.Error = "Generating events is canceled;";
			}
			else {
				this.Error = "";
			}
		}

		private void _on_progress(object sender, ProgressChangedEventArgs e) {

			foreach (CDataRecord rec_ in data_recs) {
				if (null == this.Event)
					continue;
				if (rec_.Acceptable)
					this.Event(rec_.Timestamp, rec_.Position.ToText());
				else
					this.Event(rec_.Timestamp, rec_.Description);
			}
			data_recs.Clear();
		}
	};
}