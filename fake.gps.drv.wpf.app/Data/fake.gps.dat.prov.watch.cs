﻿/*
	Created by Tech_dog (ebontrop@gmail.com) on 22-Jan-2020 at 11:02:22a, UTC+7, Novosibirsk, Tulenina, Wednesday;
	This is Fake GPS geo position watcher data provider class implementation file;
 */
using System;
using System.Device.Location;  // System.Device.dll must be referred;

namespace Fake.Core.UI.Data {

	class CWatchDataProvider : IDataProvider {

		private GeoCoordinateWatcher m_watcher = null;

		public string Error   { get; private set; }
		public bool   Running { get; private set; }
		public double Movement{ get;         set; }

		public event  OnEvent Event;

		public CWatchDataProvider() { this.Running = false; this.Error = ""; this.Movement = 10; }
		/////////////////////////////////////////////////////////////////////////////
		public bool Break() {
			bool b_result = false;
			if (!this.Running) {
				this.Error = "Watching process is already stopped;";
				return (b_result);
			}
			if (m_watcher != null) {
				this.m_watcher.Stop();
				this.m_watcher.Dispose();
				this.m_watcher = null;
			}
			return (b_result = !(this.Running = false));
		}

		public bool Start(OnEvent _handler) { bool b_result = false;
			if (this.Running) {
				this.Error = "Watching process is already started;";
				return (b_result);
			}
			this.m_watcher = new GeoCoordinateWatcher(GeoPositionAccuracy.Default) {
				MovementThreshold = this.Movement
			};
			this.Event = _handler;

			this.m_watcher.PositionChanged += new EventHandler<GeoPositionChangedEventArgs<GeoCoordinate>>(OnPositionChanged);
			this.m_watcher.StatusChanged   += new EventHandler<GeoPositionStatusChangedEventArgs>(this.OnStatusChanged);
			this.m_watcher.Start(/*bool suppressPermissionPrompt:=*/true);

			return (b_result = (this.Running = true));
		}
		public bool Stop () { return this.Break(); }
		/////////////////////////////////////////////////////////////////////////////

		private string Timestamp() {
			DateTime time_st = DateTime.Now;
			return  (time_st.ToString());
		}
		/////////////////////////////////////////////////////////////////////////////
		// https://docs.microsoft.com/en-us/dotnet/api/system.device.location.geopositionstatuschangedeventargs

		private void OnPositionChanged(object sender, GeoPositionChangedEventArgs<GeoCoordinate> e) {
			double d_lat = e.Position.Location.Latitude;
			double d_lon = e.Position.Location.Longitude;

			string cs_pos = string.Format("geo-pos: lat={0}; lon={1}", d_lat, d_lon);

			if (null != this.Event)
			this.Event( this.Timestamp(), cs_pos);
		}

		private void OnStatusChanged  (object sender, GeoPositionStatusChangedEventArgs e) {

			string cs_rec = "#n/a";

			switch (e.Status) {
			case GeoPositionStatus.Disabled    : { cs_rec = "geo-pos-status: disabled;";     } break;
			case GeoPositionStatus.Initializing: { cs_rec = "geo-pos-status: initializing;"; } break;
			case GeoPositionStatus.NoData      : { cs_rec = "geo-pos-status: nodata;";       } break;
			case GeoPositionStatus.Ready       : { cs_rec = "geo-pos-status: ready;";        } break;
			}

			if (null != this.Event)
			this.Event( this.Timestamp(), cs_rec);
		}
	}
}
