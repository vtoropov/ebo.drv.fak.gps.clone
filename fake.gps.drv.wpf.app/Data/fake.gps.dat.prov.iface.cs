﻿/*
	Created by Tech_dog (ebontrop@gmail.com) on 14-Jan-2020 at 7:09:12p, UTC+7, Novosibirsk, Tulenina, Tuesday;
	This is Fake GPS data provider interface declaration file;
 */
using System;
using System.Device.Location;           // System.Device.dll must be referred for geo-watcher class;

namespace Fake.Core.UI.Data {

	public enum eProviderType {
		e_event,
		e_timer
	}

	class CGeoPosition : GeoCoordinate {

		public CGeoPosition() { }
		public CGeoPosition(GeoCoordinate _coord) : base(
			_coord.Latitude, _coord.Longitude, _coord.Altitude, _coord.HorizontalAccuracy, _coord.VerticalAccuracy, _coord.Speed, _coord.Course
			) {
		}

		public bool   IsLatitude () { return (-90.0  <= this.Latitude  && this.Latitude  <= 90.0);  }
		public bool   IsLongitute() { return (-180.0 <= this.Longitude && this.Longitude <= 180.0); }
		public bool   IsValid() { return (this.IsLatitude() && this.IsLongitute()); }
		public string ToText () {
			string cs_pos = string.Format("lat={0}; lon={1};", this.Latitude, this.Longitude);
			return cs_pos;
		}
	}

	class CDataRecord {
		public bool   Acceptable     { get; set; }
		public string Description    { get; set; }
		public CGeoPosition Position { get; set; }
		public string Timestamp      { get; set; }

		public CDataRecord(bool _b_accept = true) {
			this.Timestamp   = DateTime.Now.ToString(); this.Acceptable = _b_accept;
		}
	}

	public delegate void OnEvent(string _timestamp, string _record);

	interface IDataProvider {
		string Error { get; }
		bool   Break ();
		bool   Start (OnEvent _handler);
		bool   Stop  ();
	}
}
