// 2016 OK

/*++

Module:

    Driver.h

Description:

    This module contains the type definitions for the sensors service
    driver callback class.

--*/

#pragma once

#include "fake.gps.tcp.trace.h"
#include "fake.gps.tcp.server.h"
#include "fake.gps.pps.server.h"
#include "fake.gps.pps.server.overlap.h"
#include "fake.gps.cmd.defs.h"

#include "FakeGPS.h"
//
// This class handles driver events for the sensors service driver.
// It supports the OnDeviceAdd event, which occurs when the driver is called
// to setup per-device handlers for a new device stack.
//

class ATL_NO_VTABLE CMyDriver :
    public CComObjectRootEx<CComMultiThreadModel>,
    public CComCoClass<CMyDriver, &CLSID_FakeGPS>,
    public IDriverEntry,
	public shared::drv::um::ICommandProcessor
{
private:
	shared::drv::um::CWsa_Trace_Sender   m_trace;
	shared::drv::um::CWsa_Server         m_t_srv;
	shared::drv::um::CPipeServer_Overlap m_p_srv;

	::ATL::CComPtr<CFakeGPS>             m_p_gps;

public:

    CMyDriver();

    DECLARE_NO_REGISTRY()
    DECLARE_CLASSFACTORY()
    DECLARE_NOT_AGGREGATABLE(CMyDriver)

    BEGIN_COM_MAP(CMyDriver)
        COM_INTERFACE_ENTRY(IDriverEntry)
    END_COM_MAP()

public:

    // IDriverEntry
    STDMETHOD (OnInitialize)(_In_ IWDFDriver* pDriver);
    STDMETHOD (OnDeviceAdd )(_In_ IWDFDriver* pDriver, _In_ IWDFDeviceInitialize* pDeviceInit);
    STDMETHOD_(void, OnDeinitialize)(_In_ IWDFDriver* pDriver);

private: // shared::drv::um::ICommandProcessor
#pragma warning(disable: 4481)
	virtual HRESULT   ICmdProc_Get(shared::drv::um::ICommand&) override sealed;
	virtual HRESULT   ICmdProc_Set(shared::drv::um::ICommand&) override sealed;
#pragma warning(default: 4481)
};

OBJECT_ENTRY_AUTO(__uuidof(FakeGPS), CMyDriver)