// 2016 OK

/*++

Module:

    Driver.cpp

Description:

    This module contains the implementation for the sensors service driver callback class.

--*/
#include "Internal.h"
#include "FakeGPSLib.h" // IDL Generated File
#include "Driver.tmh"
#include "Driver.h"
#include "Sensor.h"
#include "GeolocationSensor.h"
#if (0)
#include "FakeGPS.h"
#include "shared.log.journal.h"
#include "shared.net.wsa._if.h"
#else
#include "fake.gps.tcp.guard.h"
#include "fake.gps.tcp.cfg.h"
#endif

using namespace shared::drv::um;
/////////////////////////////////////////////////////////////////////////////

CMyDriver::CMyDriver(void) : m_p_srv(*this, Trace(), TPipeMode::e_bin_mode) {}

/*++

CMyDriver::OnDeviceAdd
    The framework call this function when device is detected. This driver creates a device callback object

Parameters:
    pDriver     - pointer to an IWDFDriver object
    pDeviceInit - pointer to a device initialization object

Return Values:
    S_OK: device initialized successfully

--*/
HRESULT CMyDriver::OnDeviceAdd(
    _In_ IWDFDriver* pDriver,
    _In_ IWDFDeviceInitialize* pDeviceInit)
{
    // informational at the start of the run in case there are no other log entries to view
#if (0)
    Trace(TRACE_LEVEL_CRITICAL   , "   ");
    Trace(TRACE_LEVEL_CRITICAL   , "------------------------------ START ------------------------------------------");
    Trace(TRACE_LEVEL_CRITICAL   , "FakeGPS - Trace log running with TRACE_LEVEL == CRITICAL");
    Trace(TRACE_LEVEL_ERROR      , "FakeGPS - Trace log running with TRACE_LEVEL == ERROR");
    Trace(TRACE_LEVEL_WARNING    , "FakeGPS - Trace log running with TRACE_LEVEL == WARNING");
    Trace(TRACE_LEVEL_INFORMATION, "FakeGPS - Trace log running with TRACE_LEVEL == INFORMATION");
    Trace(TRACE_LEVEL_VERBOSE    , "FakeGPS - Trace log running with TRACE_LEVEL == VERBOSE");

    Trace(TRACE_LEVEL_INFORMATION, "%!FUNC! Entry");
#else
	Trace() << __FUNCTION__;
#endif
    HRESULT hr = CFakeGPS::CreateInstance(pDriver, pDeviceInit, m_p_gps);
	if (SUCCEEDED(hr))
		Trace() << "CMyDriver::OnDeviceAdd(): succees;";
	else
		Trace().PutLine(
				"CMyDriver::OnDeviceAdd(): error=0x%x;", hr
			);
    return hr;
}

/*++

CMyDriver::OnInitialize

    The framework calls this function just after loading the driver. The driver
    can perform any global, device independent initialization in this routine.

--*/
HRESULT CMyDriver::OnInitialize(
    _In_ IWDFDriver* pDriver)  {
    UNREFERENCED_PARAMETER(pDriver);
#if (0)
	shared::net::CWsa_Guard wsa_guard;
	if (wsa_guard.Secured() == false) {
		wsa_guard.Capture();

		if (wsa_guard.Error().State()) {
			shared::log::CEventJournal::LogError(wsa_guard.Error());
		}
		else {
			CAtlString cs_msg(_T("CMyDriver::OnInitialize()::Wsa has initialied;"));
			shared::log::CEventJournal::LogInfoA("CMyDriver::OnInitialize()::Wsa has initialied;");
		}
	}
#else
	CWsa_Guard wsa_guard;
	HRESULT hr_ = wsa_guard.Capture();
	if (SUCCEEDED(hr_)) {

		m_trace.Address() = CWsa_Cfg().TracePair().Sender();
		m_trace.Delay  () = 100;
		m_trace.Cache  ().Preserve(false); // cannot preserve cash due to a string being send may have length much less;
		m_trace.Timeout().Set(CTimeout::e_default, 100); // sets send timeout in msecs;

		hr_ = m_trace.Cache().Create(CWsa_Cfg().CacheSize(), CWsa_Cache::e_char);
		hr_ = m_trace.Turn (true);

		m_t_srv.Address()  = CWsa_Cfg().CmdPair().Sender();
		m_t_srv.Cache  ().Preserve(true);
		m_t_srv.Timeout().Set(CTimeout::e_sending, 5000); // sets send timeout in msecs;
		m_t_srv.Timeout().Set(CTimeout::e_receive, 5000); // sets recv timeout in msecs;

		hr_ = m_t_srv.Cache().Create(CWsa_Cfg().CacheSize(), CWsa_Cache::e_char);
		hr_ = m_t_srv.Turn (true);

		hr_ = m_p_srv.Turn (true);

		m_trace << __FUNCTION__;
	}
#endif
    return S_OK;
}

/*++

CMyDriver::OnDeinitialize

    The framework calls this function just before de-initializing itself. All
    WDF framework resources should be released by driver before returning
    from this call.

--*/
void CMyDriver::OnDeinitialize(
    _In_ IWDFDriver* pDriver) {
    UNREFERENCED_PARAMETER(pDriver);
#if (0)
	shared::net::CWsa_Guard wsa_guard;
	if (wsa_guard.Secured() == true) {
		wsa_guard.Release();

		if (wsa_guard.Error().State()) {
			shared::log::CEventJournal::LogError(wsa_guard.Error());
		}
		else
			shared::log::CEventJournal::LogInfoA("CMyDriver::OnDeinitialize()::Wsa has released;");
			shared::log::CEventJournal::LogInfo(_T("CMyDriver::OnDeinitialize()::Wsa has released;"));
	}
#else

	m_trace << "CMyDriver::OnDeinitialize::begin()";
	m_p_srv.Turn(false);
	m_t_srv.Turn(false);
	m_trace << "CMyDriver::OnDeinitialize::end()";
	m_trace.Turn(false);

	CWsa_Guard wsa_guard; wsa_guard.Release();
#endif
    return;
}


#include "fake.gps.cmd.geo.loc.h"
/////////////////////////////////////////////////////////////////////////////

HRESULT   CMyDriver::ICmdProc_Get(shared::drv::um::ICommand& _cmd) {

	HRESULT hr_ = S_OK;
	if (NULL == m_p_gps)
		return (hr_ = OLE_E_BLANK);

	switch (_cmd.Type()) {
	case TCmdType::e_locate : { // TODO: it looks like not reliable; must be reviewed;

			shared::drv::um::CCmd_Geo_loc& geo_loc = reinterpret_cast<shared::drv::um::CCmd_Geo_loc&>(_cmd);

			TSensorMgrRawPtr p_mgr = m_p_gps->GetManagerPtr();

			if (NULL != p_mgr && p_mgr->m_pSensorList.GetCount()) {

				CGeolocationSensor* p_sensor = reinterpret_cast<CGeolocationSensor*>(p_mgr->m_pSensorList.GetHead());
				if (p_sensor) {
					geo_loc.Latitude (p_sensor->Latitude ());
					geo_loc.Longitude(p_sensor->Longitude());

					Trace_ex() << "CMyDriver::ICmdProc_Get::geo command is accepted;";
				}
			}

		} break;
	default:
		hr_ = DISP_E_TYPEMISMATCH;
	}

	return hr_;
}

HRESULT   CMyDriver::ICmdProc_Set(shared::drv::um::ICommand& _cmd) {

	HRESULT hr_ = S_OK;
	if (NULL == m_p_gps)
		return (hr_ = OLE_E_BLANK);

	switch (_cmd.Type()) {
	case TCmdType::e_locate : {

			shared::drv::um::CCmd_Geo_loc& geo_loc = reinterpret_cast<shared::drv::um::CCmd_Geo_loc&>(_cmd);

			TSensorMgrRawPtr p_mgr = m_p_gps->GetManagerPtr();

			if (NULL != p_mgr && p_mgr->m_pSensorList.GetCount()) {

				CGeolocationSensor* p_sensor = reinterpret_cast<CGeolocationSensor*>(p_mgr->m_pSensorList.GetHead());
				if (p_sensor) {
					p_sensor->Latitude (geo_loc.Latitude ());
					p_sensor->Longitude(geo_loc.Longitude());

				//	const bool b_active = p_mgr->m_fDeviceActive; p_mgr->m_fDeviceActive = true;

				//	hr_ = p_sensor->UpdateGeolocationDataFieldValues();
					if (SUCCEEDED(hr_))
					Trace_ex() << "CMyDriver::ICmdProc_Get::set geo command succeeded;";
					else {
					Trace_ex().PutLine(
						"CMyDriver::ICmdProc_Get::set geo command error=0x%x;", hr_
						);
					}

				//	p_mgr->m_fDeviceActive = b_active;
				}
			}
		} break;
	default:
		hr_ = DISP_E_TYPEMISMATCH;
	}

	return hr_;
}