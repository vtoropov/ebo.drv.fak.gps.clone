/*
	Created by Tech_dog (ebontrop@gmail.com) on 11-Dec-2019 at 9:05:22p, UTC+7, Novosibirsk, Tulenina, Wednesday;
	This is Fake GPS driver TCP/IP generic address interface implementation file.
*/
#include "StdAfx.h"
#include "fake.gps.tcp.address.h"

using namespace shared::drv::um;
/////////////////////////////////////////////////////////////////////////////

CWsa_Address:: CWsa_Address(void) { ::memset(&m_data, 0, sizeof(SOCKADDR_IN)); m_data.sin_family = AF_INET; }
CWsa_Address::~CWsa_Address(void) {}

/////////////////////////////////////////////////////////////////////////////

HRESULT    CWsa_Address::Ip(LPCSTR _lp_sz_ip) {
	HRESULT hr_ = S_OK;
	if (NULL == _lp_sz_ip || !::strlen(_lp_sz_ip))
		return (hr_ = E_INVALIDARG);

	m_data.sin_addr.s_addr = ::inet_addr(_lp_sz_ip);

	return  hr_;
}

bool       CWsa_Address::Is (void) const {
	bool b_res = false;

	if (0 == m_data.sin_addr.S_un.S_un_b.s_b1 &&
	    0 == m_data.sin_addr.S_un.S_un_b.s_b2 &&
	    0 == m_data.sin_addr.S_un.S_un_b.s_b3 &&
	    0 == m_data.sin_addr.S_un.S_un_b.s_b4)  return b_res;

	if (0 == m_data.sin_port) return b_res;

	b_res = true;
	return b_res;
}

HRESULT    CWsa_Address::Local(const bool _b_prev) {
	HRESULT hr_ = S_OK;

	if (_b_prev) {
		CHAR sz_host[_MAX_PATH] = {0};
		const INT n_res = ::gethostname(sz_host, _countof(sz_host) * sizeof(CHAR));
		if (SOCKET_ERROR == n_res) {
			return (hr_ = __DwordToHresult(::WSAGetLastError()));
		}

		struct hostent* p_he = ::gethostbyname(sz_host);
		if (NULL == p_he) {
			return (hr_ = __DwordToHresult(::WSAGetLastError()));
		}

		m_data.sin_addr.S_un.S_un_b.s_b1 = p_he->h_addr_list[0][0];
		m_data.sin_addr.S_un.S_un_b.s_b2 = p_he->h_addr_list[0][1];
		m_data.sin_addr.S_un.S_un_b.s_b3 = p_he->h_addr_list[0][2];
		m_data.sin_addr.S_un.S_un_b.s_b4 = p_he->h_addr_list[0][3];
	}
	else {
		struct addrinfoW*  p_res = NULL; // TODO: must be used a macro for defining appropriate structure declaration;
		struct addrinfoW*  p_ptr = NULL;
		struct addrinfoW   hints = {0} ;
		hints.ai_family   = AF_UNSPEC  ;
		hints.ai_socktype = SOCK_STREAM;
		hints.ai_protocol = IPPROTO_TCP;
#if (0)
	//	CAtlString leads to consuming CPU time by WUDFHost.exe during driver update; this class was missed for removing;
		CAtlString cs_port; cs_port.Format(_T("%u"), m_data.sin_port);
		const DWORD d_res = ::GetAddrInfo(_T("localhost"), (LPCTSTR)cs_port, &hints, &p_res);
#else
		TCHAR lp_s_buffer[32] = {0};
		::_stprintf_s(lp_s_buffer, _countof(lp_s_buffer), _T("%u"), m_data.sin_port);
		const DWORD d_res = ::GetAddrInfo(_T("localhost"), lp_s_buffer, &hints, &p_res);
#endif
		if (d_res)
			return (hr_ = __DwordToHresult(::WSAGetLastError()));

		bool b_found = false;
		for (p_ptr = p_res; p_ptr != NULL; p_ptr->ai_next) {
			switch ( p_ptr->ai_family ){
			case AF_INET    : {
				m_data.sin_addr.S_un.S_un_b = (reinterpret_cast<sockaddr_in*>(p_ptr->ai_addr))->sin_addr.S_un.S_un_b; b_found = true;
			} break;
			case AF_INET6   : {} break;
			case AF_NETBIOS : {} break;
			case AF_UNSPEC  : {} break;
			}
			if (b_found)
				break;
		}
		if (false == b_found)
			hr_ = DISP_E_TYPEMISMATCH;

		if (p_res) {
			::FreeAddrInfoW(p_res); p_res = NULL;
		}
	}
	return  hr_;
}

HRESULT    CWsa_Address::Port(const USHORT _val) {
	HRESULT hr_ = S_OK;
	if (0 == _val)
		return (hr_ = E_INVALIDARG);

	m_data.sin_port = ::htons(_val);

	return  hr_;
}

/////////////////////////////////////////////////////////////////////////////

PSOCKADDR     CWsa_Address::Ptr (void) { return reinterpret_cast<PSOCKADDR>(&m_data); }
SOCKADDR_IN&  CWsa_Address::Ref (void) { return  m_data; }

/////////////////////////////////////////////////////////////////////////////

CWsa_Address& CWsa_Address::operator << (LPCSTR   _lp_sz_val) { this->Ip(_lp_sz_val); return *this; }
CWsa_Address& CWsa_Address::operator << (const USHORT _u_val) { this->Port(_u_val)  ; return *this; }

/////////////////////////////////////////////////////////////////////////////

CWsa_Address::operator const bool(void) const { return this->Is (); }
CWsa_Address::operator PSOCKADDR (void)       { return this->Ptr(); }
CWsa_Address::operator const INT (void) const { return sizeof(SOCKADDR_IN); }

/////////////////////////////////////////////////////////////////////////////

CWsa_Address_bcast_rcv:: CWsa_Address_bcast_rcv(const USHORT _u_port) : TBase() {
	TBase::m_data.sin_port = ::htons(_u_port);
	TBase::m_data.sin_addr.s_addr = INADDR_ANY;
}
CWsa_Address_bcast_rcv::~CWsa_Address_bcast_rcv(void) {}

/////////////////////////////////////////////////////////////////////////////

CWsa_Address_bcast_snd:: CWsa_Address_bcast_snd(const USHORT _u_port) : TBase() {
	TBase::m_data.sin_port = ::htons(_u_port);
	TBase::m_data.sin_addr.s_addr = INADDR_BROADCAST;
}
CWsa_Address_bcast_snd::~CWsa_Address_bcast_snd(void) {}