/*
	Created by Tech_dog (ebontrop@gmail.com) on 12-Dec-2019 at 7:57:27p, UTC+7, Novosibirsk, Tulenina, Thursday;
	This is Fake GPS driver TCP/IP generic server/client/trace base interface implementation file.
*/
#include "StdAfx.h"
#include "fake.gps.tcp.base.h"

using namespace shared::drv::um;
/////////////////////////////////////////////////////////////////////////////

namespace shared { namespace drv { namespace um { namespace _impl {

	DWORD WINAPI _thread_fun(CWsa_Base* _p_bas){
		if (NULL == _p_bas)
			return ERROR_INVALID_ADDRESS;
		else
			return _p_bas->Thread_Fun();
	}

}}}}
using namespace shared::drv::um::_impl;
/////////////////////////////////////////////////////////////////////////////

CWsa_Base:: CWsa_Base(const DWORD _freq_in_ms) : 
	m_thread(NULL), m_break_it_on(false), m_delay(100, _freq_in_ms), m_stop_evt(NULL) {
}
CWsa_Base::~CWsa_Base(void) {}

/////////////////////////////////////////////////////////////////////////////
const
CWsa_Address&  CWsa_Base::Address(void) const { return m_trs_a;  }
CWsa_Address&  CWsa_Base::Address(void)       { return m_trs_a;  }
const
CWsa_Cache&    CWsa_Base::Cache  (void) const { return m_cache;  }
CWsa_Cache&    CWsa_Base::Cache  (void)       { return m_cache;  }
const
CDelayEvent&   CWsa_Base::Delay  (void) const { return m_delay;  }
CDelayEvent&   CWsa_Base::Delay  (void)       { return m_delay;  }

/////////////////////////////////////////////////////////////////////////////

bool           CWsa_Base::IsOn   (void) const { return NULL != m_thread; }
TTimeout_r     CWsa_Base::Timeout(void) const { return m_timout; }
TTimeout_w     CWsa_Base::Timeout(void)       { return m_timout; }
HRESULT        CWsa_Base::Turn   (const bool _b_on)  {

	if (_b_on == true  && this->IsOn() == true ) return (__DwordToHresult(ERROR_INVALID_STATE));
	if (_b_on == false && this->IsOn() == false) return (__DwordToHresult(ERROR_INVALID_STATE)); HRESULT hr_ = S_OK;

	if (_b_on) {

		m_break_it_on = false;
		DWORD dw_id_  = 0;

		m_stop_evt = ::CreateEvent(NULL, TRUE, FALSE, NULL);

		m_thread = ::CreateThread(NULL, 0, (LPTHREAD_START_ROUTINE)_thread_fun, this, 0, &dw_id_);
		if (NULL == m_thread)
			hr_  = __DwordToHresult(::GetLastError());
	}
	else {
		m_break_it_on = true;

		if (m_stop_evt) { ::WaitForSingleObject(m_stop_evt, INFINITE);  }
		if (m_stop_evt) { ::CloseHandle(m_stop_evt);  m_stop_evt = NULL;}
		if (m_thread)   { ::CloseHandle(m_thread)  ;  m_thread = NULL;  }
	}

	return hr_;
}