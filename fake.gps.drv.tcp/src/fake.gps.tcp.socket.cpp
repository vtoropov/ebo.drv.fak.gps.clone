/*
	Created by Tech_dog (ebontrop@gmail.com) on 11-Dec-2019 at 8:33:45p, UTC+7, Novosibirsk, Tulenina, Wednesday;
	This is Fake GPS driver TCP/IP generic socket interface implementation file.
*/
#include "StdAfx.h"
#include "fake.gps.tcp.socket.h"

using namespace shared::drv::um;
/////////////////////////////////////////////////////////////////////////////

CWsa_Socket:: CWsa_Socket(void) : m_sock(NULL), m_type(_type::e_udp) {}
CWsa_Socket::~CWsa_Socket(void) {}

/////////////////////////////////////////////////////////////////////////////

HRESULT  CWsa_Socket::Create(const _type e_type, const bool b_udp) {

	if (this->IsValid() == true)
		return (__DwordToHresult(ERROR_ALREADY_INITIALIZED));

	if (e_type == _type::e_tcp) {
		m_sock = ::WSASocket(AF_INET, SOCK_STREAM, IPPROTO_TCP, NULL, 0, 0);
	}
	else {
		m_sock = ::socket(PF_INET, SOCK_DGRAM, (b_udp ? IPPROTO_UDP : 0));
	}

	if (INVALID_SOCKET == m_sock)
		return (__DwordToHresult(::WSAGetLastError()));
	else
		return S_OK;
}

HRESULT  CWsa_Socket::Destroy(void) {
	
	if (this->IsValid() == false)
		return (__DwordToHresult(ERROR_INVALID_STATE));

	const INT n_res = ::closesocket(m_sock);
	if (SOCKET_ERROR == n_res)
		return (__DwordToHresult(::WSAGetLastError()));
	else
		m_sock = NULL;

	return S_OK;
}

bool      CWsa_Socket::IsValid(void) const { return (NULL != m_sock); }
SOCKET&   CWsa_Socket::Ref    (void)       { return m_sock;  }

/////////////////////////////////////////////////////////////////////////////

CWsa_Socket::operator const SOCKET& (void) const { return m_sock; }
CWsa_Socket::operator       SOCKET& (void)       { return m_sock; }