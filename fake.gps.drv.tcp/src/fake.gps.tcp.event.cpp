/*
	Created by Tech_dog (ebontrop@gmail.com) on 11-Dec-2019 at 8:02:19p, UTC+7, Novosibirsk, Tulenina, Wednesday;
	This is Fake GPS driver TCP/IP waitable event interface implementation file.
*/
#include "StdAfx.h"
#include "fake.gps.tcp.event.h"

using namespace shared::drv::um;
/////////////////////////////////////////////////////////////////////////////

CDelayEvent:: CDelayEvent(const DWORD nTimeSlice, const DWORD nTimeFrame) : m_nTimeSlice(nTimeSlice), m_nTimeFrame(nTimeFrame), m_nCurrent(0) {
	ATLASSERT(m_nTimeSlice);
	ATLASSERT(m_nTimeFrame);
}

CDelayEvent::~CDelayEvent(void) {
}

/////////////////////////////////////////////////////////////////////////////

bool CDelayEvent::Elapsed(void) const { return (m_nCurrent >= m_nTimeFrame); }
bool CDelayEvent::IsReset(void) const { return (m_nCurrent == 0); }

VOID CDelayEvent::Reset(const DWORD nTimeSlice, const DWORD nTimeFrame) {
	if (0 < nTimeSlice) m_nTimeSlice = nTimeSlice; ATLASSERT(m_nTimeSlice);
	if (0 < nTimeFrame) m_nTimeFrame = nTimeFrame; ATLASSERT(m_nTimeFrame);
	
	m_nCurrent = 0;
}

void CDelayEvent::Wait(void) {
	::Sleep(m_nTimeSlice);
	m_nCurrent += m_nTimeSlice;
}

/////////////////////////////////////////////////////////////////////////////

CDelayEvent& CDelayEvent::operator= (const DWORD _v) { if (_v) { m_nTimeFrame = _v; }  return *this; }

/////////////////////////////////////////////////////////////////////////////

namespace shared { namespace drv { namespace um { namespace _impl {

	timeval CTimeout_Set (const LONG _timeout) {

		static LONG l_scale = 1000;
		timeval t_out_ = {0};

		t_out_.tv_sec  = _timeout / l_scale;
		t_out_.tv_usec =(_timeout - t_out_.tv_sec * l_scale) * l_scale;

		return t_out_;
	}

}}}}
using namespace shared::drv::um::_impl;
/////////////////////////////////////////////////////////////////////////////

CTimeout:: CTimeout(void) { ::memset(m_outs, 0, sizeof(LONG) * _countof(m_outs)); }
CTimeout::~CTimeout(void) {}

/////////////////////////////////////////////////////////////////////////////

HRESULT  CTimeout::Apply(SOCKET _socket, const CTimeout::_oper _oper) {
	if (NULL == _socket)
		return E_INVALIDARG;

	LONG l_out = m_outs[_oper];
	if ( l_out == 0)
		return S_FALSE;

	HRESULT hr_ = S_OK;

	INT n_res = 0;
	switch (_oper) {
	case CTimeout::e_default: {
			fd_set   set_ = {0};
			FD_ZERO(&set_);
			FD_SET (_socket, &set_);

			timeval out_ = CTimeout_Set(l_out);

			// https://docs.microsoft.com/en-us/windows/win32/api/winsock2/nf-winsock2-select
			n_res = ::select(0, &set_, NULL, NULL, &out_);
			if (n_res < 1)
				hr_ = S_FALSE;
		} break;
	case CTimeout::e_receive: {
			// https://docs.microsoft.com/en-us/windows/win32/api/winsock/nf-winsock-setsockopt
			n_res = ::setsockopt(_socket, SOL_SOCKET, SO_RCVTIMEO, reinterpret_cast<char*>(&l_out), sizeof(LONG));
			if (SOCKET_ERROR == n_res)
				hr_ = __DwordToHresult(::WSAGetLastError());
		} break;
	case CTimeout::e_sending: {
			n_res = ::setsockopt(_socket, SOL_SOCKET, SO_SNDTIMEO, reinterpret_cast<char*>(&l_out), sizeof(LONG));
			if (SOCKET_ERROR == n_res)
				hr_ = __DwordToHresult(::WSAGetLastError());
		} break;
	}
	return  hr_;
}

LONG     CTimeout::Get  (const CTimeout::_oper _oper) const  { return m_outs[_oper]; }
VOID     CTimeout::Set  (const CTimeout::_oper _oper, const LONG _v){ m_outs[_oper] = _v; }