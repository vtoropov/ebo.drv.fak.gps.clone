/*
	Created by Tech_dog (ebontrop@gmail.com) on 14-Dec-2019 at 1:44:51a, UTC+7, Novosibirsk, Tulenina, Saturday;
	This is Fake GPS driver TCP/IP service connect configuration interface implementation file.
*/
#include "StdAfx.h"
#include "fake.gps.tcp.cfg.h"

using namespace shared::drv::um;

/////////////////////////////////////////////////////////////////////////////

CWsa_Connect:: CWsa_Connect(void) {}
CWsa_Connect::~CWsa_Connect(void) {}

/////////////////////////////////////////////////////////////////////////////

TAddress_r     CWsa_Connect::Receiver(void) const { return m_rcv_a; }
TAddress_w     CWsa_Connect::Receiver(void)       { return m_rcv_a; }
TAddress_r     CWsa_Connect::Sender  (void) const { return m_svc_a; }
TAddress_w     CWsa_Connect::Sender  (void)       { return m_svc_a; }

/////////////////////////////////////////////////////////////////////////////

CWsa_Connect&  CWsa_Connect::operator<<(TAddress_r _addr) { this->Receiver() = _addr; return *this; }
CWsa_Connect&  CWsa_Connect::operator>>(TAddress_r _addr) { this->Sender  () = _addr; return *this; }

/////////////////////////////////////////////////////////////////////////////

CWsa_Cmd_Pair::CWsa_Cmd_Pair(void) {  // receiver address port is not required, becouse only service port is importat ;
	TConnect::Receiver().Local(true); TConnect::Receiver().Port(   0); // this is a receiver requesting command result;
	TConnect::Sender  ().Local(true); TConnect::Sender()  .Port(8090); // this is a service for processing command;
}

/////////////////////////////////////////////////////////////////////////////

CWsa_Trace_Pair::CWsa_Trace_Pair(void) {
	TConnect::Receiver() = CWsa_Address_bcast_rcv(8080);
	TConnect::Sender  () = CWsa_Address_bcast_snd(8080);
}

/////////////////////////////////////////////////////////////////////////////

CWsa_Cfg:: CWsa_Cfg(void) : m_cache_size(4096) {}
CWsa_Cfg::~CWsa_Cfg(void) {}

/////////////////////////////////////////////////////////////////////////////

ULONG                   CWsa_Cfg::CacheSize(void) const { return m_cache_size; }
const CWsa_Cmd_Pair &   CWsa_Cfg::CmdPair  (void) const { return m_cmd_pair  ; }
const CWsa_Trace_Pair&  CWsa_Cfg::TracePair(void) const { return m_trace_pair; }

/////////////////////////////////////////////////////////////////////////////

/////////////////////////////////////////////////////////////////////////////