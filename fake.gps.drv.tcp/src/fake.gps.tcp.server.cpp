/*
	Created by Tech_dog (ebontrop@gmail.com) on 12-Dec-2019 at 1:42:52p, UTC+7, Novosibirsk, Tulenina, Thursday;
	This is Fake GPS driver TCP/IP generic server interface implementation file.
*/
#include "StdAfx.h"
#include "fake.gps.tcp.server.h"
#include "fake.gps.tcp.trace.h"
#include "fake.gps.cmd.geo.loc.h"
#include "fake.gps.cmd.time.h"

using namespace shared::drv::um;
/////////////////////////////////////////////////////////////////////////////

namespace shared { namespace drv { namespace um { namespace _impl {

}}}}
using namespace shared::drv::um::_impl;
/////////////////////////////////////////////////////////////////////////////

CWsa_Server:: CWsa_Server(const DWORD _freq_in_ms) : TBase(_freq_in_ms) {}
CWsa_Server::~CWsa_Server(void) {}

/////////////////////////////////////////////////////////////////////////////

HRESULT  CWsa_Server::Turn (const bool _b_on)  {
	
	HRESULT hr_ = S_OK; // TODO: actually current mode must be checked before doing anything;

	if (_b_on) {

		hr_ = m_socket.Create(CWsa_Socket::e_udp, false);
		if (m_socket.IsValid() == false)
			return hr_;

		if (TBase::Timeout().Get(CTimeout::e_receive)) TBase::Timeout().Apply(TBase::m_socket, CTimeout::e_receive);
		if (TBase::Timeout().Get(CTimeout::e_sending)) TBase::Timeout().Apply(TBase::m_socket, CTimeout::e_sending);

		INT n_res  = ::bind(m_socket, m_trs_a, m_trs_a);
		if (n_res == SOCKET_ERROR) {
			hr_    = __DwordToHresult(::WSAGetLastError());
			return hr_;
		}
		else
			Trace() << "CWsa_Server::Turn(on): has started to listen client;";
	}
	else {
		hr_ = TBase::Turn(_b_on);
#if (0)
		if (SUCCEEDED(hr_))
			Trace() << "CWsa_Server::Turn(off): success;";
		else
			Trace().PutLine("CWsa_Server::Turn(off): error=0x%x;", hr_);
#endif
	}

	return hr_;
}

/////////////////////////////////////////////////////////////////////////////

DWORD    CWsa_Server::Thread_Fun(void) {

	if (m_socket.IsValid() == false) return ERROR_INVALID_STATE;
	if (m_cache .IsValid() == false) return ERROR_INVALID_DATA ;

	CWsa_Address ad_dest;
	shared::drv::um::CCmd_Time_t cmd_time;

	m_delay.Reset(10, 10);

	while (m_break_it_on == false) {

		m_delay.Wait();
		if (m_delay.Elapsed()) m_delay.Reset();
		else
			continue;

		INT n_result = 0;
		INT n_ad_len = ad_dest;         // gets a length of the destination/client address structure;
		                                // https://docs.microsoft.com/en-us/windows/win32/api/winsock/nf-winsock-recvfrom
		n_result = ::recvfrom(m_socket, m_cache, m_cache, 0, ad_dest, &n_ad_len);
		if (SOCKET_ERROR == n_result) { // https://docs.microsoft.com/en-us/windows/win32/winsock/windows-sockets-error-codes-2

			n_result = ::WSAGetLastError();
			Trace().PutLine("CWsa_Server::recvfrom::error: %d", n_result);

			return static_cast<DWORD>(n_result);
		}
		else if (0 == n_result)         // no data received;
			continue;
		
		if (m_break_it_on)
			break;

		if (cmd_time.Is((PBYTE)m_cache.Data(), m_cache.Size()) == false)
			continue;
		else
			cmd_time.Set(time(NULL));

		n_result = ::sendto(m_socket, (const PCHAR)cmd_time.Raw(), cmd_time.Size(), 0, ad_dest, ad_dest);
		if (SOCKET_ERROR == n_result) {
			return static_cast<DWORD>(::WSAGetLastError());
		}
		else if (static_cast<ULONG>(n_result) < m_cache.Size())
			continue;                   // a receiver may interrupt a connection; it doesn't matter;
	}
	return NO_ERROR;
}