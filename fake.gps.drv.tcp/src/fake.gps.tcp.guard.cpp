/*
	Created by Tech_dog (ebontrop@gmail.com) on 11-Dec-2019 at 7:40:22p, UTC+7, Novosibirsk, Tulenina, Wednesday;
	This is Fake GPS driver TCP/IP WSA initialize guard interface implementation file.
*/
#include "StdAfx.h"
#include "fake.gps.tcp.guard.h"

using namespace shared::drv::um;

/////////////////////////////////////////////////////////////////////////////

namespace shared { namespace drv { namespace um { namespace _impl {

	bool& WSA_Guard_State(void) {
		static bool b_init = false;
		return b_init;
	}

}}}}
using namespace shared::drv::um::_impl;
/////////////////////////////////////////////////////////////////////////////

CWsa_Guard:: CWsa_Guard(void) {}
CWsa_Guard::~CWsa_Guard(void) {}

/////////////////////////////////////////////////////////////////////////////

HRESULT   CWsa_Guard::Capture(void) {
	if (this->Secured() == true)
		return (__DwordToHresult(ERROR_ALREADY_INITIALIZED));

	WSADATA wsa_dat = { 0 };
	const INT n_res = ::WSAStartup(MAKEWORD(2, 2), &wsa_dat);
	if ( 0 == n_res )
		WSA_Guard_State() = true;

	return (0 == n_res ? S_OK : __DwordToHresult(n_res));
}

HRESULT   CWsa_Guard::Release(void) {
	if (this->Secured() == false)
		return (__DwordToHresult(ERROR_INVALID_STATE));
	// https://docs.microsoft.com/en-us/windows/desktop/api/winsock/nf-winsock-wsacleanup
	// ***important***:
	//    terminates socket initialization for all threads;
	const INT n_res = ::WSACleanup();
	if ( 0 == n_res )
		WSA_Guard_State() = false;

	return  (0 == n_res ? S_OK : __DwordToHresult(::WSAGetLastError()));
}

/////////////////////////////////////////////////////////////////////////////

bool      CWsa_Guard::Secured(void) { return WSA_Guard_State(); }

/////////////////////////////////////////////////////////////////////////////

CWsa_Guard::CWsa_Guard(const CWsa_Guard&) {}
CWsa_Guard& CWsa_Guard::operator= (const CWsa_Guard&) { return *this; }