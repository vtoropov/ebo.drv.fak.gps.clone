/*
	Created by Tech_dog (ebontrop@gmail.com) on 11-Dec-2019 at 7:48:40p, UTC+7, Novosibirsk, Tulenina, Wednesday;
	This is Fake GPS driver TCP/IP generic trace interface implementation file.
*/
#include "StdAfx.h"
#include "fake.gps.tcp.trace.h"
#include "fake.gps.tcp.event.h"

using namespace shared::drv::um;
/////////////////////////////////////////////////////////////////////////////

namespace shared { namespace drv { namespace um { namespace _impl {

	typedef std::queue<CWsa_Cache> TTraces;

	TTraces&    CWsa_Trace_Que(void) {
		static TTraces que_;
		return que_;
	}

	CWsa_Trace& CWsa_Trace_Get(void) {
		static CWsa_Trace trace_;
		return trace_;
	}

	class CWsa_Trace_Locker {
	private:
		TAccessGuard& m_guard;

	public:
		 CWsa_Trace_Locker (TAccessGuard& _guard) : m_guard(_guard) {
			 if (::TryEnterCriticalSection(&m_guard)) ::EnterCriticalSection(&m_guard);
		 }
		~CWsa_Trace_Locker (void) {
			__try {
				::LeaveCriticalSection(&m_guard);
			}
			__except(STATUS_NO_MEMORY == ::GetExceptionCode())
			{}
		}
	};

}}}}
using namespace shared::drv::um::_impl;
/////////////////////////////////////////////////////////////////////////////

CWsa_Trace:: CWsa_Trace(void) { ::InitializeCriticalSection(&m_guard); }
CWsa_Trace::~CWsa_Trace(void) { ::DeleteCriticalSection    (&m_guard); }

/////////////////////////////////////////////////////////////////////////////

HRESULT      CWsa_Trace::IPipeTrace_PutError(LPCSTR _lp_sz_desc, const HRESULT _h_result) {
	if (NULL == _lp_sz_desc || !::strlen(_lp_sz_desc))
		return E_INVALIDARG;

	this->PutLine(_lp_sz_desc, _h_result);
	
	return S_OK;
}
HRESULT      CWsa_Trace::IPipeTrace_PutInfo (LPCSTR _lp_sz_desc) {
	if (NULL == _lp_sz_desc || !::strlen(_lp_sz_desc))
		return E_INVALIDARG;

	*this << _lp_sz_desc;
	return S_OK;
}
HRESULT      CWsa_Trace::IPipeTrace_PutInfo (LPCSTR _lp_sz_desc, const DWORD   _d_data  ) {
	if (NULL == _lp_sz_desc || !::strlen(_lp_sz_desc))
		return E_INVALIDARG;

	this->PutLine(_lp_sz_desc, _d_data);
	return S_OK;
}
HRESULT      CWsa_Trace::IPipeTrace_PutInfo (LPCSTR _lp_sz_desc, LPCSTR    _lp_sz_data  ) {
	if (NULL == _lp_sz_desc || !::strlen(_lp_sz_desc))
		return E_INVALIDARG;

	this->PutLine(_lp_sz_desc, _lp_sz_data);
	return S_OK;
}
HRESULT      CWsa_Trace::IPipeTrace_PutWarn (LPCSTR _lp_sz_desc, const HRESULT _h_result) {
	if (NULL == _lp_sz_desc || !::strlen(_lp_sz_desc))
		return E_INVALIDARG;

	this->PutLine(_lp_sz_desc, _h_result);
	return S_OK;
}

/////////////////////////////////////////////////////////////////////////////

const bool   CWsa_Trace::IsEmpty  (void) const { CWsa_Trace_Locker locker(m_guard); return CWsa_Trace_Que().empty(); }

CWsa_Cache   CWsa_Trace::GetLast  (void) const {

	CWsa_Trace_Locker locker(m_guard);

	CWsa_Cache the_first;

	if (CWsa_Trace_Que().empty() == false) {
		the_first = CWsa_Trace_Que().front(); CWsa_Trace_Que().pop();
	}
	return the_first;
}

const void   CWsa_Trace::PutLine  (LPCSTR _lp_sz_fmt, ...) {
	if (NULL == _lp_sz_fmt || !::strlen( _lp_sz_fmt))
		return;

	CWsa_Trace_Locker locker(m_guard);

	HRESULT hr_   = S_OK;
	size_t t_size = 0;

	va_list  args_;
	va_start(args_, _lp_sz_fmt);
	try {
		do {
			t_size += 2048;
			PCHAR p_sz_buf = new CHAR[t_size]; ::memset(p_sz_buf, 0, t_size * sizeof(CHAR));

			hr_ = ::StringCchVPrintfExA (
					p_sz_buf, t_size, NULL, NULL, 0, _lp_sz_fmt, args_
				);
			if (SUCCEEDED(hr_)) {
				CWsa_Cache line_; line_ = p_sz_buf;
				CWsa_Trace_Que().push(line_);
			}
			if (p_sz_buf) {
				delete[] p_sz_buf; p_sz_buf = NULL;
			}

		} while (STRSAFE_E_INSUFFICIENT_BUFFER == hr_);
	}
	catch (const ::std::bad_alloc&) {}
	va_end(args_);
}

/////////////////////////////////////////////////////////////////////////////

CWsa_Trace&  CWsa_Trace::operator << (LPCSTR  _lp_sz ) {

	CWsa_Trace_Locker locker(m_guard);

	CWsa_Cache line_; line_ = _lp_sz;
	CWsa_Trace_Que().push(line_);

	return *this;
}
CWsa_Trace&  CWsa_Trace::operator << (LPCTSTR _lp_sz ) {

	CWsa_Trace_Locker locker(m_guard);

	CWsa_Cache line_; line_ = _lp_sz;
	CWsa_Trace_Que().push(line_);

	return *this;
}

CWsa_Trace&  CWsa_Trace::operator  = (LPCSTR  _lp_sz) { this->m_default = _lp_sz; return *this; }
CWsa_Trace&  CWsa_Trace::operator  = (LPCTSTR _lp_sz) { this->m_default = _lp_sz; return *this; }

/////////////////////////////////////////////////////////////////////////////

CWsa_Trace::operator CWsa_Cache& (void)     { return m_default; }

/////////////////////////////////////////////////////////////////////////////

CWsa_Trace&  shared::drv::um::Trace(void)   { return CWsa_Trace_Get(); }

/////////////////////////////////////////////////////////////////////////////

CWsa_Trace_Sender:: CWsa_Trace_Sender(const DWORD _freq_in_ms) : TBase(_freq_in_ms), m_bcast(true) { CWsa_Trace_Get() << __FUNCTION__; }
CWsa_Trace_Sender::~CWsa_Trace_Sender(void) {}

/////////////////////////////////////////////////////////////////////////////

HRESULT  CWsa_Trace_Sender::Turn (const bool _b_on)  {
	if (_b_on) {
	}
	else {
		Trace() << "CWsa_Trace_Sender::Turn(off);";
	}
	return TBase::Turn(_b_on);
}

/////////////////////////////////////////////////////////////////////////////

HRESULT  CWsa_Trace_Sender::Send (void) {

	static bool b_use_def = false;

	if (CWsa_Trace().IsEmpty()) {
		if (b_use_def == false) {
			b_use_def  = true ; CWsa_Trace_Get() = "Waiting for events...";
			return this->Send(CWsa_Trace_Get());
		}
		else
			return S_OK;
	}

	CWsa_Cache line_ = CWsa_Trace().GetLast(); /*b_use_def = false;*/
	return this->Send(line_);
}

HRESULT  CWsa_Trace_Sender::Send (const CWsa_Cache& _dat) {
	
	if (_dat.IsValid() == false)
		return (__DwordToHresult(ERROR_INVALID_DATA));
	else
		return  this->Send(_dat.Data(), _dat.Size());
}

HRESULT  CWsa_Trace_Sender::Send (PCHAR const _data, const DWORD _size) {
	
	if (NULL == _data || 0 == _size)
		return (E_POINTER);

	HRESULT hr_ = m_socket.Create(CWsa_Socket::e_udp, m_bcast);

	if (m_socket.IsValid() == false)
		return hr_;

	if (m_bcast) { // https://docs.microsoft.com/en-us/windows/win32/api/winsock/nf-winsock-setsockopt
		char ch_b_cast = 1;
		const INT n_res = ::setsockopt(m_socket, SOL_SOCKET, SO_BROADCAST, (char*)&ch_b_cast, sizeof(char));
		if (SOCKET_ERROR == n_res) {
			hr_ = __DwordToHresult(::WSAGetLastError());
			return hr_;
		}
	}

	if (TBase::Timeout().Get(CTimeout::e_sending))
	hr_ = TBase::Timeout().Apply(TBase::m_socket, CTimeout::e_sending);

	if (hr_ == S_OK) {
		const INT n_res = ::sendto(m_socket, _data, _size, 0, m_trs_a, m_trs_a);

		if (n_res == SOCKET_ERROR) {
			hr_   = __DwordToHresult(::WSAGetLastError());
		}
		else if (n_res < (INT)_size) { // sending data is incomplete; TODO: destroy operation overrides the result;
			hr_   = S_FALSE;
		}
	}
	return (hr_ = m_socket.Destroy());
}

/////////////////////////////////////////////////////////////////////////////

CWsa_Trace_Sender& CWsa_Trace_Sender::operator << (LPCSTR  _lp_sz ) { CWsa_Trace_Get() << _lp_sz; return *this; }
CWsa_Trace_Sender& CWsa_Trace_Sender::operator << (LPCTSTR _lp_sz ) { CWsa_Trace_Get() << _lp_sz; return *this; }

/////////////////////////////////////////////////////////////////////////////

DWORD    CWsa_Trace_Sender::Thread_Fun(void) {

	HRESULT hr_ = S_OK;

	while (m_break_it_on == false) {

		m_delay.Wait();
		if (m_delay.Elapsed()) m_delay.Reset();
		else
			continue;

		hr_ = this->Send();
		if (FAILED(hr_)) {
			break;
		}
	}
	if (m_stop_evt) ::SetEvent(m_stop_evt);
	return NO_ERROR;
}

/////////////////////////////////////////////////////////////////////////////

CWsa_Trace_Receiver:: CWsa_Trace_Receiver(IUdpEvent_Async& _snk, const DWORD _freq_in_ms)
	: TBase(_freq_in_ms), m_evt_sink(_snk), m_bcast(true), m_rcv_b(0) {}
CWsa_Trace_Receiver::~CWsa_Trace_Receiver(void) {}

/////////////////////////////////////////////////////////////////////////////

HRESULT  CWsa_Trace_Receiver::Receive(void) {
	
	HRESULT hr_ = S_OK;

	if (TBase::m_cache.IsValid() == false)
		return (hr_ = __DwordToHresult(ERROR_INVALID_DATA));

	if (TBase::m_socket.IsValid() == false)
		return (hr_ = OLE_E_BLANK);

	INT n_sz  = sizeof(SOCKADDR_IN);
	TBase::m_cache.Clear();

	if (TBase::Timeout().Get(CTimeout::e_default))
    hr_ = TBase::Timeout().Apply(TBase::m_socket, CTimeout::e_default);

	if (hr_ == S_OK)
		m_rcv_b = ::recvfrom(
			TBase::m_socket.Ref(), TBase::m_cache.Data(), TBase::m_cache.Size(), 0, TBase::m_trs_a.Ptr(), &n_sz
		);
	else
		m_rcv_b = 0;
#if (0)
	if (SOCKET_ERROR == n_res) {
		const INT n_err = ::WSAGetLastError(); 
		m_error = n_err;
	}
#else
	if (m_rcv_b < 0) {
		m_rcv_b = 0;
		const INT n_err = ::WSAGetLastError(); n_err;
	}
#endif
	return hr_;
}

HRESULT  CWsa_Trace_Receiver::Turn   (const bool _b_on ) {

	HRESULT hr_ = S_OK;

	if (_b_on) {
#if (0)
		if (TBase::m_srv_a.Is() == false) {
			return (hr_ = __DwordToHresult(ERROR_INVALID_ADDRESS));
		}
#endif
		hr_ = m_socket.Create(CWsa_Socket::e_udp, m_bcast);
		if (m_socket.IsValid() == false)
			return hr_;

		INT n_res  = ::bind(m_socket, (SOCKADDR*)&m_srv_a.Ref(), sizeof(SOCKADDR_IN));
		if (n_res == SOCKET_ERROR) {
			hr_    = __DwordToHresult(::WSAGetLastError());
			return hr_;
		}
		else
			hr_ = CWsa_Base::Turn(_b_on);
	}
	else if ( CWsa_Base::IsOn()) {
		hr_ = CWsa_Base::Turn(_b_on);
	}
	return hr_;
}

/////////////////////////////////////////////////////////////////////////////

DWORD    CWsa_Trace_Receiver::Thread_Fun(void) {
	HRESULT hr_ = S_OK;

	m_evt_sink.IUdpEvt_OnStart();

	while (m_break_it_on == false) {

		m_delay.Wait();
		if (m_delay.Elapsed()) m_delay.Reset();
		else
			continue;

		hr_ = this->Receive();
		
		if (FAILED(hr_)) {
			break;
		}
		else if (m_rcv_b)
			m_evt_sink.IUdpEvt_OnReceive(m_cache);
	}

	if (SUCCEEDED(hr_)) m_evt_sink.IUdpEvt_OnComplete();
	else                m_evt_sink.IUdpEvt_OnError(hr_);

	if (m_stop_evt) ::SetEvent(m_stop_evt);

	return NO_ERROR;
}

/////////////////////////////////////////////////////////////////////////////

shared::drv::um::CWsa_Trace& Trace_ex(void) { return Trace(); }

/////////////////////////////////////////////////////////////////////////////