/*
	Created by Tech_dog (ebontrop@gmail.com) on 11-Dec-2019 at 8:18:11p, UTC+7, Novosibirsk, Tulenina, Wednesday;
	This is Fake GPS driver TCP/IP data cache interface implementation file.
*/
#include "StdAfx.h"
#include "fake.gps.tcp.cache.h"

using namespace shared::drv::um;
/////////////////////////////////////////////////////////////////////////////

CWsa_Cache:: CWsa_Cache(void) : m_p_data(NULL), m_u_size(0), m_d_type(_tp::e_char), m_b_preserve_sz(false) { }
CWsa_Cache:: CWsa_Cache(const CWsa_Cache& _cache) : CWsa_Cache() { *this = _cache; }
CWsa_Cache::~CWsa_Cache(void) {
	if (this->IsValid())
		this->Destroy();
}

/////////////////////////////////////////////////////////////////////////////
HRESULT   CWsa_Cache::Clear   (void) {
	if (this->IsValid() == false)
		return OLE_E_BLANK;

	::memset(m_p_data, 0, this->Size());
	return S_OK;
} 

HRESULT   CWsa_Cache::Create  (const ULONG _u_sz, const _tp _type) {
	if (_u_sz < 1)
		return (E_INVALIDARG);

	HRESULT hr_ = S_OK;

	if (this->IsValid())
		this->Destroy();  // no error provided in this version;

	try {
		m_p_data = new CHAR[_u_sz];
		::memset(m_p_data, 0, sizeof(CHAR) * _u_sz);
		m_u_size = _u_sz;
		m_d_type = _type;
	}
	catch(::std::bad_alloc&){
		hr_ = E_OUTOFMEMORY;
	}

	return hr_;
}

PCHAR const
          CWsa_Cache::Data    (void) const { return m_p_data; }

HRESULT   CWsa_Cache::Destroy (void) {
	
	if (this->IsValid() == false)
		return (__DwordToHresult(ERROR_INVALID_STATE));

	HRESULT hr_ = S_OK;

	try {
		delete m_p_data; m_p_data = NULL; m_u_size = 0; hr_ = OLE_E_BLANK; m_d_type = _tp::e_char;
	}
	catch(...){
		hr_ = __DwordToHresult(ERROR_INVALID_ADDRESS);
	}

	return  hr_;
}

bool      CWsa_Cache::IsValid (void) const { return (NULL != m_p_data && 0 < m_u_size); }
bool      CWsa_Cache::Preserve(void) const { return m_b_preserve_sz; }
void      CWsa_Cache::Preserve(const bool _val) { m_b_preserve_sz = _val; }
ULONG     CWsa_Cache::Size    (void) const { return (m_u_size * sizeof(CHAR)); }

HRESULT   CWsa_Cache::ToCache (const PBYTE _p_buffer, const DWORD _dw_sz) {

	if (NULL == _p_buffer || 0 == _dw_sz)
		return (E_INVALIDARG);

	if (this->Preserve() && this->Size() < _dw_sz)
		return (E_NOT_SUFFICIENT_BUFFER);

	HRESULT hr_ = S_OK;
	
	if (this->Preserve())
		hr_ = this->Clear();
	else
		hr_ = this->Create(_dw_sz, _tp::e_char);
	if (FAILED(hr_))
		return hr_;

	const errno_t err_ = ::memcpy_s(m_p_data, this->Size(), (void*)_p_buffer, _dw_sz);
	if (err_)
		 hr_ = __DwordToHresult(ERROR_INVALID_DATA);

	return hr_;
}

HRESULT   CWsa_Cache::ToCache (LPCSTR _lp_sz_dt) {

	if (NULL == _lp_sz_dt)
		return (E_POINTER);

	const ULONG l_sz_ = static_cast<ULONG>(::strlen(_lp_sz_dt)) * sizeof(CHAR);
	if (0 == l_sz_ && this->Preserve() == false)
		return (E_INVALIDARG);

	const ULONG l_req = l_sz_ + sizeof(CHAR); // adds a room for zero termination;

	if (this->Preserve() && this->Size() < l_req)
		return (E_NOT_SUFFICIENT_BUFFER);

	HRESULT hr_ = S_OK;
	if (this->Preserve())
		hr_ = this->Clear();
	else
		hr_ = this->Create(l_req, _tp::e_char);

	if (SUCCEEDED(hr_) && l_sz_) { // nothing to copy if string length is zero;
		const errno_t err_ = ::memcpy_s(m_p_data, this->Size(), (void*)_lp_sz_dt, l_sz_);
		if (err_)
			hr_ = __DwordToHresult(ERROR_INVALID_DATA);
	}
	return hr_;
}

HRESULT   CWsa_Cache::ToCache (LPCTSTR _lp_sz_dt) {

	if (NULL == _lp_sz_dt)
		return (E_POINTER);

	const ULONG l_sz_ = static_cast<ULONG>(::_tcslen(_lp_sz_dt)) * sizeof(TCHAR);
	if (0 == l_sz_ && this->Preserve() == false)
		return (E_INVALIDARG);

	const ULONG l_req = l_sz_ + sizeof(TCHAR); // adds a room for zero termination;

	if (this->Preserve() && this->Size() < l_req)
		return (E_NOT_SUFFICIENT_BUFFER);

	HRESULT hr_ = S_OK;
	if (this->Preserve())
		hr_ = this->Clear();
	else
		hr_ = this->Create(l_req, _tp::e_wide);

	if (SUCCEEDED(hr_) && l_sz_) { // nothing to copy if string length is zero;
		const errno_t err_ = ::memcpy_s(m_p_data, this->Size(), (void*)_lp_sz_dt, l_sz_);
		if (err_)
			hr_ = __DwordToHresult(ERROR_INVALID_DATA);
	}
	return hr_;
}
CWsa_Cache::_tp
CWsa_Cache::Type(void) const { return m_d_type; }

/////////////////////////////////////////////////////////////////////////////

CWsa_Cache::operator PCHAR const  (void) const { return this->Data(); }
CWsa_Cache::operator const ULONG  (void) const { return this->Size(); }
CWsa_Cache::operator const bool   (void) const { return this->IsValid(); }

/////////////////////////////////////////////////////////////////////////////

CWsa_Cache& CWsa_Cache::operator =(LPCSTR  _lp_sz_dat) { this->ToCache(_lp_sz_dat); return *this; }
CWsa_Cache& CWsa_Cache::operator =(LPCTSTR _lp_sz_dat) { this->ToCache(_lp_sz_dat); return *this; }

/////////////////////////////////////////////////////////////////////////////

CWsa_Cache& CWsa_Cache::operator =(const CWsa_Cache& _cache) {

	if (_cache.IsValid() == true) {
		this->Preserve(false);
		this->ToCache((PBYTE)_cache.Data(), _cache.Size());
		this->Preserve(_cache.Preserve()) ;
		this->m_d_type = _cache.Type();
	}

	return *this;
}