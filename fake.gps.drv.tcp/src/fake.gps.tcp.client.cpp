/*
	Created by Tech_dog (ebontrop@gmail.com) on 12-Dec-2019 at 9:09:51p, UTC+7, Novosibirsk, Tulenina, Thursday;
	This is Fake GPS driver TCP/IP generic client interface implementation file.
*/
#include "StdAfx.h"
#include "fake.gps.tcp.client.h"

using namespace shared::drv::um;
/////////////////////////////////////////////////////////////////////////////

namespace shared { namespace drv { namespace um { namespace _impl {

}}}}
using namespace shared::drv::um::_impl;
/////////////////////////////////////////////////////////////////////////////

CWsa_Client:: CWsa_Client(const DWORD _freq_in_ms) : TBase(_freq_in_ms) {}
CWsa_Client::~CWsa_Client(void) {}

/////////////////////////////////////////////////////////////////////////////
const
CWsa_Address& CWsa_Client::Server(void) const { return m_srv_a; }
CWsa_Address& CWsa_Client::Server(void)       { return m_srv_a; }

HRESULT  CWsa_Client::Turn (const bool _b_on)  {

	HRESULT hr_ = S_OK; // TODO: actually current mode must be checked before doing anything;

	if (_b_on) {

		hr_ = m_socket.Create(CWsa_Socket::e_udp, false);
		if (m_socket.IsValid() == false)
			return hr_;

		if (TBase::Timeout().Get(CTimeout::e_receive)) TBase::Timeout().Apply(TBase::m_socket, CTimeout::e_receive);
		if (TBase::Timeout().Get(CTimeout::e_sending)) TBase::Timeout().Apply(TBase::m_socket, CTimeout::e_sending);

		INT n_res  = ::bind(m_socket, TBase::m_trs_a, TBase::m_trs_a);
		if (n_res == SOCKET_ERROR) {
			hr_    = __DwordToHresult(::WSAGetLastError());
			return hr_;
		}
	}

	return TBase::Turn(_b_on);
}

/////////////////////////////////////////////////////////////////////////////

DWORD    CWsa_Client::Thread_Fun(void) {

	if (m_socket.IsValid() == false) return ERROR_INVALID_STATE;
	if (m_cache .IsValid() == false) return ERROR_INVALID_DATA;
	if (m_srv_a .Is()      == false) return ERROR_INVALID_ADDRESS; // this is not true error code for this case;
	                                                               // but it allows to distinguish  error reason;
	while (m_break_it_on == false) {

		m_delay.Wait();
		if (m_delay.Elapsed()) m_delay.Reset();
		else
			continue;

		INT n_result = 0;
		INT n_ad_len = m_srv_a;             // gets a length of the destination/server address structure;

		n_result = ::sendto(m_socket, m_cache, m_cache, 0, m_srv_a, n_ad_len);
		if (SOCKET_ERROR == n_result) {
			const DWORD wsa_err = static_cast<DWORD>(::WSAGetLastError());
			return wsa_err;
		}
		else if (false)
			continue;
		                                    // https://docs.microsoft.com/en-us/windows/win32/api/winsock/nf-winsock-recvfrom
		n_result = ::recvfrom(m_socket, m_cache, m_cache, 0, m_srv_a, &n_ad_len);
		if (SOCKET_ERROR == n_result) {     // https://docs.microsoft.com/en-us/windows/win32/winsock/windows-sockets-error-codes-2
			const DWORD wsa_err = static_cast<DWORD>(::WSAGetLastError());
			return wsa_err;
		}
		else if (static_cast<ULONG>(n_result) < m_cache.Size())
			continue;                       // a server may interrupt a connection; it doesn't matter;
		else if (0 == n_result)             // no data received;
			continue;

		if (m_break_it_on)
			break;

	}
	return NO_ERROR;
}