/*
	Created by Tech_dog (ebontrop@gmail.com) on 7-Dec-2019 at 5:22:18p, UTC+7, Novosibirsk, Tulenina, Saturday;
	This is Fake GPS driver TCP/IP data reader interface implementation file.
*/
#include "StdAfx.h"
#include "fake.gps.tcp.reader.h"

using namespace shared::drv::um;

/////////////////////////////////////////////////////////////////////////////

namespace shared { namespace drv { namespace um { namespace _impl {

	DWORD WINAPI _thread_fun(CTcpIpReader* _p_reader){
		if (NULL == _p_reader)
			return ERROR_INVALID_ADDRESS;
		else
			return _p_reader->Thread_Fun();
	}

}}}}
using namespace shared::drv::um::_impl;
/////////////////////////////////////////////////////////////////////////////

CTcpIpReader:: CTcpIpReader(void) : m_socket(NULL), m_thread(NULL), m_break_it_on(false), m_socket_acc(NULL) {}
CTcpIpReader::~CTcpIpReader(void) {}

/////////////////////////////////////////////////////////////////////////////

bool     CTcpIpReader::IsOn(void) const       { return (NULL != m_thread); }
SHORT    CTcpIpReader::Port(void) const       { return m_port; }
HRESULT  CTcpIpReader::Port(const SHORT _p)   {
	if (0 == _p)
		return E_INVALIDARG;
	else {
		m_port = _p;
		return S_OK;
	}
}
HRESULT  CTcpIpReader::Turn(const bool _b_on) {
	if (_b_on && this->IsOn())
		return (HRESULT_FROM_WIN32(ERROR_INVALID_STATE));

	if (_b_on == false && this->IsOn() == false)
		return (HRESULT_FROM_WIN32(ERROR_INVALID_STATE));

	HRESULT hr_ = S_OK;

	if (_b_on) {
		DWORD dw_id_ = 0;
		m_thread = ::CreateThread(NULL, 0, (LPTHREAD_START_ROUTINE)_thread_fun, this, 0, &dw_id_);
		if (NULL == m_thread)
			hr_  = HRESULT_FROM_WIN32(::GetLastError());
	}
	else {
		m_break_it_on = true;
		::WaitForSingleObject(m_thread, INFINITE);
		::CloseHandle(m_thread);
		m_thread = NULL;
	}

	return hr_;
}

/////////////////////////////////////////////////////////////////////////////

DWORD    CTcpIpReader::Thread_Fun(void) {
	while (m_break_it_on == false) {

		fd_set readfds;
		FD_ZERO(&readfds);
		timeval tv = {0} ;
		tv.tv_sec  =  5;
		// (0) creates temporary socket for assigning it to data accept socket;
		while (m_break_it_on == false) {
			FD_SET(m_socket, &readfds);
			const INT n_ret = ::select(0, &readfds, NULL, NULL, &tv);
			if ( 0 <  n_ret) {
				break; // there is one or more sockets ready for work;
			}
			else if (0 > n_ret) {
				const DWORD dw_sock_err = ::WSAGetLastError();
				if (WSAENOTSOCK == dw_sock_err) {
					m_socket = ::WSASocket(AF_INET, SOCK_STREAM, IPPROTO_TCP, NULL, 0, 0);
					sockaddr_in service = {0};
					service.sin_family  = AF_INET;

					CAtlStringA cs_ip("127.0.0.1");

					service.sin_addr.s_addr = ::inet_addr(cs_ip.GetString());
					service.sin_port = ::htons(m_port);

					const INT n_bind = ::bind(m_socket, (SOCKADDR*) &service, sizeof (service));
					if ( 0 != n_bind ) {
					//	return ::WSAGetLastError();
					}
					const INT n_lstn = ::listen(m_socket, 1);
					if ( 0 != n_lstn ) {
					//	return ::WSAGetLastError();
					}
					FD_ZERO(&readfds);
					FD_SET (m_socket , &readfds);
				}
			}
		}
		// (1) sets command data exchange socket;
		m_socket_acc = ::accept(m_socket , NULL, NULL);
		::closesocket(m_socket);m_socket = NULL;

		if (INVALID_SOCKET == m_socket_acc)
			return NO_ERROR;

	}
	return NO_ERROR;
}