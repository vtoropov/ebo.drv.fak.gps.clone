#ifndef _FAKEGPSTCPBASE_H_045BDCC3_5585_4F68_A3AC_F9927979FFCD_INCLUDEDED
#define _FAKEGPSTCPBASE_H_045BDCC3_5585_4F68_A3AC_F9927979FFCD_INCLUDEDED
/*
	Created by Tech_dog (ebontrop@gmail.com) on 12-Dec-2019 at 7:52:18p, UTC+7, Novosibirsk, Tulenina, Thursday;
	This is Fake GPS driver TCP/IP generic server/client/trace base interface declaration file.
*/
#include "fake.gps.tcp.event.h"
#include "fake.gps.tcp.address.h"
#include "fake.gps.tcp.socket.h"
#include "fake.gps.tcp.cache.h"

namespace shared { namespace drv { namespace um {

	class CWsa_Base {
	protected:
		volatile bool m_break_it_on;
		HANDLE        m_thread;
		HANDLE        m_stop_evt;
		CWsa_Address  m_trs_a ;
		CWsa_Socket   m_socket;
		CWsa_Cache    m_cache ;
		CDelayEvent   m_delay ;
		CTimeout      m_timout;

	protected:
		CWsa_Base(const DWORD _freq_in_ms);
	   ~CWsa_Base(void);

	public:
		virtual DWORD Thread_Fun(void) PURE;

	public:
		TAddress_r    Address(void) const;
		TAddress_w    Address(void)      ;
		const
		CWsa_Cache&   Cache  (void) const;
		CWsa_Cache&   Cache  (void)      ;
		const
		CDelayEvent&  Delay  (void) const;
		CDelayEvent&  Delay  (void)      ;
		bool          IsOn   (void) const;
		TTimeout_r    Timeout(void) const;
		TTimeout_w    Timeout(void)      ;
		HRESULT       Turn   (const bool _b_on );

	};

}}}

#endif/*_FAKEGPSTCPBASE_H_045BDCC3_5585_4F68_A3AC_F9927979FFCD_INCLUDEDED*/