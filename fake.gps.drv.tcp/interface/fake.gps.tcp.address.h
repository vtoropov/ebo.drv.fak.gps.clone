#ifndef _FAKEGPSTCPADDRESS_H_045BDCC3_5585_4F68_A3AC_F9927979FFCD_INCLUDEDED
#define _FAKEGPSTCPADDRESS_H_045BDCC3_5585_4F68_A3AC_F9927979FFCD_INCLUDEDED
/*
	Created by Tech_dog (ebontrop@gmail.com) on 11-Dec-2019 at 9:01:29p, UTC+7, Novosibirsk, Tulenina, Wednesday;
	This is Fake GPS driver TCP/IP generic address interface declaration file.
*/

namespace shared { namespace drv { namespace um {

	class CWsa_Address {
	protected:
		SOCKADDR_IN   m_data;

	public:
		 CWsa_Address(void);
		~CWsa_Address(void);

	public:
		HRESULT       Ip  (LPCSTR _lp_sz_ip);
		bool          Is  (void) const      ;
		HRESULT       Local(const bool _b_prev = false); // gets local host IP; if _b_prev is false, GetAddrInfo() is used;
		HRESULT       Port(const USHORT)    ;
		PSOCKADDR     Ptr (void);
		SOCKADDR_IN&  Ref (void);

	public:
		CWsa_Address& operator << (LPCSTR);
		CWsa_Address& operator << (const USHORT);

	public:
		operator const bool (void) const;
		operator PSOCKADDR  (void)      ;
		operator const INT  (void) const;    // returns size of address structure;
	};

	typedef const CWsa_Address& TAddress_r;  // read only reference;
	typedef       CWsa_Address& TAddress_w;  // read-write reference;

	class CWsa_Address_bcast_rcv : public CWsa_Address {
	                              typedef CWsa_Address TBase;
	public:
		 CWsa_Address_bcast_rcv(const USHORT _u_port);
		~CWsa_Address_bcast_rcv(void);
	};

	class CWsa_Address_bcast_snd : public CWsa_Address {
	                              typedef CWsa_Address TBase;
	public:
		 CWsa_Address_bcast_snd(const USHORT _u_port);
		~CWsa_Address_bcast_snd(void);
	};

}}}

#endif/*_FAKEGPSTCPADDRESS_H_045BDCC3_5585_4F68_A3AC_F9927979FFCD_INCLUDEDED*/