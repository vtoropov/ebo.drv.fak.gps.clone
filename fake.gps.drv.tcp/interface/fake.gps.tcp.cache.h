#ifndef _FAKEGPSTCPCACHE_H_045BDCC3_5585_4F68_A3AC_F9927979FFCD_INCLUDEDED
#define _FAKEGPSTCPCACHE_H_045BDCC3_5585_4F68_A3AC_F9927979FFCD_INCLUDEDED
/*
	Created by Tech_dog (ebontrop@gmail.com) on 11-Dec-2019 at 8:13:54p, UTC+7, Novosibirsk, Tulenina, Wednesday;
	This is Fake GPS driver TCP/IP data cache interface declaration file.
*/

namespace shared { namespace drv { namespace um {

	class CWsa_Cache {
	public:
		enum _tp {
			e_char = 0x0, // default;
			e_wide = 0x1, // interpreted as wide char buffer; but buffer size will be returned in bytes; always;
		};
	private:
		PCHAR     m_p_data;
		ULONG     m_u_size;
		_tp       m_d_type;
		bool      m_b_preserve_sz;  // preserve initial size that was put via 'create' function; default is false;

	public:
		 CWsa_Cache(void);
		 CWsa_Cache(const CWsa_Cache&);
		~CWsa_Cache(void);

	public:
		HRESULT     Clear     (void) ;
		HRESULT     Create    (const ULONG _u_sz, const _tp _type);   // creates a buffer of size (in chars) provided;
		PCHAR const Data      (void) const;
		HRESULT     Destroy   (void) ;
		bool        IsValid   (void) const;
		bool        Preserve  (void) const;
		void        Preserve  (const bool);
		ULONG       Size      (void) const;                           // returns size in chars (bytes);
		HRESULT     ToCache   (const PBYTE _p_buffer, const DWORD _dw_sz);
		HRESULT     ToCache   (LPCSTR  _lp_sz_buffer);
		HRESULT     ToCache   (LPCTSTR _lp_sz_buffer);
		_tp         Type      (void) const;

	public:
		operator PCHAR const  (void) const;
		operator const ULONG  (void) const;
		operator const bool   (void) const;
	public:
		CWsa_Cache& operator =(LPCSTR  );
		CWsa_Cache& operator =(LPCTSTR );
	public:
		CWsa_Cache& operator =(const CWsa_Cache&);
	};

}}}

#endif/*_FAKEGPSTCPCACHE_H_045BDCC3_5585_4F68_A3AC_F9927979FFCD_INCLUDEDED*/