#ifndef _FAKEGPSTCPCFG_H_045BDCC3_5585_4F68_A3AC_F9927979FFCD_INCLUDEDED
#define _FAKEGPSTCPCFG_H_045BDCC3_5585_4F68_A3AC_F9927979FFCD_INCLUDEDED
/*
	Created by Tech_dog (ebontrop@gmail.com) on 14-Dec-2019 at 1:16:36a, UTC+7, Novosibirsk, Tulenina, Saturday;
	This is Fake GPS driver TCP/IP service connect configuration interface declaration file.
*/
#include "fake.gps.tcp.address.h"

namespace shared { namespace drv { namespace um {

	class CWsa_Connect {
	private:
		CWsa_Address   m_svc_a;  // service/sender  TCP/IP address;
		CWsa_Address   m_rcv_a;  // client/receiver TCP/IP address;

	public:
		 CWsa_Connect (void);
		~CWsa_Connect (void);

	public:
		TAddress_r     Receiver(void) const;
		TAddress_w     Receiver(void)      ;
		TAddress_r     Sender  (void) const;
		TAddress_w     Sender  (void)      ;

	public:
		CWsa_Connect& operator<<(TAddress_r);  // sets receiver address;
		CWsa_Connect& operator>>(TAddress_r);  // sets sender   address;
	};

	class CWsa_Cmd_Pair  : public CWsa_Connect {
	                      typedef CWsa_Connect TConnect;
	public:
		CWsa_Cmd_Pair(void);
	};

	class CWsa_Trace_Pair : public CWsa_Connect {
	                       typedef CWsa_Connect TConnect;
	public:
		  CWsa_Trace_Pair(void);
	};

	class CWsa_Cfg {
	private:
		CWsa_Cmd_Pair   m_cmd_pair ;
		CWsa_Trace_Pair m_trace_pair;
		ULONG           m_cache_size;  // predefined cache size; the cache size must be preserved from changing;

	public:
		CWsa_Cfg (void);
	   ~CWsa_Cfg (void);

	public:
		ULONG CacheSize(void)   const;
		const CWsa_Cmd_Pair  &  CmdPair  (void) const;
		const CWsa_Trace_Pair&  TracePair(void) const;
	};

}}}

#endif/*_FAKEGPSTCPCFG_H_045BDCC3_5585_4F68_A3AC_F9927979FFCD_INCLUDEDED*/