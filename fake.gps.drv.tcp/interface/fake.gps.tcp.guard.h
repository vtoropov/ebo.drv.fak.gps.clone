#ifndef _FAKEGPSTCPGUARD_H_045BDCC3_5585_4F68_A3AC_F9927979FFCD_INCLUDEDED
#define _FAKEGPSTCPGUARD_H_045BDCC3_5585_4F68_A3AC_F9927979FFCD_INCLUDEDED
/*
	Created by Tech_dog (ebontrop@gmail.com) on 11-Dec-2019 at 7:36:56p, UTC+7, Novosibirsk, Tulenina, Wednesday;
	This is Fake GPS driver TCP/IP WSA initialize guard interface declaration file.
*/

namespace shared { namespace drv { namespace um {

	class CWsa_Guard {
	public:
		 CWsa_Guard(void);
		~CWsa_Guard(void);

	public:
		HRESULT   Capture(void);
		HRESULT   Release(void);
	public:
		static
		bool      Secured(void);

	private:
		CWsa_Guard(const CWsa_Guard&);
		CWsa_Guard& operator= (const CWsa_Guard&);
	};

}}}

#endif/*_FAKEGPSTCPGUARD_H_045BDCC3_5585_4F68_A3AC_F9927979FFCD_INCLUDEDED*/