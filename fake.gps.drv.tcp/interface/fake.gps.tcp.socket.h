#ifndef _FAKEGPSTCPSOCKET_H_045BDCC3_5585_4F68_A3AC_F9927979FFCD_INCLUDEDED
#define _FAKEGPSTCPSOCKET_H_045BDCC3_5585_4F68_A3AC_F9927979FFCD_INCLUDEDED
/*
	Created by Tech_dog (ebontrop@gmail.com) on 11-Dec-2019 at 8:30:03p, UTC+7, Novosibirsk, Tulenina, Wednesday;
	This is Fake GPS driver TCP/IP generic socket interface declaration file.
*/

namespace shared { namespace drv { namespace um {

	class CWsa_Socket {
	public:
		enum _type {
			e_udp = 0x0,
			e_tcp = 0x1,
		};
	protected:
		SOCKET     m_sock;
		_type      m_type;

	public:
	    CWsa_Socket(void);
	   ~CWsa_Socket(void);
	public:
		virtual HRESULT Create (const _type, const bool b_udp = false);
	public:
		HRESULT    Destroy(void);
		bool       IsValid(void) const;
		SOCKET&    Ref    (void);
		_type      Type   (void) const;

	public:
		operator const SOCKET& (void) const;
		operator       SOCKET& (void)      ;
	};

}}}

#endif/*_FAKEGPSTCPSOCKET_H_045BDCC3_5585_4F68_A3AC_F9927979FFCD_INCLUDEDED*/