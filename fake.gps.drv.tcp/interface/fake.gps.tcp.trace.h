#ifndef _FAKEGPSTCPTRACE_H_045BDCC3_5585_4F68_A3AC_F9927979FFCD_INCLUDEDED
#define _FAKEGPSTCPTRACE_H_045BDCC3_5585_4F68_A3AC_F9927979FFCD_INCLUDEDED
/*
	Created by Tech_dog (ebontrop@gmail.com) on 11-Dec-2019 at 7:45:06p, UTC+7, Novosibirsk, Tulenina, Wednesday;
	This is Fake GPS driver TCP/IP generic trace interface declaration file.
*/
#include "fake.gps.tcp.base.h"
#include "fake.gps.tcp.client.h"
#include "fake.gps.pps.defs.h"

#pragma comment(lib, "ws2_32.lib")

namespace shared { namespace drv { namespace um {

	typedef CRITICAL_SECTION TAccessGuard;

	class CWsa_Trace : public IPipeTrace {
	private:
		mutable
		TAccessGuard m_guard;
		CWsa_Cache   m_default;

	public:
		 CWsa_Trace (void);
		~CWsa_Trace (void);

	public: // IPipeTrace;
#pragma warning(disable: 4481)
		virtual HRESULT     IPipeTrace_PutError(LPCSTR _lp_sz_desc, const HRESULT _h_result) override sealed;
		virtual HRESULT     IPipeTrace_PutInfo (LPCSTR _lp_sz_desc)                          override sealed;
		virtual HRESULT     IPipeTrace_PutInfo (LPCSTR _lp_sz_desc, const DWORD   _d_data  ) override sealed;
		virtual HRESULT     IPipeTrace_PutInfo (LPCSTR _lp_sz_desc, LPCSTR    _lp_sz_data  ) override sealed;
		virtual HRESULT     IPipeTrace_PutWarn (LPCSTR _lp_sz_desc, const HRESULT _h_result) override sealed;
#pragma warning(default: 4481)
	public:
		CWsa_Cache  GetLast (void) const;
		const bool  IsEmpty (void) const;
		const void  PutLine (LPCSTR _lp_sz_fmt, ...);

	public:
		CWsa_Trace& operator << (LPCSTR );
		CWsa_Trace& operator << (LPCTSTR);
		CWsa_Trace& operator  = (LPCSTR ); // sets default message;
		CWsa_Trace& operator  = (LPCTSTR); // sets default message;

	public:
		operator CWsa_Cache& (void);
	};

	CWsa_Trace&  Trace(void);

	class CWsa_Trace_Sender : public CWsa_Base {
	                         typedef CWsa_Base TBase;
	protected:
		volatile bool m_bcast ; // UDP/broadcast is true by default;

	public:
		 CWsa_Trace_Sender (const DWORD _freq_in_ms = 2000);
		~CWsa_Trace_Sender (void);

	public:
#pragma warning(disable: 4481)
		DWORD       Thread_Fun(void) override sealed; // virtual methods must be going first for better performance;
#pragma warning(default: 4481)
	public:
		HRESULT     Turn (const bool _b_on );
		HRESULT     Send (void);                                 // sends cached data;
		HRESULT     Send (const CWsa_Cache&);                    // sends data cache provided;
		HRESULT     Send (PCHAR const _data, const DWORD _size); // sends raw data provided;

	public:
		CWsa_Trace_Sender& operator << (LPCSTR );
		CWsa_Trace_Sender& operator << (LPCTSTR);
	};

	interface IUdpEvent_Async {
		virtual HRESULT IUdpEvt_OnComplete(void) PURE;
		virtual HRESULT IUdpEvt_OnError   (const HRESULT) PURE;
		virtual HRESULT IUdpEvt_OnReceive (const CWsa_Cache&)  { HRESULT hr_ = E_NOTIMPL; return hr_; }
		virtual HRESULT IUdpEvt_OnSend    (const CWsa_Cache&)  { HRESULT hr_ = E_NOTIMPL; return hr_; }
		virtual HRESULT IUdpEvt_OnStart   (void) PURE;
	};

	class CWsa_Trace_Receiver : public CWsa_Client {
	                           typedef CWsa_Client TBase;
	protected:
		IUdpEvent_Async& m_evt_sink;
		volatile bool    m_bcast ; // UDP/broadcast is true by default;
		volatile INT     m_rcv_b ; // recently received byte count;

	public:
		 CWsa_Trace_Receiver (IUdpEvent_Async&, const DWORD _freq_in_ms = 2000);
		~CWsa_Trace_Receiver (void);

	public:
#pragma warning(disable: 4481)
		DWORD       Thread_Fun(void) override sealed; // virtual methods must be going first for better performance;
#pragma warning(default: 4481)
	public:
		HRESULT     Receive(void);                    // receives data to cache;
		HRESULT     Turn   (const bool _b_on );
	};

}}}

shared::drv::um::CWsa_Trace& Trace_ex(void);

#endif/*_FAKEGPSTCPTRACE_H_045BDCC3_5585_4F68_A3AC_F9927979FFCD_INCLUDEDED*/