#ifndef _FAKEGPSTCPEVENT_H_045BDCC3_5585_4F68_A3AC_F9927979FFCD_INCLUDEDED
#define _FAKEGPSTCPEVENT_H_045BDCC3_5585_4F68_A3AC_F9927979FFCD_INCLUDEDED
/*
	Created by Tech_dog (ebontrop@gmail.com) on 11-Dec-2019 at 7:59:09p, UTC+7, Novosibirsk, Tulenina, Wednesday;
	This is Fake GPS driver TCP/IP waitable event interface declaration file.
*/

namespace shared { namespace drv { namespace um {

	class CDelayEvent {
	public:
		enum _frm : DWORD {
			e_na      = 0,
			eInfinite = (DWORD)-1
		};
	protected:
		volatile DWORD m_nTimeSlice;    // in milliseconds;
		volatile DWORD m_nTimeFrame;    // total time to wait for;
	protected:
		DWORD    m_nCurrent;            // current time;
	public:
		CDelayEvent(const DWORD nTimeSlice, const DWORD nTimeFrame);
		virtual ~CDelayEvent(void);
	public:
		virtual bool Elapsed (void) const;
		virtual bool IsReset (void) const;
		virtual VOID Reset   (const DWORD nTimeSlice = _frm::e_na, const DWORD nTimeFrame = _frm::e_na);
		virtual VOID Wait    (void);

	public:
		CDelayEvent& operator= (const DWORD) ; // sets time frame value for waiting for;
	};

	class CTimeout {
	public:
		enum _oper {
			e_default = 0x0,  // default operation means to be applied to both: receiving & sending data;
			e_receive = 0x1,  // receive operation; overides the default one;
			e_sending = 0x2,  // send    operation; overides the default one;
		};
	private:
		LONG  m_outs[_oper::e_sending + 1];
	public:
		 CTimeout(void);
		~CTimeout(void);

	public:
		HRESULT  Apply(SOCKET, const CTimeout::_oper);
		LONG     Get  (const CTimeout::_oper) const  ;
		VOID     Set  (const CTimeout::_oper, const LONG _value);
	};

	typedef const CTimeout& TTimeout_r;
	typedef       CTimeout& TTimeout_w;
}}}

#endif/*_FAKEGPSTCPEVENT_H_045BDCC3_5585_4F68_A3AC_F9927979FFCD_INCLUDEDED*/