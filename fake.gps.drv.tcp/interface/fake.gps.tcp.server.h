#ifndef _FAKEGPSTCPSERVER_H_045BDCC3_5585_4F68_A3AC_F9927979FFCD_INCLUDEDED
#define _FAKEGPSTCPSERVER_H_045BDCC3_5585_4F68_A3AC_F9927979FFCD_INCLUDEDED
/*
	Created by Tech_dog (ebontrop@gmail.com) on 12-Dec-2019 at 1:07:53p, UTC+7, Novosibirsk, Tulenina, Thursday;
	This is Fake GPS driver TCP/IP generic server interface declaration file.
*/
#include "fake.gps.tcp.base.h"
#include "fake.gps.cmd.defs.h"

namespace shared { namespace drv { namespace um {

	class CWsa_Server : public CWsa_Base {
	                   typedef CWsa_Base TBase;
	public:
		CWsa_Server(const DWORD _freq_in_ms = 2000);
	   ~CWsa_Server(void);

	public:
#pragma warning(disable: 4481)
		DWORD       Thread_Fun(void) override sealed; // virtual methods must be going first for better performance;
#pragma warning(default: 4481)
	public:
		HRESULT     Turn (const bool _b_on ); // base class method override;

	};

}}}

#endif/*_FAKEGPSTCPSERVER_H_045BDCC3_5585_4F68_A3AC_F9927979FFCD_INCLUDEDED*/