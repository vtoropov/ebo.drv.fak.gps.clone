#ifndef _FAKEGPSTCPREADER_H_045BDCC3_5585_4F68_A3AC_F9927979FFCD_INCLUDEDED
#define _FAKEGPSTCPREADER_H_045BDCC3_5585_4F68_A3AC_F9927979FFCD_INCLUDEDED
/*
	Created by Tech_dog (ebontrop@gmail.com) on 7-Dec-2019 at 5:10:02p, UTC+7, Novosibirsk, Tulenina, Saturday;
	This is Fake GPS driver TCP/IP data reader interface declaration file.
*/

namespace shared { namespace drv { namespace um {

	class CTcpIpReader {
	protected:
		mutable
		bool       m_break_it_on;
		SOCKET     m_socket;
		SOCKET     m_socket_acc;
		HANDLE     m_thread;
		SHORT      m_port  ;

	public:
		 CTcpIpReader (void) ;
		~CTcpIpReader (void) ;

	public:
		bool       IsOn(void) const;
		SHORT      Port(void) const;
		HRESULT    Port(const SHORT);
		HRESULT    Turn(const bool _b_on);

	public:
		DWORD      Thread_Fun(void);
	};

}}}

#endif/*_FAKEGPSTCPREADER_H_045BDCC3_5585_4F68_A3AC_F9927979FFCD_INCLUDEDED*/