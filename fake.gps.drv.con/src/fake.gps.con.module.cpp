/*
	Created by Tech_dog (ebontrop@gmail.com) on 5-Apr-2019 on 10:23:21p, UTC+7, Phuket, Rawai, Thursday;
	This is sound-bin-trans receiver desktop console application entry point file.
	-----------------------------------------------------------------------------
	Adopted to FakeGPS project on 13-Dec-2019 at 12:11:50p, UTC+7, Novosibirsk, Tulenina, Friday;
*/
#include "StdAfx.h"
#include "shared.gen.sys.com.h"
#include "shared.gen.sys.err.h"

using namespace shared::sys_core;

#include "shared.gen.cmd.arg.h"
#include "shared.gen.con.bas.h"

using namespace shared::common;
using namespace shared::common::ui;

#include "fake.gps.tcp.guard.h"
#include "fake.gps.pps.server.overlap.h"
#include "fake.gps.pps.client.synch.h"

using namespace shared::drv::um;

#include "fake.gps.con.trace.h"
#include "fake.gps.con.tcp.h"
#include "fake.gps.con.pps.h"
#include "fake.gps.con.res.h"
/////////////////////////////////////////////////////////////////////////////

namespace shared { namespace drv { namespace client {

	class CConsole_Rcv : public shared::common::ui::CConsole , public ICommandProcessor {
	                    typedef shared::common::ui::CConsole TBase;
	private:
		CError        m_error;
	public:
		CConsole_Rcv(void) {
			m_error << __MODULE__ << S_OK;
			CAtlString cs_title(_T("FakeGPS Driver Project Console"));
#if defined(WIN64)
			cs_title += _T(" [x64]");
#else
			cs_title += _T(" [x86]");
#endif
			TBase::OnCreate(cs_title.GetString());
			TBase::SetIcon (IDR_FAKE_GSP_CON_ICO);
		}

	private: // ICommandProcessor
		virtual HRESULT   ICmdProc_Get(ICommand&) override {
			return S_OK;
		}
		virtual HRESULT   ICmdProc_Set(ICommand&) override {
			return S_OK;
		}
	};

}}}
using namespace shared::drv::client;
/////////////////////////////////////////////////////////////////////////////

INT _tmain(VOID) {
	INT n_res = 0;

	::SetConsoleOutputCP(CP_UTF8);

	CConsole_Rcv con_;

	CCoIniter com_lib(false);
	if (com_lib == false) {
		con_.WriteErr(com_lib.Error());
		return (n_res = 1);
	}

#pragma region _sec
	CCoSecurityProvider sec_;
	HRESULT hr_ = sec_.InitNoIdentity();
	if (FAILED(hr_)) {
		con_.WriteErr(sec_.Error());
		return (n_res = 1);
	}
#pragma endregion

	CError sys_err; sys_err << __MODULE__ << S_OK;

	CWsa_Guard wsa_guard;
	hr_ = wsa_guard.Capture();
	if (SUCCEEDED(hr_))
		con_ << _T("CWsa_Guard::WSA is captured.");
	else {
		sys_err = hr_; con_.WriteErr(sys_err);
	}

	CCommandLine cmd_line;

#pragma region _wsa_trace
	CClient_Trace trc_rcv(con_); if (cmd_line.Has  (_T("trace"))) {
		hr_ = trc_rcv.Delay(100);
		hr_ = trc_rcv.Init();
	}
#pragma endregion

#pragma region _wsa_receive
	CClient_Tcp cmd_rcv(con_); if (cmd_line.Has  (_T("tcp"))) {
	hr_ = cmd_rcv.Init(); }  else  con_.WriteInfo(_T("Using TCP client is turned off;"));
#pragma endregion

#pragma region _pps_client
	CClient_Pipe pps_cli(con_, trc_rcv); if (cmd_line.Has  (_T("pipe"))) {
	hr_ = pps_cli.Init(); }            else  con_.WriteInfo(_T("Using Pipe client is turned off;"));
#pragma endregion

#pragma region _pps_server_overlap

	CPipeServer_Overlap pps_srv_over(con_, trc_rcv, TPipeMode::e_bin_mode); if (cmd_line.Has  (_T("pipe_srv_over"))) {
	hr_ = pps_srv_over.Turn(true); } else  con_.WriteInfo(_T("Using Pipe overlap server is turned off;"));

#pragma endregion

#pragma region _pps_server_overlap

	CPipeClient_Synch pps_cli_synch(trc_rcv, TPipeMode::e_bin_mode); if (cmd_line.Has  (_T("pipe_cli_sync"))) {
	
	if (cmd_line.Has  (_T("geo_lat"))) {
		pps_cli_synch.Cmd_geo().Latitude(::_tstof(cmd_line.Arg(_T("geo_lat")).GetString()));
	}
	if (cmd_line.Has  (_T("geo_lon"))) {
		pps_cli_synch.Cmd_geo().Longitude(::_tstof(cmd_line.Arg(_T("geo_lon")).GetString()));
	}
	hr_ = pps_cli_synch.Turn(true); } else  con_.WriteInfo(_T("Using Pipe synchronous client is turned off;"));

#pragma endregion

	con_.OnWait(_T("Press any key to stop.\n"));

	pps_srv_over.Turn(false);

	hr_ = pps_cli.Term();
	hr_ = cmd_rcv.Term(); if (cmd_line.Has  (_T("trace"))) {
	hr_ = trc_rcv.Term(); }
	hr_ = wsa_guard.Release();
	if (SUCCEEDED(hr_))
		con_ << _T("CWsa_Guard::WSA is released.");

	con_.OnWait(_T("Worker thread(s) have stopped.\n\t"
		           "Press any key or [x] close button for exiting the console;"));

	return n_res;
}