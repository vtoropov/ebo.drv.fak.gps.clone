/*
	Created by Tech_dog (ebontrop@gmail.com) on 18-Dec-2019 at 11:14:56p, UTC+7, Novosibirsk, Tulenina, Wednesday;
	This is FakeGPS driver desktop app named pipe client wrapper interface implementation file.
*/
#include "StdAfx.h"
#include "fake.gps.con.pps.h"

using namespace shared::drv::um;
using namespace shared::drv::client;

/////////////////////////////////////////////////////////////////////////////

CClient_Pipe:: CClient_Pipe(CConsole& _out, IPipeTrace& _trace) : 
	m_out(_out), m_client(*this, _trace), m_reader(*this, _trace), m_writer(*this, _trace) {
	m_error << __MODULE__ << S_OK;
}
CClient_Pipe::~CClient_Pipe(void) {}

/////////////////////////////////////////////////////////////////////////////

HRESULT   CClient_Pipe::IPipeEvent_OnCreate(LPCTSTR _pipe_uri) {
	m_error << __MODULE__ << S_OK;

	CAtlString cs_msg;
	cs_msg.Format(
		_T("Pipe client created: [uri:=%s]"), _pipe_uri
	);

	HRESULT hr_ = m_out.WriteInfo((LPCTSTR)cs_msg);

	hr_ = m_writer.Put(m_client.Handle(), "This is pipe test message;");
	if (FAILED(hr_)) {
		m_error.State(hr_, _T("CPipeWriter::Put() failed;")); m_out.WriteErr(m_error);
	}
	hr_ = m_reader.Get(m_client.Handle());
	if (FAILED(hr_)) {
		m_error.State(hr_, _T("CPipeReader::Get() failed;")); m_out.WriteErr(m_error);
	}

	return  hr_;
}

HRESULT   CClient_Pipe::IPipeEvent_OnRead  (const LPBYTE _p_data, const DWORD _dw_sz){
	if (NULL == _p_data || 0 == _dw_sz)
		return E_INVALIDARG;
	else
		return S_OK;
}

HRESULT   CClient_Pipe::IPipeEvent_OnWrite (const LPBYTE _p_data, const DWORD _dw_sz){
	if (NULL == _p_data || 0 == _dw_sz)
		return E_INVALIDARG;
	else
		return S_OK;
}

/////////////////////////////////////////////////////////////////////////////

TErrorRef   CClient_Pipe:: Error(void) const { return m_error; }
HRESULT     CClient_Pipe:: Init (void) {
	m_error << __MODULE__ << S_OK;

	m_out.WriteInfo(_T("Connecting to driver pipe..."));

	HRESULT hr_ = m_client.Create(_T("FakeGPS"), 1000);
	if (FAILED(hr_)) {
		m_error.State(hr_, _T("CClientPipe::Create() failed;")); m_out.WriteErr(m_error);
	}

	return m_error;
}
HRESULT     CClient_Pipe:: Term (void) {
	m_error << __MODULE__ << S_OK;

	HRESULT hr_ = m_error;

	hr_ = m_writer.Stop ();
	hr_ = m_reader.Stop ();
	hr_ = m_client.Destroy();

	if (FAILED(hr_))
		m_error = hr_;

	return m_error;
}

/////////////////////////////////////////////////////////////////////////////