#ifndef _FAKEGPSCONTRACE_H_3AF2A70B_5330_41ED_9E07_B39047C65294_INCLUDED
#define _FAKEGPSCONTRACE_H_3AF2A70B_5330_41ED_9E07_B39047C65294_INCLUDED
/*
	Created by Tech_dog (ebontrop@gmail.com) on 18-Dec-2019 at 11:39:22a, UTC+7, Novosibirsk, Tulenina, Wednesday;
	This is FakeGPS driver desktop app TCP/IP and Pipe trace wrapper interface declaration file. 
*/
#include "shared.gen.sys.err.h"
#include "shared.gen.cmd.arg.h"
#include "shared.gen.con.bas.h"

#include "fake.gps.tcp.cache.h"
#include "fake.gps.tcp.trace.h"
#include "fake.gps.pps.defs.h"

namespace shared { namespace drv { namespace client {

	using shared::sys_core::CError;
	using shared::common::ui::CConsole;

	using shared::drv::um::IPipeTrace;
	using shared::drv::um::IUdpEvent_Async;
	using shared::drv::um::CWsa_Cache;
	using shared::drv::um::CWsa_Trace_Receiver;

	class CClient_Trace : public IUdpEvent_Async, public IPipeTrace {
	private:
		CConsole&  m_out  ;
		CError     m_error;
		CWsa_Trace_Receiver
		           m_trace;
	public:
		 CClient_Trace (CConsole&);
		~CClient_Trace (void);

	public:
		HRESULT    Delay(const DWORD _msec);
		TErrorRef  Error(void) const;
		HRESULT    Init (void);
		HRESULT    Term (void);

	private: // IUdpEvent_Async
#pragma warning(disable: 4481)
		virtual HRESULT   IUdpEvt_OnComplete (void) override sealed;
		virtual HRESULT   IUdpEvt_OnError    (const HRESULT hr_) override sealed;
		virtual HRESULT   IUdpEvt_OnReceive  (const CWsa_Cache& _data) override sealed;
		virtual HRESULT   IUdpEvt_OnStart    (void) override sealed;
#pragma warning(default: 4481)
	private: // IPipeTrace Impl;
#pragma warning(disable: 4481)
		virtual HRESULT   IPipeTrace_PutError(LPCSTR _lp_sz_desc, const HRESULT _h_result) override sealed;
		virtual HRESULT   IPipeTrace_PutInfo (LPCSTR _lp_sz_desc) override sealed;
		virtual HRESULT   IPipeTrace_PutInfo (LPCSTR _lp_sz_desc, const DWORD   _d_data  ) override sealed;
		virtual HRESULT   IPipeTrace_PutInfo (LPCSTR _lp_sz_desc, LPCSTR    _lp_sz_data  ) override sealed;
		virtual HRESULT   IPipeTrace_PutWarn (LPCSTR _lp_sz_desc, const HRESULT _h_result) override sealed;
#pragma warning(default: 4481)
	};

}}}

#endif/*_FAKEGPSCONTRACE_H_3AF2A70B_5330_41ED_9E07_B39047C65294_INCLUDED*/