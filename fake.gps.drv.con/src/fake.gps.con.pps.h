#ifndef _FAKEGPSCONPPS_H_3AF2A70B_5330_41ED_9E07_B39047C65294_INCLUDED
#define _FAKEGPSCONPPS_H_3AF2A70B_5330_41ED_9E07_B39047C65294_INCLUDED
/*
	Created by Tech_dog (ebontrop@gmail.com) on 18-Dec-2019 at 11:14:56p, UTC+7, Novosibirsk, Tulenina, Wednesday;
	This is FakeGPS driver desktop app named pipe client wrapper interface declaration file.
*/
#include "shared.gen.sys.err.h"
#include "shared.gen.cmd.arg.h"
#include "shared.gen.con.bas.h"

#include "fake.gps.cli.pipe.h"
#include "fake.gps.pps.reader.h"
#include "fake.gps.pps.writer.h"

namespace shared { namespace drv { namespace client {

	using shared::sys_core::CError;
	using shared::common::ui::CConsole;

	using shared::drv::um::CClientPipe;
	using shared::drv::um::IPipeEvents;
	using shared::drv::um::IPipeTrace ;
	using shared::drv::um::CPipeReader;
	using shared::drv::um::CPipeWriter;

	class CClient_Pipe : public IPipeEvents {
	private:
		CConsole&     m_out  ;
		CError        m_error;
		CClientPipe   m_client;
		CPipeReader   m_reader;
		CPipeWriter   m_writer;

	public:
		 CClient_Pipe (CConsole&, IPipeTrace&);
		~CClient_Pipe (void);

	private: // IPipeEvents;
#pragma warning(disable: 4481)
		virtual HRESULT     IPipeEvent_OnCreate(LPCTSTR) override sealed;
		virtual HRESULT     IPipeEvent_OnRead  (const LPBYTE _p_data, const DWORD _dw_sz) override sealed;
		virtual HRESULT     IPipeEvent_OnWrite (const LPBYTE _p_data, const DWORD _dw_sz) override sealed;
#pragma warning(default: 4481)
	public:
		TErrorRef     Error(void) const;
		HRESULT       Init (void);
		HRESULT       Term (void);
	};

}}}

#endif/*_FAKEGPSCONPPS_H_3AF2A70B_5330_41ED_9E07_B39047C65294_INCLUDED*/