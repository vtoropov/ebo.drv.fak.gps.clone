/*
	Created by Tech_dog (ebontrop@gmail.com) on 18-Dec-2019 at 11:52:34a, UTC+7, Novosibirsk, Tulenina, Wednesday;
	This is FakeGPS driver desktop app TCP/IP and Pipe trace wrapper interface implementation file.
*/
#include "StdAfx.h"
#include "fake.gps.con.trace.h"
#include "fake.gps.tcp.cfg.h"

using namespace shared::drv::um;
using namespace shared::drv::client;

/////////////////////////////////////////////////////////////////////////////

CClient_Trace:: CClient_Trace(CConsole& _out) : m_out(_out), m_trace(*this) { m_error << __MODULE__ << S_OK; }
CClient_Trace::~CClient_Trace(void) {}

/////////////////////////////////////////////////////////////////////////////

HRESULT   CClient_Trace::IUdpEvt_OnComplete(void) {
	m_out.WriteInfo(_T("Receiving driver trace has finished...")); return S_OK;
}

HRESULT   CClient_Trace::IUdpEvt_OnError   (const HRESULT hr_) {
	m_error = hr_;
	return m_out.WriteErr(m_error);
}

HRESULT   CClient_Trace::IUdpEvt_OnReceive (const CWsa_Cache& _data) {
	m_error << __MODULE__ << S_OK;
	if (_data.IsValid()) {
		if (_data.Type() == CWsa_Cache::e_char) {
			CAtlString cs_msg(_data.Data(), _data.Size());
			m_out.WriteInfo(cs_msg.GetString());
		}
		else {
			CAtlString cs_msg((PCWCHAR)_data.Data(), _data.Size()/sizeof(WCHAR));
			m_out.WriteInfo(cs_msg.GetString());
		}
	}
	else {
		m_error.State(__DwordToHresult(ERROR_INVALID_DATA), _T("Received data is invalid;"));
		m_out.WriteErr(m_error);
	}
	return S_OK;
}

HRESULT   CClient_Trace::IUdpEvt_OnStart   (void) {
	m_out.WriteInfo(_T("Receiving driver trace has started...")); return S_OK;
}

/////////////////////////////////////////////////////////////////////////////

HRESULT   CClient_Trace::IPipeTrace_PutError(LPCSTR _lp_sz_desc, const HRESULT _h_result) {
	CAtlString cs_desc (_lp_sz_desc);
	CAtlString cs_error; cs_error.Format((LPCTSTR)cs_desc, _h_result);

	return m_out.WriteErr(cs_error.GetString());
}

HRESULT   CClient_Trace::IPipeTrace_PutInfo (LPCSTR _lp_sz_desc) {
	return m_out.WriteInfo(CAtlString(_lp_sz_desc).GetString());
}

HRESULT   CClient_Trace::IPipeTrace_PutInfo (LPCSTR _lp_sz_desc, const DWORD   _d_data  ) {
	CAtlString cs_desc (_lp_sz_desc);
	CAtlString cs_warn; cs_warn.Format((LPCTSTR)cs_desc, _d_data);

	return m_out.WriteInfo(cs_warn.GetString());
}

HRESULT   CClient_Trace::IPipeTrace_PutInfo (LPCSTR _lp_sz_desc, LPCSTR    _lp_sz_data  ) {
	CAtlStringA cs_desc (_lp_sz_desc); cs_desc.Format(_lp_sz_desc, _lp_sz_data);
	CAtlStringW cs_mess (cs_desc)    ;

	return m_out.WriteWarn(cs_mess.GetString());
}

HRESULT   CClient_Trace::IPipeTrace_PutWarn(LPCSTR _lp_sz_desc, const HRESULT _h_result) {
	CAtlString cs_desc (_lp_sz_desc);
	CAtlString cs_warn; cs_warn.Format((LPCTSTR)cs_desc, _h_result);

	return m_out.WriteWarn(cs_warn.GetString());
}

/////////////////////////////////////////////////////////////////////////////

HRESULT    CClient_Trace::Delay(const DWORD _msec) {
	if (_msec < 100)
		return E_INVALIDARG;
	else {
		m_trace.Delay() = _msec;
		m_trace.Timeout().Set(CTimeout::e_default, _msec);
	}
	return S_OK;
}

TErrorRef  CClient_Trace::Error(void) const { return m_error; }
HRESULT    CClient_Trace::Init (void) {
	m_error << __MODULE__ << S_OK;

	m_trace.Server() = CWsa_Cfg().TracePair().Receiver();
	m_trace.Cache ().Create(CWsa_Cfg().CacheSize() / 4, CWsa_Cache::e_char);
#if (1)
	m_trace.Cache ().Preserve(true);
	m_trace.Cache ().ToCache (_T("trc_rcv.Cache().ToCache"));

	Trace().PutLine("CWsa_Server::recvfrom::error: %d", -1);
#endif

	HRESULT hr_ = m_trace.Turn(true);
	if (FAILED(hr_)) {
		m_error.State(hr_, _T("CWsa_Trace_Receiver::Turn() failed;")); m_out.WriteErr(m_error);
	}

	return m_error;
}
HRESULT    CClient_Trace::Term (void) {
	m_error << __MODULE__ << S_OK;

	HRESULT hr_ = m_trace.Turn(false);
	if (FAILED(hr_)) {
		m_error.State(hr_, _T("CWsa_Trace_Receiver::Turn()::end: failed;"));
		m_out.WriteErr(m_error);
	}

	return m_error;
}