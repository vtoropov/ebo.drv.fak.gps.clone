/*
	Created by Tech_dog (ebontrop@gmail.com) on 5-Apr-2019 at 10:14:02p, UTC+7, Phuket, Rawai, Thursday;
	This is sound-bin-trans receiver desktop console application precompiled header creation file.
	-----------------------------------------------------------------------------
	Adopted to FakeGPS project on 13-Dec-2019 at 8:22:31a, UTC+7, Novosibirsk, Tulenina, Friday;
*/

#include "StdAfx.h"

#if (_ATL_VER < 0x0700)
#include <atlimpl.cpp>
#endif //(_ATL_VER < 0x0700)