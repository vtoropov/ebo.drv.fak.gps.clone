/*
	Created by Tech_dog (ebontrop@gmail.com) on 18-Dec-2019 at 10:59:43p, UTC+7, Novosibirsk, Tulenina, Wednesday;
	This is FakeGPS driver desktop app TCP/IP client wrapper interface implementation file. 
*/
#include "StdAfx.h"
#include "fake.gps.con.tcp.h"
#include "fake.gps.tcp.cfg.h"
#include "fake.gps.cmd.defs.h"
#include "fake.gps.cmd.geo.loc.h"
#include "fake.gps.cmd.time.h"

using namespace shared::drv::um;
using namespace shared::drv::client;

/////////////////////////////////////////////////////////////////////////////

CClient_Tcp:: CClient_Tcp(CConsole& _out) : m_out(_out) { m_error << __MODULE__ << S_OK; }
CClient_Tcp::~CClient_Tcp(void) {}

/////////////////////////////////////////////////////////////////////////////

TErrorRef   CClient_Tcp::Error(void) const { return m_error; }
HRESULT     CClient_Tcp::Init (void) {
	m_error << __MODULE__ << S_OK;

	CCmd_Time_t cmd_time;

	m_tcp_client.Server () = CWsa_Cfg().CmdPair().Sender();
	m_tcp_client.Cache  ().ToCache(cmd_time.Raw(), cmd_time.Size());
	m_tcp_client.Address() = CWsa_Cfg().CmdPair().Receiver();
	m_tcp_client.Address().Port(0);

#if (1)
	cmd_time.Is((PBYTE)m_tcp_client.Cache().Data(), (INT)m_tcp_client.Cache().Size());
#endif
	m_tcp_client.Timeout().Set(CTimeout::e_receive, 666);
	m_tcp_client.Timeout().Set(CTimeout::e_sending, 666);

	HRESULT hr_ = m_tcp_client.Turn(true);
	if (FAILED(hr_)) {
		m_error.State(hr_, _T("CWsa_Client::Turn() failed;")); m_out.WriteErr(m_error);
	}
	return m_error;
}
HRESULT     CClient_Tcp::Term (void) {
	return m_tcp_client.Turn(false);
}

/////////////////////////////////////////////////////////////////////////////
