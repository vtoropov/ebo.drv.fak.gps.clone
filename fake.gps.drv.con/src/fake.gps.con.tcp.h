#ifndef _FAKEGPSCONTCP_H_3AF2A70B_5330_41ED_9E07_B39047C65294_INCLUDED
#define _FAKEGPSCONTCP_H_3AF2A70B_5330_41ED_9E07_B39047C65294_INCLUDED
/*
	Created by Tech_dog (ebontrop@gmail.com) on 18-Dec-2019 at 10:48:12p, UTC+7, Novosibirsk, Tulenina, Wednesday;
	This is FakeGPS driver desktop app TCP/IP client wrapper interface declaration file. 
*/
#include "shared.gen.sys.err.h"
#include "shared.gen.cmd.arg.h"
#include "shared.gen.con.bas.h"

#include "fake.gps.tcp.client.h"

namespace shared { namespace drv { namespace client {

	using shared::sys_core::CError;
	using shared::common::ui::CConsole;

	using shared::drv::um::CWsa_Client;

	class CClient_Tcp {
	private:
		CConsole&   m_out  ;
		CError      m_error;
		CWsa_Client m_tcp_client;

	public:
		 CClient_Tcp (CConsole&);
		~CClient_Tcp (void);

	public:
		TErrorRef   Error(void) const;
		HRESULT     Init (void);
		HRESULT     Term (void);
	};

}}}

#endif/*_FAKEGPSCONTCP_H_3AF2A70B_5330_41ED_9E07_B39047C65294_INCLUDED*/